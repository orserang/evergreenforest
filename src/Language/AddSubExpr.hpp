typedef struct AddSubExprT {
  std::vector<std::vector<std::string>> plus_vars;
  std::vector<std::vector<std::string>> minus_vars;
} AddSubExpr;
