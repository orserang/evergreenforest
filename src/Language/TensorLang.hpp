#include "../Utility/FLOAT_TYPE.hpp"

typedef struct TensorLangT {
  std::vector<FLOAT_TYPE> flat_vector;
  std::vector<long> shape; 
} TensorLang;
