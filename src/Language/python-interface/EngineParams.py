# For holding engine parameters
class EngineParams:
  def __init__(self, engine_type, damp_lambda=0.05, convergence_eps=1e-5, max_iter=1000000):
    # Ensure we are passing valid params
    assert engine_type in ('brute_force', 'loopy')
    assert damp_lambda < 0.5, "Dampening should be performed with lambda < 0.5 (higher lambda values will weight older messages over new messages, and may lead to oscillations [unproven])"

    self._engine_type = engine_type
    self._damp_lambda = damp_lambda
    self._convergence_eps = convergence_eps
    self._max_iter = max_iter

    self._engine_prefix = '@engine='
    self._damp_prefix = '@dampening='
    self._convergence_prefix = '@epsilon='
    self._max_iter_prefix = '@max_iter='

  def __str__(self):
    engine_string = self._engine_prefix + self._engine_type
    damp_string = self._damp_prefix + str(self._damp_lambda)
    convergence_string = self._convergence_prefix + str(self._convergence_eps)
    max_iter_string = self._max_iter_prefix + str(self._max_iter)

    if self._engine_type == 'brute_force':
      return engine_string + '()'
    else:      
      return engine_string + '(' + ', '.join([damp_string, convergence_string, max_iter_string]) + ')'
