
'''
For creating additive dependencies and inequalities for Evergreen
additive_str can take the following formats:
  A + B = C
  A + B < C + D
  A - B = C - D
  A,B + C,D >= E,F  
'''

class BooleanExpression:

  # Note: if a variable within the additive string contains a special character (e.g. +, -, =, @, (, ), *, [, ]),
  # numbers, or a reserved word users should take care to single quote the variable name.
  def __init__(self, additive_str, p_norm=None):

    self._additive_str = str(additive_str)
    self._p_norm = p_norm

  def __str__(self):
    result = self._additive_str
    if self._p_norm is not None:
      result += ' @p=' + str(self._p_norm)
    
    return result
