from EvergreenPMF import *

# For parsing evergreen output
class EvergreenOutputParser:
  def __init__(self, fname):
    self._log_likelihood = None
    self._kl_divergence = None
    # Note: frozensets could have been used as keys here, however, python sets are not ordered. Thus, the variable
    # ordering of joint PMFs would not be preserved.
    self._var_tuple_to_pmf = {}
    self._print_strings = []
    self._numeric_failures = []
    self._warnings = []
    self._evg_errors = []
    self._misc_lines = []

    evg_out_file = fname
    # If supplied object is a string, treat it as a filename
    if type(evg_out_file) is str:
      evg_out_file = open(fname)

    for line in evg_out_file:
      line = line.replace('\n', '')  # remove newline char

      if len(line) == 0:  # blank line
        continue

      elif line.startswith("Log probability"):
        string, log_probability_str = line.split(':')
        log_probability_str = log_probability_str.strip()
        self._log_likelihood = float(log_probability_str)

      elif line.startswith("KL Divergence"):
        string, kl_divergence_str = line.split(':')
        kl_divergence_str = kl_divergence_str.strip()
        self._kl_divergence = float(kl_divergence_str) 
     
      elif line.startswith(':'):
        self._print_strings.append(line[1:])

      elif line.startswith("evg"):
        self._numeric_failures.append(line)

      elif line.startswith("ERROR:"):
        self._evg_errors.append(line)

      elif line.startswith("Warning:"):
        self._warnings.append(line)

      elif " PMF:{[" in line:
        pmf_start_idx = line.index('PMF:{')
        pmf_string = line[pmf_start_idx:]
        var_string = line[:pmf_start_idx].strip()

        pmf = EvergreenPMF(pmf_string)

        var_tuple = tuple(var_string.split(' '))
        self._var_tuple_to_pmf[var_tuple] = pmf

      else:
        self._misc_lines.append(line)

    evg_out_file.close()

  def pmf_matching_var_tuple(self, var_tuple):
    return self._var_tuple_to_pmf[var_tuple]

  def var_tuples(self):
    return self._var_tuple_to_pmf.keys()

  def pmfs(self):
    return self._var_tuple_to_pmf.items()

  def var_to_pmfs(self):
    return self._var_tuple_to_pmf

  def log_likelihood(self):
    return self._log_likelihood
  
  def kl_divergence(self):
    return self._kl_divergence

  def print_string_lines(self):
    return self._print_strings

  def numeric_failures(self):
    return self._numeric_failures

  def evg_warnings(self):
    return self._warnings

  def evg_errors(self):
    return self._evg_errors

  def misc_lines(self):
    return self._misc_lines
