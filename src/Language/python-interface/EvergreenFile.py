from TupleUtil import *

# For the creation of Evergreen file objects
class EvergreenFile:
  def __init__(self, def_p_norm, engine_params):
    self._lines = []
    self._joint_posteriors_to_compute = set()

    p_norm_string = '@p=' + str(def_p_norm)

    self.app(p_norm_string)
    self.app(engine_params)

  def app(self, line):
    self._lines.append(str(line))

  def add_dependency(self, dep):
    self.app(dep)

  def add_joint_posterior_to_compute(self, variables):
    self._joint_posteriors_to_compute.add(variables)

  def __str__(self):
    result = '\n'.join(self._lines) + '\n'
    result += 'Pr(' + ';'.join([tup_str(translate_tup(variables)).translate(None, "()") for variables in self._joint_posteriors_to_compute]) + ') # variables to print posteriors on (may be empty) \n'
    result += 'LOG_LIKELIHOOD() # log likelihood constant\n'
    result += 'KL_DIVERGENCE() # KL divergence\n'
    return result

  def save(self, fname):
    with open(fname, mode='w') as f:
      f.write( str(self) )
