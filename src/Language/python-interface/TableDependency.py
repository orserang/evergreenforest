from TupleUtil import *
import numpy as np

# For creating table dependencies for Evergreen
class TableDependency:
  def __init__(self, variables, start_points, array, normalize=True, p_norm=None):
    if type(start_points) is int:
      start_points = (start_points,)

    if type(variables) is str:
      variables = (variables,)

    self._pmf_vars = variables
    self._supports = start_points
    self._pmf_table = np.array(array)
    self._p_norm = p_norm

    # (optionally) Normalize pmf table to ensure log normalization constant is properly computed within Evergreen
    if normalize:
      self._pmf_table /= sum(self._pmf_table.flatten())

  def __str__(self):
    result = 'PMF ' + tup_str(translate_tup(self._pmf_vars)) + ' ' + tup_str(self._supports) + ' ' + str(self._pmf_table.tolist())

    if self._p_norm is not None:
      result += ' @p=' + str(self._p_norm)

    return result

  def supports(self):
    return self._supports

  def pmf_vars(self):
    return self._pmf_vars

  def prob_table(self):
    return self._pmf_table
