# For processing tuples for Evergreen
def translate_tup(variables):
  if type(variables) is str:
    return "'" + variables.strip("'") + "'"
  return tuple([ "'" + v.strip("'") + "'" for v in variables ])

def tup_str(t):
  if type(t) is str:
    return '(' + t + ')'
  elif len(t) == 1:
    return '(' + str(t[0]) + ')'
  return str(t).replace('"', '')

def strip_tup(t):
  # If no translation table is supplied, given characters are simply deleted from str
  return str(t).translate(None, "()',")
