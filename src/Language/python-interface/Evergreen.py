from EvergreenFile import *
from TableDependency import *
from BooleanExpression import *
from EngineParams import *
from EvergreenOutputParser import *
from subprocess import Popen,PIPE
import tempfile
import os
import sys

# FIXME: currently here to allow for easy testing, can remove python 2
# code in favor of python 3 when done.
try:
  from StringIO import StringIO ## for Python 2
except ImportError:
  from io import StringIO ## for Python 3

# Defaults for engine
MAX_ITERATIONS=1000000
DAMPENING=0.05
CONVERGENCE_EPSILON=1e-5
P_NORM=1

class Evergreen:
  def __init__(self, p_norm=P_NORM, engine_type='loopy', damp_lambda=DAMPENING, convergence_eps=CONVERGENCE_EPSILON, max_iter=MAX_ITERATIONS, evg_accuracy='default'):

    assert evg_accuracy in ("low", "default", "high")

    self._pmfs = []
    self._boolean_exprs = []

    self._engine_params = EngineParams(engine_type, damp_lambda, convergence_eps, max_iter)
    self._p_norm = p_norm

    # Allow user to pick between evg-float, evg-double, and evg-long-double
    self._evg_accuracy = evg_accuracy

  # Accepts a table dependency
  def add_pmf(self, pmf):
    self._pmfs.append(pmf)

  # Accepts an additive dependency or inequality
  def add_boolean_expr(self, boolean_expr):
    self._boolean_exprs.append(boolean_expr)

  # Accepts an engine EngineParams object.
  # These parameters will be used during the next call to get_results()
  def set_engine_params(self, engine_params):
    self._engine_params = engine_params

  # Sets the default p-norm to be used during the next call to get_results()
  def set_p_norm(self, p):
    self._p_norm = p
 
  # Prints the evergreen file
  def print_evg_file(self):
    print(self._create_evg_file())

  # Saves the evergreen file
  def save_evg_file(self, fname):
    self._create_evg_file().save(fname)

  # For determining which evg executable to use
  def _get_evg_exec_with_desired_accuracy(self):
    if self._evg_accuracy is "low":
      return "evg-float"
    elif self._evg_accuracy is "default":
      return "evg-double"
    else:
      return "evg-long-double"

  def _create_evg_file(self, posteriors=()):

    evg_file = EvergreenFile(self._p_norm, self._engine_params)

    # Add table dependencies
    for pmf in self._pmfs:
      evg_file.add_dependency(pmf)

    # Add boolean expressions
    for boolean_expr in self._boolean_exprs:
      evg_file.add_dependency(boolean_expr)

    # Add posteriors to print
    for posterior in posteriors:
      if type(posterior) is str:
        evg_file.add_joint_posterior_to_compute((posterior,))

      else:
        evg_file.add_joint_posterior_to_compute(posterior)

    return evg_file

  # NOTE: assumes 'posteriors' is a tuple of tuples of strings (IE posteriors = ((X,), (Y, Z), ...))
  def get_results(self, posteriors):

    evg_file = self._create_evg_file(posteriors)

    # FIXME: debug
    #print evg_file

    # Save out .evg file
    tmp_dir = tempfile.mkdtemp()
    evg_path = tempfile.mktemp(dir=tmp_dir) + '.evg'
    evg_file.save(evg_path)

    self.tmp_dir = tmp_dir

    # Get path to evergreen src
    EVERGREEN_SRC_PATH = os.getenv('EVERGREEN_SRC_PATH')

    # Get path to evergreen executable
    evg_exec = self._get_evg_exec_with_desired_accuracy()
    evg_exec_path = '/'.join([EVERGREEN_SRC_PATH,'Language', evg_exec])

    # Ensure environment variable exists
    if EVERGREEN_SRC_PATH is None:
      raise Exception('Environment variable EVERGREEN_SRC_PATH is not defined.')

    # Ensure evg executable exists
    if not os.path.isfile(evg_exec_path):
      raise Exception('evg executable ' + evg_exec + ' not found.')

    # Run evg as a subprocess
    command = ' '.join([evg_exec_path, evg_path])
    p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE,stderr=PIPE)
    stdout, stderr = p.communicate()
    
    # let user know if evg crashes
    sys.stderr.write(stderr.decode())

    # Turn stdout string into a file
    evg_out_file = StringIO(stdout.decode())
    evg_parser = EvergreenOutputParser(evg_out_file)

    return evg_parser.var_to_pmfs(), evg_parser.log_likelihood(), evg_parser.kl_divergence()
