import numpy as np
import ast

# For processing the output of Evergreen
class EvergreenPMF:
  def __init__(self, string):
    assert(string.startswith('PMF:'))

    bounds, tensor = string.split('}')

    bounds = bounds.replace('PMF:{','')
    bounds = bounds.replace('}','')
    bounds = bounds.replace(' to ',':')

    lower, upper = bounds.split(':')

    self._first_support = np.array(ast.literal_eval(lower))
    self._last_support = np.array(ast.literal_eval(upper))
    self._dimension = len(self._first_support)

    tensor = tensor.replace('t:','').strip()
    self._tensor = np.array(ast.literal_eval(tensor))

  def tensor(self):
    return self._tensor

  def first_support(self):
    return self._first_support

  def last_support(self):
    return self._last_support

  def dimension(self):
    return self._dimension

  def get_mass(self, index):
    if type(index) is int:
      index = [index]

    if any(index < self._first_support) or any(index > self._last_support):
      return 0.0
    adjusted_index = index - self._first_support
    return self._tensor[adjusted_index][0]

  def __str__(self):
    return 'PMF:{' + str(self._first_support.tolist()) + " to " + str(self._last_support.tolist()) + '} t:' + str(self._tensor.tolist())
