#ifndef _FROM_STRING_HPP
#define _FROM_STRING_HPP

#include <sstream>
#include <string>
#include "../Utility/FLOAT_TYPE.hpp"


FLOAT_TYPE from_string(const std::string & s) {
  std::istringstream ist(s);
  FLOAT_TYPE result;
  ist >> result;
  return result;
}

#endif
