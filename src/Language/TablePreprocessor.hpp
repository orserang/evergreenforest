typedef struct TablePreprocessorT{
  private:
    TableDependency<std::string>* _table;
  public:
    TablePreprocessorT (const std::vector<std::string> & labels, const std::vector<long> & offsets, const std::vector<long> & _shape, const std::vector<FLOAT_TYPE> & _flat_vect, const FLOAT_TYPE & p, const int & yylineno) {
    Vector<FLOAT_TYPE> flat_vect(_flat_vect);
    Vector<long> shape(_shape);
    Tensor<FLOAT_TYPE> tensor(shape,flat_vect);

    if (labels.size() != tensor.dimension() || tensor.dimension() != offsets.size() || labels.size() != offsets.size())
      std::cerr << "ERROR: PMF error, number of variables, offsets, and dimension of tensor do not match on line " << yylineno << std::endl;

    PMF pmf(offsets,tensor);
    LabeledPMF<std::string> lpmf(labels,pmf);
    _table = new TableDependency<std::string>(lpmf, p);
  }

  const void clear_memory() const {
    delete _table;
  }

  const TableDependency<std::string> & table() const {
    return *_table;
  }
} TablePreprocessor;
