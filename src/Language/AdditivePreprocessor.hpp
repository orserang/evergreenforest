typedef struct AdditivePreprocessorT{
  private:
    std::vector<AdditiveDependency<std::string> > _additive_deps;
    std::vector<TableDependency<std::string> > _table_deps;
  public:
    AdditivePreprocessorT(const BoolExpr & bool_expr, int & node_index, const int & yylineno) {
      assert(bool_expr.op().compare("=") == 0);
      std::vector<std::vector<std::string> > left_vars = bool_expr.left_vars();
      std::vector<std::vector<std::string> > right_vars =  bool_expr.right_vars();
      FLOAT_TYPE p = bool_expr.p();
      bool use_noisy_or = bool_expr.use_noisy_or();

      if(left_vars.size() == 1)
        _additive_deps.push_back(AdditiveDependency<std::string>(right_vars,left_vars[0],p, use_noisy_or));
      else if(right_vars.size() == 1)
        _additive_deps.push_back(AdditiveDependency<std::string>(left_vars,right_vars[0],p, use_noisy_or)); 
      else {
        _additive_deps.push_back(AdditiveDependency<std::string>(left_vars,{"@N" + to_string(node_index)},p, use_noisy_or));
        _additive_deps.push_back(AdditiveDependency<std::string>(right_vars,{"@N" + to_string(node_index)},p, use_noisy_or));
        ++node_index;   
      }
    }
  
    const std::vector<TableDependency<std::string> > & table_deps() const {
      return _table_deps;
    }

    const std::vector<AdditiveDependency<std::string> > & additive_deps() const {
      return _additive_deps;
    }
} AdditivePreprocessor;
