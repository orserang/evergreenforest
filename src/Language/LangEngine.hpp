#ifndef _LANGENGINE_HPP
#define _LANGENGINE_HPP

#include "InferenceEnginesBuilder.hpp"
#include "LangDigraph.hpp"
#include <unordered_map>
#include <fstream>
#include "../Utility/graph_to_dot.hpp"
#include "../Utility/FLOAT_TYPE.hpp"
#include "../Utility/Clock.hpp"


typedef struct LangEngineT {
  LangGraph<std::string> graph;
  
  InferenceEnginesBuilder*ieb_ptr;

	Clock clock;
  std::unordered_map<std::string, std::vector<unsigned long> > var_to_graphs_containing;
  std::vector<Dependency<std::string>* > dependencies;
  std::vector<BoolExpr> inequalities;
  std::vector<InferenceEngine<std::string>* > engine_ptrs;
  std::map<std::string, std::pair<long, long> > var_to_bounds;
  LangEngineT(const FLOAT_TYPE & default_damp, const FLOAT_TYPE & default_eps, const long & default_max_iter):
    ieb_ptr(new BeliefPropagationInferenceEnginesBuilder(default_damp, default_eps, default_max_iter))
  { }

  ~LangEngineT() {
    delete ieb_ptr;
    
    for (Dependency<std::string>*dep_ptr : dependencies)
      delete dep_ptr;
  }

  void tick_clock(){
    clock.tick();
  }

  void ptock_clock(){
    clock.ptock();
  }

  void insert_inequality(BoolExpr inequality) {
    inequalities.push_back(inequality);
  }

  void insert_dep(Dependency<std::string> * dep) {
    dependencies.push_back(dep);
    const std::vector<std::string> & vars_used = dep->get_all_variables_used();
    for (const std::string & var : vars_used)
      var_to_graphs_containing[var].push_back(dependencies.size()-1);
    if (dynamic_cast<TableDependency<std::string>*>(dep) != NULL) {
      TableDependency<std::string>*table_dep = dynamic_cast<TableDependency<std::string>*>(dep);
      const Vector<long> first_support = table_dep->labeled_pmf().pmf().first_support();
      const Vector<long> last_support = table_dep->labeled_pmf().pmf().last_support();
      for (unsigned int i = 0; i < vars_used.size(); i++)
        var_to_bounds[vars_used[i]] = std::make_pair(first_support[i], last_support[i]);
    }
  }

  void set_engine(InferenceEnginesBuilder*ieb) {
    delete ieb_ptr;
    ieb_ptr = ieb;
  }

  void print(const std::vector<std::vector<std::string> > & result_vars) {
    build_inequalities();
    const std::vector<std::string> & flat_result_vars = flatten(result_vars);
    const std::vector<std::vector<std::string> > & partitioned_subgraphs = partition_into_subgraphs(flat_result_vars);

    const std::vector<std::vector<Dependency<std::string>* > > & dependencies_of_subgraphs = get_dependencies_of_subgraphs(partitioned_subgraphs);


    // delete old engine pointers
    for (unsigned long i=0; i<engine_ptrs.size(); ++i)
      delete engine_ptrs[i];

    engine_ptrs = ieb_ptr->build_engines(dependencies_of_subgraphs);

    std::unordered_map<std::string, int> var_to_graph_number;
    for (unsigned long i=0; i<partitioned_subgraphs.size(); ++i) {
      const std::vector<std::string> & vars_in_connected_graph = partitioned_subgraphs[i];
      for (const std::string & var : vars_in_connected_graph)
      	var_to_graph_number[var] = i;
    }

    // outer vector is for subgraph
    // middle vector is for the possibly many tuples on which we want posteriors
    // inner vector is a tuple of variables.
    std::vector<std::vector<std::vector<std::string> > > printed_partitioned_subgraphs;
    printed_partitioned_subgraphs.resize(dependencies_of_subgraphs.size());
    for (const std::vector<std::string> & result_var : result_vars) {
      int graph_num = var_to_graph_number[result_var[0]];
      for (const std::string & var : result_var)
        if (var_to_graph_number[var] != graph_num)
          std::cerr << "ERROR: Printing error, tried to print posteriors on set of vars that belong in different subgraphs." << std::endl;
      printed_partitioned_subgraphs[var_to_graph_number[result_var[0]]].push_back(result_var);
    }

    std::vector<std::vector<LabeledPMF<std::string> > > all_results_to_print(printed_partitioned_subgraphs.size());
    #pragma omp parallel for
    for (unsigned long i=0; i<printed_partitioned_subgraphs.size(); ++i)
      all_results_to_print[i] = engine_ptrs[i]->estimate_posteriors(printed_partitioned_subgraphs[i]);
    for (const std::vector<LabeledPMF<std::string> > & results_to_print : all_results_to_print)
      for (const LabeledPMF<std::string> & result_to_print : results_to_print)
        std::cout << result_to_print << std::endl; 
  }
  
  // ----------------------------------------------------
  // not for client use (i.e., private helper functions):
  // ----------------------------------------------------

  void build_inequalities() {
    for (BoolExpr inequality: inequalities) {
      std::vector<unsigned long> result_table_dims;
      std::string op = inequality.op();
      std::vector<long> first_support;
      for(const std::string & var : inequality.vars_used()) {
        std::pair<long, long> bounds;
        if (var_to_bounds.find(var) != var_to_bounds.end())
  	      bounds = var_to_bounds.find(var)->second;
        else
          std::cerr << "ERROR: Inequality error: tried to calculate inequality with variables that have no bounds" << std::endl;
        long var_small = bounds.first;
        long var_large = bounds.second;
        first_support.push_back(var_small);
        result_table_dims.push_back(var_large - var_small + 1);
      }
      int num_dimensions = inequality.left_vars()[0].size();
      int num_left_vars = inequality.left_vars().size() * num_dimensions;
      // Initialized with zeros:
      Tensor<FLOAT_TYPE> result_table(result_table_dims);

      enumerate_apply_tensors([&first_support, inequality, num_left_vars, num_dimensions, op](const_tup_t index, const int dim, FLOAT_TYPE & res_val){
        std::vector<long> left_sum_val(num_dimensions, 0);
        for (unsigned char current_dim = 0; current_dim < num_dimensions; ++current_dim)
          for(unsigned char i = current_dim; i < num_left_vars; i += num_dimensions)
            left_sum_val[current_dim] += index[i] + first_support [i];

        std::vector<long> right_sum_val(num_dimensions, 0);
        for (int current_dim = 0; current_dim < num_dimensions; ++current_dim)
          for(int i = current_dim + num_left_vars; i < dim; i += num_dimensions)
            right_sum_val[current_dim] += index[i] + first_support [i];
          
        bool meets_condition = true;        
        for (int i = 0; i < num_dimensions; ++i) {
          if (op.compare("<=") == 0 || op.compare("=<") == 0) {
            if (left_sum_val[i] > right_sum_val[i])
              meets_condition = false;
          }
          else if (op.compare("<") == 0) {
            if (left_sum_val[i] >= right_sum_val[i])
              meets_condition = false;
          }
          else if (op.compare(">") == 0) {
            if (left_sum_val[i] <= right_sum_val[i])
              meets_condition = false;
          }
          else if (op.compare(">=") == 0 || op.compare("=>") == 0) {
            if (left_sum_val[i] < right_sum_val[i])
              meets_condition = false;
          }
        }
        if (meets_condition)
          res_val = 1.0L;
        },
      result_table.data_shape(),
      result_table);     

      LabeledPMF<std::string> table_dependency(inequality.vars_used(), PMF(first_support, result_table));
      table_dependency.reset_log_normalization_constant();
      insert_dep(new TableDependency<std::string>(table_dependency, inequality.p()));
    }
    inequalities.clear();
  }
  std::vector<std::vector<std::string> > partition_into_subgraphs(const std::vector<std::string> & result_vars) {
    std::vector<std::vector<std::string> > partitioned_subgraphs;
    std::set<std::string> vars_visited;
    std::set<std::string> result_vars_visited; 
		std::vector<bool> dependency_visited(dependencies.size()-1, false);
    for(const std::string & result_var : result_vars) {
      if (result_vars_visited.find(result_var) == result_vars_visited.end()) {
        std::vector<std::string> subgraph;
        partition_into_single_subgraph(result_var, subgraph, vars_visited, result_vars, result_vars_visited, dependency_visited);
        if (subgraph.size() > 0) 
          partitioned_subgraphs.push_back(subgraph);
      }
    }
    return partitioned_subgraphs;
  }

  void partition_into_single_subgraph(const std::string & result_var, std::vector<std::string> & subgraph, std::set<std::string> & vars_visited, const std::vector<std::string> & result_vars, std::set<std::string> & result_vars_visited, std::vector<bool> &dependency_visited) {
    if (vars_visited.find(result_var) == vars_visited.end()) {
      vars_visited.insert(result_var);
      subgraph.push_back(result_var);
      for(const int & dep_index : var_to_graphs_containing[result_var]) {
        if (!dependency_visited[dep_index]) {
          dependency_visited[dep_index] = true;

					std::vector<std::string> adj_vars = dependencies[dep_index]->get_all_variables_used();
					for (const std::string & adj_var : adj_vars) {
						if (result_vars_visited.find(adj_var) == result_vars_visited.end()) {
							if (find(result_vars.begin(), result_vars.end(), adj_var) != result_vars.end())
								result_vars_visited.insert(adj_var);
							partition_into_single_subgraph(adj_var, subgraph, vars_visited, result_vars, result_vars_visited, dependency_visited);
						}
          }
        }
      }
    }
  }

  void get_dependencies_in_single_subgraph(const std::string & var, std::vector<bool> & deps_visited, std::vector<Dependency<std::string>* > & connected_dependencies) {
    for (const int & dep_index : var_to_graphs_containing[var]) {
      if (!deps_visited[dep_index]) {
        deps_visited[dep_index] = true;
        connected_dependencies.push_back(dependencies[dep_index]);
        const std::vector<std::string> & vars_used = dependencies[dep_index]->get_all_variables_used();
        for (const std::string & var_used : vars_used) {
          get_dependencies_in_single_subgraph(var_used, deps_visited, connected_dependencies);
        }
      }
    }
    if (connected_dependencies.size() == 0) {
      std::cerr << "ERROR: printing error, tried to print posteriors on var " << var << " that doesn't exist in any graph" << std::endl;
    }
  }

  std::vector<std::vector<Dependency<std::string>* > > get_dependencies_of_subgraphs(const std::vector<std::vector<std::string> > & partitioned_subgraphs) {
    std::vector<std::vector<Dependency<std::string>* > > dependencies_of_subgraphs;
    std::vector<bool> deps_visited;
    deps_visited.resize(dependencies.size());
    for(const std::vector<std::string> & subgraph : partitioned_subgraphs) {
      std::vector<Dependency<std::string>* > dependencies_in_single_graph;
      get_dependencies_in_single_subgraph(subgraph[0], deps_visited, dependencies_in_single_graph);
      dependencies_of_subgraphs.push_back(dependencies_in_single_graph);
    } 
    return dependencies_of_subgraphs;
  }
  
  /*
  void recompute_and_print_normalization_constant() {
    std::vector<std::string> all_vars;
    for (const std::pair<std::string, std::vector<unsigned long> > & p : var_to_graphs_containing)
      all_vars.push_back(p.first);
    
    std::vector<std::vector<std::string> > partitioned_subgraphs = partition_into_subgraphs(all_vars);
    std::vector<std::vector<Dependency<std::string>* > > all_partitioned_dependencies = get_dependencies_of_subgraphs(partitioned_subgraphs);

    engine_ptrs = ieb_ptr->build_engines(all_partitioned_dependencies);

    std::vector<std::vector<std::vector<std::string> > > singleton_partitions;
    for (std::vector<std::string> & subgraph_vars : partitioned_subgraphs)
      singleton_partitions.push_back(make_singletons(subgraph_vars));

    #pragma omp parallel for
    for (unsigned long i=0; i<partitioned_subgraphs.size(); ++i)
      engine_ptrs[i]->estimate_posteriors(singleton_partitions[i]);

    print_normalization_constant();
  }

  void print_normalization_constant() {
    FLOAT_TYPE log_nc = 0.0L;
    for (InferenceEngine<std::string>*ie : engine_ptrs)
      log_nc += ie->log_normalization_constant();

    std::cout << "Log probability of model: " << log_nc << std::endl;

    for (unsigned long i=0; i<engine_ptrs.size(); ++i)
      delete engine_ptrs[i];
  }
  */

  void recompute_and_print_model_metric(std::string metric) {
    std::vector<std::string> all_vars;
    for (const std::pair<std::string, std::vector<unsigned long> > & p : var_to_graphs_containing)
      all_vars.push_back(p.first);
    
    std::vector<std::vector<std::string> > partitioned_subgraphs = partition_into_subgraphs(all_vars);
    std::vector<std::vector<Dependency<std::string>* > > all_partitioned_dependencies = get_dependencies_of_subgraphs(partitioned_subgraphs);

    // delete old engine pointers
    for (unsigned long i=0; i<engine_ptrs.size(); ++i)
      delete engine_ptrs[i];

    engine_ptrs = ieb_ptr->build_engines(all_partitioned_dependencies);

    std::vector<std::vector<std::vector<std::string> > > singleton_partitions;
    for (std::vector<std::string> & subgraph_vars : partitioned_subgraphs)
      singleton_partitions.push_back(make_singletons(subgraph_vars));

    #pragma omp parallel for
    for (unsigned long i=0; i<partitioned_subgraphs.size(); ++i)
      engine_ptrs[i]->estimate_posteriors(singleton_partitions[i]);

    print_model_metric(metric);        
  }

  // possible values for metric are log_normalization_constant and kl_divergence
  void print_model_metric(std::string metric_string) {
    FLOAT_TYPE metric = 0.0L;
    if (metric_string.compare("LOG_LIKELIHOOD") == 0) {
      for (InferenceEngine<std::string>*ie : engine_ptrs)
        metric += ie->calc_log_normalization_constant();
      std::cout << "Log probability of model: " << metric << std::endl;
    }
    else if (metric_string.compare("KL_DIVERGENCE") == 0) {
      //fixme: code breaks here
      for (InferenceEngine<std::string>*ie : engine_ptrs)
        metric += ie->calc_kl_divergence();
      std::cout << "KL Divergence of model: " << metric << std::endl;
    }
    else
      std::cerr << "Warning: engine metric " << metric << " not recognized" << std::endl;

  }


  void save_graph(char*str) {
    BetheInferenceGraphBuilder<std::string> igb;
    for (Dependency<std::string>* dep : dependencies)
      igb.insert_dependency(*dep);

    std::ofstream fout(str);
    graph_to_dot(igb.to_graph(), fout);
    fout.close();
  }
} LangEngine;

#endif
