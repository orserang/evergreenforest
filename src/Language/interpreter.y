%{
#include <iostream>
#include <unordered_map>
#include <string>
#include <cstring>
#include <algorithm>
#include <vector>
#include <limits>

#include "../Utility/FLOAT_TYPE.hpp"

FLOAT_TYPE p = 1;
bool use_noisy_or = false;

#include "../Evergreen/evergreen.hpp"

#include "AddSubExpr.hpp"
#include "BoolExpr.hpp"
#include "VarTuple.hpp"
#include "AddSubTerm.hpp"
#include "PrintBlock.hpp"
#include "IntTuple.hpp"
#include "TensorLang.hpp"
#include "LangEngine.hpp" // fixme: rename

#include "TablePreprocessor.hpp"
#include "AdditivePreprocessor.hpp"

#define YYDEBUG 1
#define YYMAXDEPTH  100000
#define YYINITDEPTH 100000

  const FLOAT_TYPE default_dampening = 0;
  const FLOAT_TYPE default_epsilon = 1e-15;
  const long default_max_iter = 1L<<14;
  LangEngine engine(default_dampening, default_epsilon, default_max_iter);

  extern "C" FILE *yyin;
  extern "C" int yylex();
  extern "C" int yyparse();
  extern int yylineno;
  int node_index = 0;
  void yyerror(const char *s);
%}

%union {
  FLOAT_TYPE floatPoint;
  int integer;
  char*str;
  AddSubExpr*addSubExpr;
  AddSubTerm*addSubTerm;
  VarTuple*varTuple;
  IntTuple*intTuple;
  TensorLang*tensor;
  PrintBlock*printBlock;
}

%define parse.error verbose

%token EOL
%token COMMENT
%token PMFTOK
%token UNIFORM

%token PRINT
%token MODEL_METRIC

%token GRAPH
%token SEMICOLON
%token LPAREN RPAREN
%token LBRACKET RBRACKET

%token VARNAME
%token SPECIAL_VARNAME

%token FLOAT UNSIGNED_INT
%token ASSIGNTOK
%token ADD SUB MULT DIV COMMA EQUALS INEQUALITYOP OR

%token STRING

%token ENGINE
%token DAMPENING
%token EPSILON
%token MAXITER
%token LOOPY
%token BRUTEFORCE

%token P
%token ATMARK
%token TICK TOCK

%token INVALID_TOKEN

%type<floatPoint> UNSIGNED_INT
%type<floatPoint> FLOAT
%type<floatPoint> floatInput
%type<intTuple> intTupleExpr

%type<integer> intExpr
%type<floatPoint> floatExpr
%type<floatPoint> floatAddSubExpr
%type<floatPoint> floatMultDivExpr
%type<floatPoint> atomicExpr

%type<str> STRING

%type<str> VAR
%type<str> VARNAME
%type<str> SPECIAL_VARNAME

%type<varTuple> varTuple

%type<str> pmfLine
%type<str> uniformPMF
%type<str> categoricalPMF

%type<str> inequalityLine
%type<str> INEQUALITYOP
%type<str> EQUALS

%type<addSubExpr> addSubExpr
%type<addSubTerm> addSubTerm
%type<varTuple> additiveTuple
%type<str> additiveTerm

%type<str> printLine
%type<printBlock> printBlock

%type<str> MODEL_METRIC
%type<str> modelMetricLine

%type<tensor> floatTensor
%type<tensor> floatTensorTuple
%type<tensor> floatTupleExpr
%%

prog: multiLine
;

multiLine: multiLine EOL line
| line
;

line: inequalityLine
| additiveLine
| orLine
| tickLine
| tockLine
| pmfLine 
| printLine
| modelMetricLine
| saveGraphLine
| commentLine
| pLine
| engineLine
| error
| INVALID_TOKEN
| /* NULL */
;

engineLine: ATMARK ENGINE EQUALS engineType
;

engineType: BRUTEFORCE LPAREN RPAREN {
  engine.set_engine(new BruteForceInferenceEnginesBuilder);
}
| LOOPY LPAREN RPAREN {
  engine.set_engine(new BeliefPropagationInferenceEnginesBuilder(default_dampening, default_epsilon, default_max_iter));
}
| LOOPY LPAREN ATMARK DAMPENING EQUALS floatExpr COMMA ATMARK EPSILON EQUALS floatExpr COMMA ATMARK MAXITER EQUALS intExpr RPAREN {
  FLOAT_TYPE dampening = $6;
  assert(dampening >= 0.0 && dampening <= 1.0);
  FLOAT_TYPE epsilon = $11;
  assert(epsilon >= 0.0 && epsilon <= 1.0);
  long max_iter = $16;
  assert(max_iter >= 0.0);

  engine.set_engine(new BeliefPropagationInferenceEnginesBuilder(dampening, epsilon, max_iter));
}
;
 
pLine: ATMARK P EQUALS floatInput {
  FLOAT_TYPE p_value = $4;
  assert(p_value>0);
  p = $4;
}
;

commentLine: line COMMENT
;

inequalityLine: addSubExpr INEQUALITYOP addSubExpr {

  // NOTE: yylineno is 1 higher than it should be in error functions here, we reduce to this rule when a newLine is found

  BoolExpr bool_expr ($1, $3, $2, p, yylineno - 1, use_noisy_or);  

  engine.insert_inequality(bool_expr);

  // we're finished with both addSubExpr types:
  delete $1;
  delete $3;  
}
| addSubExpr INEQUALITYOP addSubExpr ATMARK P EQUALS floatExpr {

  FLOAT_TYPE p_value = $7;
  assert(p_value>0);
  BoolExpr bool_expr ($1, $3, $2, p_value, yylineno - 1, use_noisy_or);  

  engine.insert_inequality(bool_expr);

  delete $1;
  delete $3; 
}
;

additiveLine: addSubExpr EQUALS addSubExpr {

  BoolExpr bool_expr ($1, $3, $2, p, yylineno - 1, use_noisy_or);  

  AdditivePreprocessor preprocessor(bool_expr, node_index, yylineno - 1);
  for (AdditiveDependency<std::string> additive: preprocessor.additive_deps())
    engine.insert_dep(new AdditiveDependency<std::string>(additive));
  for (TableDependency<std::string> table: preprocessor.table_deps())
    engine.insert_dep(new TableDependency<std::string>(table));

  delete $1;
  delete $3; 
}
| addSubExpr EQUALS addSubExpr ATMARK P EQUALS floatExpr {
  FLOAT_TYPE p_value = $7;
  assert(p_value>0);

  BoolExpr bool_expr ($1, $3, $2, p_value, yylineno - 1, use_noisy_or);  

  AdditivePreprocessor preprocessor(bool_expr, node_index, yylineno - 1);
  for (AdditiveDependency<std::string> additive: preprocessor.additive_deps())
    engine.insert_dep(new AdditiveDependency<std::string>(additive));
  for (TableDependency<std::string> table: preprocessor.table_deps())
    engine.insert_dep(new TableDependency<std::string>(table));

  delete $1;
  delete $3; 
}
;

orLine: ATMARK OR {
  use_noisy_or=true;
 }
;

tickLine: ATMARK TICK {
  engine.tick_clock();
 }
;

tockLine: ATMARK TOCK {
  engine.ptock_clock();
 }
;

pmfLine: uniformPMF
| categoricalPMF
;

uniformPMF: PMFTOK LPAREN varTuple RPAREN LPAREN intTupleExpr RPAREN LPAREN intTupleExpr RPAREN UNIFORM {
  std::vector<std::string> labels($3->vars.begin(), $3->vars.end());
  if ($6->ints.size() != $9->ints.size() || $6->ints.size() != labels.size() || labels.size() != $9->ints.size())
    std::cerr << "ERROR: PMF error, number of variables, low bounds and high bounds do not match on line " << yylineno << std::endl;
  std::vector<long> shape;
  long flat_length = 1;
  for (unsigned long i=0; i<$6->ints.size(); ++i)
    shape.push_back($9->ints[i] - $6->ints[i] + 1);
  for (long dim : shape)
    flat_length *= dim; 
  std::vector<FLOAT_TYPE> flat_vect(flat_length, 1.0L/FLOAT_TYPE(flat_length));

  TablePreprocessor preprocessor(labels, $6->ints, shape, flat_vect, p, yylineno - 1);

  engine.insert_dep(new TableDependency<std::string>(preprocessor.table()));

  preprocessor.clear_memory();
  delete $6;
  delete $9;
  delete $3;
}
;

categoricalPMF: PMFTOK LPAREN varTuple RPAREN LPAREN intTupleExpr RPAREN floatTensor {
  std::vector<std::string> labels($3->vars.begin(), $3->vars.end());  
  std::reverse($8->shape.begin(),$8->shape.end());

  TablePreprocessor preprocessor($3->vars, $6->ints, $8->shape, $8->flat_vector, p, yylineno - 1);

  engine.insert_dep(new TableDependency<std::string>(preprocessor.table()));

  preprocessor.clear_memory();
  delete $8;
  delete $6;
  delete $3;
}
| PMFTOK LPAREN varTuple RPAREN LPAREN intTupleExpr RPAREN floatTensor ATMARK P EQUALS floatExpr {
  FLOAT_TYPE p_value = $12;
  assert(p_value>0);
  std::reverse($8->shape.begin(),$8->shape.end());

  TablePreprocessor preprocessor($3->vars, $6->ints, $8->shape, $8->flat_vector, p_value, yylineno - 1);

  engine.insert_dep(new TableDependency<std::string>(preprocessor.table()));

  preprocessor.clear_memory();
  delete $8;
  delete $6;
  delete $3;
}
;

printLine: PRINT LPAREN printBlock RPAREN {
  std::vector<std::vector<std::string> > result_vars = $3->vars;
  engine.print($3->vars);
  // we are finished with the print block now:
  delete $3;
}
| PRINT LPAREN STRING RPAREN {
  printf(":%s\n", $3);
  delete $3;
}
;

modelMetricLine: MODEL_METRIC LPAREN RPAREN {
  engine.print_model_metric($1);
}
| MODEL_METRIC LPAREN MULT RPAREN {
  engine.recompute_and_print_model_metric($1);
}
;

printBlock: printBlock SEMICOLON varTuple {
  $$->vars.push_back($3->vars);
  
  // we're now finished with the varTuple:
  delete $3;
}
| varTuple {
  $$ = new PrintBlock;
  $$->vars.push_back($1->vars);
  delete $1;
} 
;

saveGraphLine: GRAPH LPAREN STRING RPAREN {
  engine.save_graph($3);
  //note: when compiling, we get warnings for not capturing the exit code of the system call
  // todo: is there a better way to do this without an ugly system call?
  std::string instruction_a = std::string("python $EVERGREEN_SRC_PATH/Utility/draw_dot.py ") + $3 + std::string(" ") + $3 + std::string(".png");
  int exit_code = system(instruction_a.c_str());
  if (exit_code != 0) {
    std::cerr << instruction_a << " failed with exit-code " << exit_code << std::endl;
  }
  // todo: hard-coded for linux... need python executable that draws
  // the graph in interactive mode
  std::string instruction_b = std::string("xdg-open ") + $3 + std::string(".png");
  exit_code = system(instruction_b.c_str());
  if (exit_code != 0) {
    std::cerr << instruction_a << " failed with exit-code " << exit_code << std::endl;
  }
}
;

floatTensor: LBRACKET floatTensorTuple RBRACKET {
  $$ = $2;
}
| LBRACKET floatTupleExpr RBRACKET {
  $$ = $2;
}
;

floatTensorTuple: floatTensorTuple COMMA floatTensor {
  long array_in_tensor_length = $1->shape[$$->shape.size()-2];
  long array_2_in_tensor_length = $3->shape[$$->shape.size()-2];
  assert(array_in_tensor_length == array_2_in_tensor_length);
  $$ = $1;  
  for (FLOAT_TYPE flt: $3->flat_vector)
    $$->flat_vector.push_back(flt);
  $$->shape[$$->shape.size()-1] = $$->shape[$$->shape.size()-1]+1;
  delete $3;
}
| floatTensor {
  $$->shape.push_back(1);
}
;

floatTupleExpr: floatTupleExpr COMMA floatExpr {
  $$->flat_vector.push_back($3);
  $$->shape[0] = $$->shape[0]+1;
}
| floatExpr {
  $$ = new TensorLang;
  $$->shape = {1};
  $$->flat_vector.push_back($1);
}
;

addSubExpr: addSubExpr addSubTerm {
  if ($2->sign == true) {
    $$->plus_vars.push_back($2->vars);
  } 
  else {
    $$->minus_vars.push_back($2->vars);      
  }
  delete $2;
}
| addSubTerm {
  $$ = new AddSubExpr;
  if ($1->sign == true) {
    $$->plus_vars.push_back($1->vars);
  } 
  else {
    $$->minus_vars.push_back($1->vars);      
  }
  delete $1;
}
;

addSubTerm: ADD additiveTuple {
  $$ = new AddSubTerm;
  $$->sign = true; 
  $$->vars = $2->vars;
  delete $2;
}
| SUB additiveTuple {
  $$ = new AddSubTerm;
  $$->sign = false; 
  $$->vars = $2->vars;
  delete $2;
}
| additiveTuple {
  $$ = new AddSubTerm;
  $$->sign = true; 
  $$->vars = $1->vars;
  delete $1;
}
;

additiveTuple: additiveTuple COMMA additiveTerm {
  $$ = $1;
  $$->vars.push_back($3);
  free($3);
}
| additiveTerm {
  $$ = new VarTuple;
  $$->vars.push_back($1);
  free($1);      
} 
;

additiveTerm: VAR
| floatMultDivExpr {

  std::vector<FLOAT_TYPE> flat_vect = {1};
  std::vector<long> shape = {1};
  std::vector<long> offsets = {(long)$1};
  std::vector<std::string> labels = {to_string((long)$1) + "_" + to_string(node_index)};

  TablePreprocessor preprocessor(labels, offsets, shape, flat_vect, p, yylineno);

  engine.insert_dep(new TableDependency<std::string>(preprocessor.table()));

  std::string node_str = to_string((long)$1) + "_" + to_string(node_index);
  $$ = new char[node_str.length() + 1];
  strcpy($$, node_str.c_str());
  
  preprocessor.clear_memory();
  node_index++;
}
;

intTupleExpr: intTupleExpr COMMA intExpr {
  $$ = $1;
  $$->ints.push_back($3);
}
| intExpr {
  $$ = new IntTuple;
  $$->ints.push_back($1);
}
;

varTuple: varTuple COMMA VAR {
  $$ = $1;
  $$->vars.push_back($3);
  free($3);
}
| VAR {
  $$ = new VarTuple;
  $$->vars.push_back($1);
  free($1);
}
;

VAR: VARNAME
| SPECIAL_VARNAME

/********************************
Expressions and Numbers:
********************************/

intExpr: floatExpr{
  $$ = (long)$1;
}
;

floatExpr: floatAddSubExpr
;

floatAddSubExpr: floatAddSubExpr ADD floatMultDivExpr {
  $$ = $1 + $3;
}
| floatAddSubExpr SUB floatMultDivExpr {
  $$ = $1 - $3;
}
| ADD floatMultDivExpr {
  $$ = $2;
}
| SUB floatMultDivExpr {
  $$ = -$2;
}
| floatMultDivExpr
;

floatMultDivExpr: floatMultDivExpr MULT atomicExpr {
	$$ = $1 * $3;
}
| floatMultDivExpr DIV atomicExpr {
	if ($3 == 0) {
	  std::cerr << "ERROR: Cannot divide by 0 on line " << yylineno << std::endl;
	  exit(1);
	}
	else {
  	  $$ = $1 / $3;
	}
}
| atomicExpr
;

atomicExpr: floatInput
| LPAREN floatExpr RPAREN {
	$$ = $2;
}
;

floatInput: FLOAT
| UNSIGNED_INT
;

%%

int main(int argc, char **argv) {
  FILE *fh;
  if (argc == 2 && (fh = fopen(argv[1], "r")))
    yyin = fh;
  //yydebug = 1; //(uncomment to enable debugging)
  yyparse();

  //  fclose(fh);
  return 0;
}

/* Display error messages */
void yyerror(const char *s) {
  
  fprintf(stderr, "ERROR: %s on line %d\n", s, yylineno);
}
