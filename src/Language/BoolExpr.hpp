typedef struct BoolExprT {
  private:
    std::vector<std::vector<std::string> > _left_vars;
    std::vector<std::vector<std::string> > _right_vars;
    std::vector<std::string> _vars_used;
    FLOAT_TYPE _p;
    std::string _op; // can be <=, >=, <, >, or ==.
    std::set<std::string> _possible_ops = {"<=", "<", "=", ">", ">=", "or"};
    bool _use_noisy_or;	

  public:
  BoolExprT(const AddSubExpr * left, const AddSubExpr * right, const std::string & op, const FLOAT_TYPE & p, const int & yylineno, const bool & use_noisy_or) {
    
    for (std::vector<std::string> plus_var : left->plus_vars) {
      _left_vars.push_back(plus_var);
      for (std::string var : plus_var)  _vars_used.push_back(var);
    }
    for (std::vector<std::string> minus_var : left->minus_vars) {
      _right_vars.push_back(minus_var);
      for (std::string var : minus_var) _vars_used.push_back(var);  
    }
    for (std::vector<std::string> plus_var : right->plus_vars) {
      _right_vars.push_back(plus_var);
      for (std::string var : plus_var)  _vars_used.push_back(var);
    }
    for (std::vector<std::string> minus_var : right->minus_vars) {
      _left_vars.push_back(minus_var);
      for (std::string var : minus_var) _vars_used.push_back(var);     
    }

    if (_left_vars.size() == 0 || _right_vars.size() == 0)
      std::cerr << "ERROR: boolean expression error, summing out all negative variables produces no variables on one side of the expression on line " << yylineno <<  std::endl;       

    unsigned long num_dimensions = _left_vars[0].size();

    for (const std::vector<std::string> & tup : _left_vars)
      if (tup.size() != num_dimensions) {
        std::cerr << "ERROR: boolean expression error, Not all tuples of the expression share the same length on line " << yylineno << std::endl;
      break;
      }
    for (const std::vector<std::string> & tup : _right_vars)
      if (tup.size() != num_dimensions) {
        std::cerr << "ERROR: boolean expression error, Not all tuples of the expression share the same length on line " << yylineno << std::endl;
      break;
      }

    _use_noisy_or = use_noisy_or;
    assert(_possible_ops.find(op) != _possible_ops.end());
    _op = op;
    _p = p;
  }
  
  const std::vector<std::string> & vars_used() const {
    return _vars_used;    
  }

  const std::vector<std::vector<std::string> > & left_vars() const {
    return _left_vars;
  }

  const std::vector<std::vector<std::string> > & right_vars() const {
    return _right_vars;
  }

  const FLOAT_TYPE & p() const {
    return _p;
  }

  const std::string & op() const {
    return _op;
  }

  const bool & use_noisy_or() const {
    return _use_noisy_or;
  }

} BoolExpr;
