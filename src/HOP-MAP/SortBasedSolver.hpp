#include <vector>
#include <math.h> 
#include <algorithm>
#include <assert.h>
#include <set>

#include "../Utility/LogDouble.hpp"

typedef LogDouble T;

class SortBasedSolver{
private:
  const std::vector<std::pair<T, T> > _input_probs;
  const std::vector<T> _output_probs;
  const unsigned long _number_outcomes;
  const unsigned long _number_variables;
  long _MAP_y_value;
  std::vector<long> _input_index_to_sorted_index;
  std::vector<long> _sorted_index_to_input_index;
  std::vector<std::pair<std::pair<T, T>, int> > _sorted_prob_and_index;
  std::vector<T> _prefixes;
  std::vector<T> _suffixes;
  std::set<long> _y_used;
  std::vector<T> _max_prob_from_below_x_zero;
  std::vector<T> _max_prob_from_above_x_zero;
  std::vector<T> _max_prob_from_below_x_one;
  std::vector<T> _max_prob_from_above_x_one; 


  static bool compare_for_sorting_input_probs(const std::pair<std::pair<T, T>, int> &lhs, const std::pair<std::pair<T, T>, int> &rhs) {
    return (lhs.first.second > rhs.first.second);
  }

  void sort_input_probabilities() {
    // Sort inputs while keeping original variable indices
    _sorted_prob_and_index.resize(_number_variables);
    _input_index_to_sorted_index.resize(_number_variables);
    _sorted_index_to_input_index.resize(_number_variables);
    for (unsigned long i=0; i< _number_variables; ++i)
      _sorted_prob_and_index[i] = std::make_pair(_input_probs[i], i);
    std::sort(_sorted_prob_and_index.begin(), _sorted_prob_and_index.end(), compare_for_sorting_input_probs);

    for (unsigned long i=0; i<_number_variables; ++i) {
      long unsorted_index = _sorted_prob_and_index[i].second;
      _input_index_to_sorted_index[unsorted_index] = i;
      _sorted_index_to_input_index[i] = unsorted_index;
    }
  }

  void calculate_prefixes() {
    _prefixes.resize(_number_outcomes);
    _prefixes[0] = T(1.0);
    for (unsigned long i=1; i<_number_outcomes; ++i)
      _prefixes[i] = _prefixes[i-1] * _sorted_prob_and_index[i-1].first.second;
  }
  
  void calculate_suffixes() {
    _suffixes.resize(_number_outcomes);
    _suffixes[_number_outcomes - 1] = T(1.0);
    for (long i =_number_outcomes-2; i>=0; --i)
      _suffixes[i] = _suffixes[i+1] * _sorted_prob_and_index[i].first.first;
  }

  void compute_and_print_map() {
    std::vector<T> max_marginal_y(_number_outcomes);
    // Get max-marginal distribution on N
    for (unsigned long i=0; i< _number_outcomes; ++i)
      max_marginal_y[i] = _output_probs[i] *  _prefixes[i] * _suffixes[i];
    
    // Find MAP of N
    unsigned long number_present=0;
    for (unsigned long i=0; i< _number_outcomes; ++i)
      if (max_marginal_y[i] > max_marginal_y[number_present])
        number_present = i;
    _MAP_y_value = number_present;
    // first best_index indices of _sorted_prob_and_index are 1, others are 0
    
    T nrm_const = T(0.0);
    for (unsigned long i=0; i < max_marginal_y.size(); ++i)
      nrm_const += max_marginal_y[i];

    //unsigned long max_index = 0;
    T max_value = T(-1.0/0.0);
    std::cout << "Y [0] ";
    for (unsigned long i=0; i< _number_outcomes-1; ++i) {
      max_marginal_y[i]= max_marginal_y[i]/nrm_const;
      std::cout << double(max_marginal_y[i]) << ", ";
      if (max_marginal_y[i] > max_value) {
	max_value = max_marginal_y[i];
	//max_index = i;
      }
    }

    unsigned long i = _number_outcomes-1;
    max_marginal_y[i]= max_marginal_y[i]/nrm_const;
    std::cout << double(max_marginal_y[i]);
    if (max_marginal_y[i] > max_value) {
      max_value = max_marginal_y[i];
      //max_index = i;
    }
    std::cout << std::endl;

  }

  void calc_and_cache_max_prefixes_and_suffixes() {
    T max_value_from_below_x_zero = T(0);
    for (unsigned long i=0; i < _number_variables; ++i) {
      long y = i; // y=i because this is for y< index of variable, but variables in paper are indexed base-0
      T prob_Y_eq_y = T(_output_probs[y] * _prefixes[y] * _suffixes[y]);

      max_value_from_below_x_zero = std::max(max_value_from_below_x_zero, prob_Y_eq_y);
      _max_prob_from_below_x_zero[i] = max_value_from_below_x_zero;
    }

    T max_value_from_above_x_zero = T(0);
    for (long i=_number_variables-1; i >=0; --i) {
      long y = i+1; //y=i+1 because this is for y>= index of variable, but variables in paper are indexed base-0
      if (y == long(_number_variables)) {
				_max_prob_from_above_x_zero[i] = T(0);
      }
      else {
        T prob_Y_eq_y = T(_output_probs[y] * _prefixes[y+1] * _suffixes[y+1]);
        max_value_from_above_x_zero = std::max(max_value_from_above_x_zero, prob_Y_eq_y);
        _max_prob_from_above_x_zero[i] = max_value_from_above_x_zero;
      }
    }

    T max_value_from_below_x_one = T(0);
    for (unsigned long i=0; i < _number_variables; ++i) {
      long y = i;
      if (y == 0) {
				_max_prob_from_below_x_one[i] = T(0);
      }
      else{
				T prob_Y_eq_y = T(_output_probs[y] * _prefixes[y-1] * _suffixes[y-1]);
				max_value_from_below_x_one = std::max(max_value_from_below_x_one, prob_Y_eq_y);
				_max_prob_from_below_x_one[i] = max_value_from_below_x_one;
      }
    }
		
    T max_value_from_above_x_one = T(0);
    for (long i=_number_variables-1; i >=0; --i) {
      long y = i+1; //y=i+1 because this is for y>= index of variable, but variables in paper are indexed base-0
      T prob_Y_eq_y = T(_output_probs[y] * _prefixes[y] * _suffixes[y]);
      max_value_from_above_x_one = std::max(max_value_from_above_x_one, prob_Y_eq_y);
      _max_prob_from_above_x_one[i] = max_value_from_above_x_one;
    }
  }

public:
  SortBasedSolver(std::vector<std::pair<T, T> > &input_probs, std::vector<T> &output_probs):
    _input_probs(input_probs),
    _output_probs(output_probs),
    _number_outcomes(output_probs.size()),
    _number_variables(input_probs.size()),
    _max_prob_from_below_x_zero(output_probs.size()),
    _max_prob_from_above_x_zero(output_probs.size()),
    _max_prob_from_below_x_one(output_probs.size()),
    _max_prob_from_above_x_one(output_probs.size())
    {
      // Note: could be relaxed in the future:
      assert(_input_probs.size()+1 == _output_probs.size());
      sort_input_probabilities();
      calculate_prefixes();
      calculate_suffixes();
    }

  void get_map() {
    compute_and_print_map();
  }

  void get_all_marginals() {
    calc_and_cache_max_prefixes_and_suffixes();
    for (unsigned long i=0; i<_number_variables; ++i) {
      // Index i is supposed to be index of X_i
      T prob_x_i_is_zero = _sorted_prob_and_index[i].first.first;
      T prob_x_i_is_one  = _sorted_prob_and_index[i].first.second;
      
      T max_marg_x_i_is_zero = std::max(_max_prob_from_below_x_zero[i],  _max_prob_from_above_x_zero[i] * (prob_x_i_is_zero / prob_x_i_is_one));
      T max_marg_x_i_is_one  = std::max(_max_prob_from_below_x_one[i] * (prob_x_i_is_one / prob_x_i_is_zero), _max_prob_from_above_x_one[i]);

      T normalization_constant = max_marg_x_i_is_zero + max_marg_x_i_is_one;
      std::cout << "X" << _sorted_index_to_input_index[i] << " [0] " << double(max_marg_x_i_is_zero/normalization_constant) << ", " << double(max_marg_x_i_is_one/normalization_constant) << std::endl;
    }
  }
};
