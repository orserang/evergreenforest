#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <fstream>
#include <assert.h>
#include <string.h>
#include <fstream>
#include <sstream>

#include "SortBasedSolver.hpp"

void normalize_probabilities(std::vector<double> &probs, unsigned k){
  double normalization_constant;
  for (unsigned i=0; i < probs.size(); i += k){
    normalization_constant = 0.0;
    for (unsigned j = 0; j < k; ++j)
      normalization_constant += probs[i+j];
    for (unsigned j = 0; j < k; ++j)
      probs[i + j] /= normalization_constant;
  }
}

std::vector<std::pair<double, double> > vector_to_vector_of_pairs(std::vector<double> &v){
  std::vector<std::pair<double, double> > out;
  for (unsigned i=0; i < v.size()/2; ++i)
    out.push_back( std::make_pair(v[2*i], v[2*i+1] ) );
  return out;
}


void get_map(std::vector<double> &input_probs, std::vector<double> &output_probs){
  std::vector< std::pair<LogDouble, LogDouble> > input_probs_as_pairs_log_double;
  std::vector<LogDouble> output_probs_log_double;

  for (auto &prob : input_probs){
    LogDouble input_prob_first  = LogDouble(prob);
    LogDouble input_prob_second = LogDouble(1.0-prob);
    input_probs_as_pairs_log_double.emplace_back(std::make_pair(input_prob_first, input_prob_second));
  }

  for (auto &output_prob_double : output_probs)
    output_probs_log_double.push_back(LogDouble(output_prob_double));

  SortBasedSolver solver(input_probs_as_pairs_log_double, output_probs_log_double);
  Clock c;
  solver.get_map();
  c.ptock();
}


void get_marginals_and_map(std::vector<double> &input_probs, std::vector<double> &output_probs){
  std::vector< std::pair<LogDouble, LogDouble> > input_probs_as_pairs_log_double;
  std::vector<LogDouble> output_probs_log_double;

  for (auto &prob : input_probs){
    LogDouble input_prob_first = LogDouble(prob);
    LogDouble input_prob_second = LogDouble(1.0-prob);
    input_probs_as_pairs_log_double.push_back(std::make_pair(input_prob_first, input_prob_second));
  }

  for (auto &output_prob_double : output_probs)
    output_probs_log_double.push_back(LogDouble(output_prob_double));
  
  SortBasedSolver solver(input_probs_as_pairs_log_double, output_probs_log_double);
  Clock c;
  solver.get_all_marginals();
  solver.get_map();
  c.ptock();
}
