#include <fstream>
#include <random>

#include "../Evergreen/evergreen.hpp"
#include "../Utility/inference_utilities.hpp"
#include "sort_based_solver.hpp"

int main(int argc, char**argv){
  if (argc < 2) {
    std::cerr << "Usage:<n>" << std::endl;
    exit(1);
  }

  unsigned long N = atoi(argv[1]);

  std::random_device rd;
  std::uniform_int_distribution<int> ud(1,65533);
  std::mt19937 mt(rd());

  auto seed = ud(mt);
  srand(seed);
  std::vector<FLOAT_TYPE> input_probs;
  std::vector<FLOAT_TYPE> output_probs;

	for (unsigned long i=0; i < N; ++i)
		input_probs.push_back(((double) rand() / (RAND_MAX)));
	
	for (unsigned long i=0; i < N+1; ++i)
		output_probs.push_back(((double) rand() / (RAND_MAX)));
	
	get_marginals_and_map(input_probs, output_probs);
};
