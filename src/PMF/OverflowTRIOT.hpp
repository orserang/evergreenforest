#ifndef _OVERFLOWTRIOT_HPP
#define _OVERFLOWTRIOT_HPP

#include "OverflowTemplateSearch.hpp"
#include "../Tensor/TensorUtils.hpp"
#include <vector>

// TODO: Can a namespace casing like this improve compilation time
// by restricting lookup of template classes within this
// namespace?

// TODO: Would explicit template arguments enable faster compilation?


inline unsigned long support_to_index(signed_const_tup_t tup, const_tup_t shape, const Vector<long> &first_support, unsigned char dimension) {
  long res = 0;
  unsigned char k;
  for (k=1; k<dimension; ++k) {
    res += (tup[k-1] - first_support[k-1]);
    res *= shape[k];
  }
  res += tup[k-1] - first_support[k-1];
  return res;
}

template <unsigned int DIMENSION>
inline unsigned long support_to_index_fixed_dimension(signed_tup_t tup, signed_const_tup_t first_support, const_tup_t shape) {
  long res = 0;
  unsigned int k;
  for (k=1; k<DIMENSION; ++k) {
    res += (tup[k-1] - first_support[k-1]);
    res *= shape[k];
  }
  res += tup[k-1] - first_support[k-1];
  return res;
}


namespace OverflowTRIOT {
  //---------------------------------------------
  // For each, with visible counter:
  //---------------------------------------------


  /////////////////////////////////////////////////////////////////////////

  // todo: Do one with <unsigned char DIMENSION, unsigned char CURRENT, 1c> so that can do ++counter[CURRENT]
  // and one with <unsigned char DIMENSION, unsigned char CURRENT, -1c> so that can do --counter[CURRENT]
  template <unsigned char DIMENSION, unsigned char CURRENT, char DELTA>
  class OverflowForEachVisibleCounterFixedDimensionHelper {
  public:
    template <typename FUNCTION, typename ...TENSORS>
    inline static void apply(signed_tup_t counter, const_tup_t shape, signed_const_tup_t lower_bound, signed_const_tup_t upper_bound, signed_const_tup_t first_support, FUNCTION function, TENSORS & ...args) {
      for (counter[CURRENT] = lower_bound[CURRENT]; DELTA*(counter[CURRENT]) < DELTA*(upper_bound[CURRENT]); counter[CURRENT] += DELTA){
        OverflowForEachVisibleCounterFixedDimensionHelper<DIMENSION-1, CURRENT+1, DELTA>::template apply<FUNCTION, TENSORS...>(counter, shape, lower_bound, upper_bound, first_support, function, args...);
      }
    }
  };
  /////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////  

  // fixme: Do one with <unsigned char DIMENSION, unsigned char CURRENT, 1c> so that can do ++counter[CURRENT]
  // and one with <unsigned char DIMENSION, unsigned char CURRENT, -1c> so that can do --counter[CURRENT]
  template <unsigned char CURRENT, char DELTA>
  class OverflowForEachVisibleCounterFixedDimensionHelper<1u, CURRENT, DELTA> {
  public:
    template <typename FUNCTION, typename ...TENSORS>
    inline static void apply(signed_tup_t counter, const_tup_t shape, signed_const_tup_t lower_bound, signed_const_tup_t upper_bound, signed_const_tup_t first_support, FUNCTION function, TENSORS & ...args) {
      for (counter[CURRENT] = (lower_bound[CURRENT]); DELTA*(counter[CURRENT]) < DELTA*(upper_bound[CURRENT]); counter[CURRENT] += DELTA){
	function(static_cast<signed_const_tup_t>(counter), CURRENT+1, args[support_to_index_fixed_dimension<CURRENT+1>(counter, first_support, shape)]...);
      }
    }
  };
  /////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////

  template <unsigned char CURRENT, char DELTA>
  class OverflowForEachVisibleCounterFixedDimensionHelper<0u, CURRENT, DELTA> {
  public:
    template <typename FUNCTION, typename ...TENSORS>
    inline static void apply(signed_tup_t counter, const_tup_t shape, signed_const_tup_t lower_bound, signed_const_tup_t upper_bound, signed_const_tup_t first_support, FUNCTION function, TENSORS & ...args) {
      // Do nothing
    }
  };
  /////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////

  template <unsigned char DIMENSION, char DELTA>
  class OverflowForEachVisibleCounterFixedDimension {
  public:
    template <typename FUNCTION, typename ...TENSORS>
    inline static void apply(const_tup_t shape, signed_const_tup_t lower_bound, signed_const_tup_t upper_bound, signed_const_tup_t first_support, FUNCTION function, TENSORS & ...args) {
      long counter[DIMENSION];
      memset(counter, 0, DIMENSION*sizeof(unsigned long));
      OverflowForEachVisibleCounterFixedDimensionHelper<DIMENSION,0,DELTA>::template apply<FUNCTION, TENSORS...>(counter, shape, lower_bound, upper_bound, first_support, function, args...);
    }
  };

  /////////////////////////////////////////////////////////////////////////  
  template<char DELTA>
  class OverflowForEachVisibleCounterFixedDimension<0U, DELTA>	{
  public:
    template <typename FUNCTION, typename ...TENSORS>
    inline static void apply(const_tup_t shape, signed_const_tup_t lower_bound, signed_const_tup_t upper_bound, signed_const_tup_t first_support, FUNCTION function, TENSORS & ...args) {
		// do nothing, so that memset is not called with size = 0 which is a GCC extension
	}
  };
}

template <typename FUNCTION, typename ...TENSORS>
void overflow_enumerate_for_each_tensors_increase(FUNCTION function, const Vector<unsigned long> & shape, const Vector<long> & lower_bounds, const Vector<long> & upper_bounds, signed_const_tup_t first_support, TENSORS & ...args){
  // Same as enumerate_for_each_tensors but over supports, not indices
  OverflowLinearTemplateSearch<0u,MAX_TENSOR_DIMENSION,1,OverflowTRIOT::OverflowForEachVisibleCounterFixedDimension>::apply(shape.size(), shape, lower_bounds, upper_bounds, first_support, function, args...);
}

template <typename FUNCTION, typename ...TENSORS>
void overflow_enumerate_for_each_tensors_decrease(FUNCTION function, const Vector<unsigned long> & shape, const Vector<long> & lower_bounds, const Vector<long> & upper_bounds, signed_const_tup_t first_support, TENSORS & ...args){
  // Same as enumerate_for_each_tensors but over supports, not indices
  OverflowLinearTemplateSearch<0u,MAX_TENSOR_DIMENSION,-1,OverflowTRIOT::OverflowForEachVisibleCounterFixedDimension>::apply(shape.size(), shape, upper_bounds, lower_bounds, first_support, function, args...); // switch upper_bounds, lower_bounds
}

#endif
