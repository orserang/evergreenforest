#ifndef _DIVERGENCE_HPP
#define _DIVERGENCE_HPP

#include "squared.hpp"
#include "../Utility/FLOAT_TYPE.hpp"

template <template <typename> class TENSOR_LHS, template <typename> class TENSOR_RHS>
FLOAT_TYPE se(const TensorLike<FLOAT_TYPE, TENSOR_LHS> & lhs, const TensorLike<FLOAT_TYPE, TENSOR_RHS> & rhs) {
  #ifdef SHAPE_CHECK
  assert( lhs.view_shape() == rhs.view_shape() );
  #endif
  
  FLOAT_TYPE tot = 0.0L;

  for_each_tensors([&tot](FLOAT_TYPE lhs_val, FLOAT_TYPE rhs_val){
      tot += squared(lhs_val - rhs_val);
    },
    lhs.view_shape(),
    lhs, rhs);

  return tot;
}

template <typename VARIABLE_KEY>
class LabeledPMF;

template <typename VARIABLE_KEY>
FLOAT_TYPE mse_divergence(const LabeledPMF<VARIABLE_KEY> & lhs, const LabeledPMF<VARIABLE_KEY> & rhs) {
  #ifdef SHAPE_CHECK
  assert(lhs.has_same_variables(rhs));
  #endif
  
  std::pair<TensorView<FLOAT_TYPE>, Vector<long> > lhs_view_and_first_sup = lhs.view_of_intersection_with(rhs);
  std::pair<TensorView<FLOAT_TYPE>, Vector<long> > rhs_view_and_first_sup = rhs.view_of_intersection_with(lhs);

  const TensorView<FLOAT_TYPE> & lhs_view = lhs_view_and_first_sup.first;
  const TensorView<FLOAT_TYPE> & rhs_view = rhs_view_and_first_sup.first;
  
  FLOAT_TYPE lhs_view_mass = 0.0L;
  for_each_tensors([&lhs_view_mass](FLOAT_TYPE val){
      lhs_view_mass += val;
    },
    lhs_view.view_shape(),
    lhs_view
    );
  FLOAT_TYPE rhs_view_mass = 0.0L;
  for_each_tensors([&rhs_view_mass](FLOAT_TYPE val){
      rhs_view_mass += val;
    },
    rhs_view.view_shape(),
    rhs_view
    );
  
  FLOAT_TYPE nonintersecting_se = squared(1.0L - lhs_view_mass) + squared(1.0L - rhs_view_mass);

  FLOAT_TYPE intersecting_se;
  if (lhs.ordered_variables() == rhs.ordered_variables()) {
    // variables are in the same order; no need to transpose:
    intersecting_se = se(lhs_view, rhs_view);
  }
  else {
    // transpose rhs to get variables in the same order:
    Tensor<FLOAT_TYPE> rhs_part(rhs_view);
    Vector<unsigned int> new_rhs_order = rhs.lookup_indices(lhs.ordered_variables());
    transpose(rhs_part, new_rhs_order);
    
    intersecting_se = se(lhs_view_and_first_sup.first, rhs_part);
  }

  // Note: lhs_view.flat_size() == rhs_view.flat_size()
  return (nonintersecting_se + intersecting_se) / (lhs.pmf().table().flat_size() + rhs.pmf().table().flat_size() - lhs_view.flat_size());
}

#endif
