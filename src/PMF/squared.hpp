#ifndef _SQUARED_HPP
#define _SQUARED_HPP

#include "../Utility/FLOAT_TYPE.hpp"

inline FLOAT_TYPE squared(FLOAT_TYPE x) {
  return x*x;
}

#endif
