#ifndef _KL_DIVERGENCE_HPP
#define _KL_DIVERGENCE_HPP

#include "../Utility/FLOAT_TYPE.hpp"

// 6/14/19 Note: Most of this code is taken from divergence.hpp, as such, this and divergence  could
// likely be refactored to reuse similiar code. 

// TODO: figure out good tolerance for different types
//#ifdef FAST_FLOAT
//    float ZERO_TOLERANCE = ;
//  #else
//    #ifdef ACCURATE_FLOAT
//      long double ZERO_TOLERANCE = ;
//    #else
      // standard
//      double ZERO_TOLERANCE = ;
//    #endif
//  #endif

template <template <typename> class TENSOR_LHS, template <typename> class TENSOR_RHS>
FLOAT_TYPE kl(const TensorLike<FLOAT_TYPE, TENSOR_LHS> & lhs, const TensorLike<FLOAT_TYPE, TENSOR_RHS> & rhs) {
  #ifdef SHAPE_CHECK
  assert( lhs.view_shape() == rhs.view_shape() );
  #endif
  
  FLOAT_TYPE tot = 0.0L;
  for_each_tensors([&tot](FLOAT_TYPE lhs_val, FLOAT_TYPE rhs_val) {
    
    // Note: lhs = P(X), rhs = Q(X). So, rhs emits lhs

    // TODO: put in tolerance for zero?
    // TODO: will continue statement result in loss of effeciency? (may mess up branch predicitions?) 
    if (lhs_val == 0.0L) {
      // continue;
      tot += FLOAT_TYPE(0.0L);

    //0 -> finite = \inf entropy
    } else if (rhs_val == 0.0L) {

      if (lhs_val > 0.0L)
	// Note: could simply return early here, however this return causes the lambda to be of
	// non-void type and so the compiler throws a warning(warning control reaches end of
	// non-void function)
	tot += std::numeric_limits<FLOAT_TYPE>::infinity();
	//return std::numeric_limits<FLOAT_TYPE>::infinity();

      else // lhs == 0 && rhs == 0
	// continue;
	tot += FLOAT_TYPE(0.0L);
    
    } else {
      tot += lhs_val * ( logl(lhs_val) - logl(rhs_val) );
    }
    
    },
    lhs.view_shape(),
    lhs, rhs);

  return tot;
}

template <typename VARIABLE_KEY>
class LabeledPMF;

template <typename VARIABLE_KEY>
FLOAT_TYPE kl_divergence(const LabeledPMF<VARIABLE_KEY> & lhs, const LabeledPMF<VARIABLE_KEY> & rhs) {
  #ifdef SHAPE_CHECK
  assert(lhs.has_same_variables(rhs));
  #endif

  // Get intersecting tensor views 
  std::pair<TensorView<FLOAT_TYPE>, Vector<long> > lhs_view_and_first_sup = lhs.view_of_intersection_with(rhs);
  std::pair<TensorView<FLOAT_TYPE>, Vector<long> > rhs_view_and_first_sup = rhs.view_of_intersection_with(lhs);

  const TensorView<FLOAT_TYPE> & lhs_view = lhs_view_and_first_sup.first;
  const TensorView<FLOAT_TYPE> & rhs_view = rhs_view_and_first_sup.first;

  FLOAT_TYPE intersecting_kl;
  if (lhs.ordered_variables() == rhs.ordered_variables()) {
    
    // variables are in the same order; no need to transpose:  
    intersecting_kl = kl(lhs_view, rhs_view);
  }
  else {
    // transpose rhs to get variables in the same order:
    Tensor<FLOAT_TYPE> rhs_part(rhs_view);
    Vector<unsigned int> new_rhs_order = rhs.lookup_indices(lhs.ordered_variables());
    transpose(rhs_part, new_rhs_order);
    
    intersecting_kl = kl(lhs_view_and_first_sup.first, rhs_part);
  }
  return intersecting_kl;
}

#endif
