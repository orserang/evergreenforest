#ifndef _OVERFLOW_TRIMMING_HPP_
#define _OVERFLOW_TRIMMING_HPP_

#include "../Tensor/TensorUtils.hpp"
#include "OverflowTRIOT.hpp"

inline FLOAT_TYPE p_add_single_values(const FLOAT_TYPE lhs, const FLOAT_TYPE rhs, const FLOAT_TYPE p){
  FLOAT_TYPE max_val = std::max(lhs, rhs);
  return max_val * ::pow(( ::pow((lhs/max_val), p) +  ::pow((rhs/max_val), p)), 1.0/p );
}

inline tup_t support_to_position_in_tensor(const Tensor<FLOAT_TYPE> &result, signed_const_tup_t result_first_support, signed_const_tup_t support){
  // Support to position in tensor (not flattened Vector)
  unsigned long* position = (unsigned long*) malloc(sizeof(unsigned long) * result.dimension());
  for (unsigned long i = 0; i < result.dimension(); ++i)
    position[i] = support[i] - result_first_support[i];
  return tup_t(position);
}

inline tup_t support_to_position_in_tensor(const Tensor<FLOAT_TYPE> &result, const Vector<long> &result_first_support, signed_const_tup_t support){
  return support_to_position_in_tensor(result,  &result_first_support[0], support);
}

inline tup_t support_to_position_in_tensor(const Tensor<FLOAT_TYPE> &result, const Vector<long> &result_first_support,  const Vector<long> &support){
  return support_to_position_in_tensor(result,  &result_first_support[0], &support[0]);
}

inline tup_t support_to_position_in_pmf(const PMF &pmf, signed_const_tup_t support){
  return support_to_position_in_tensor(pmf.table(), pmf.first_support(), support);
}

inline unsigned long tensor_tuple_to_index(const Tensor<FLOAT_TYPE> & tensor, const_tup_t tup) {
  return tuple_to_index(tup, tensor.data_shape(), tensor.dimension());
}

inline void move_support_in_tensor(Tensor<FLOAT_TYPE> &result, const Vector<long> &result_first_support, signed_const_tup_t const_source_support, signed_const_tup_t const_dest_support, const FLOAT_TYPE p){

  tup_t source_position =  support_to_position_in_tensor(result, result_first_support, const_source_support);
  unsigned long source_index = tensor_tuple_to_index(result, source_position);

  tup_t dest_position = support_to_position_in_tensor(result, result_first_support, const_dest_support);
  unsigned long dest_index = tensor_tuple_to_index(result, dest_position);

  const FLOAT_TYPE source_val = result[source_index];
  const FLOAT_TYPE dest_val   = result[dest_index];        
  const FLOAT_TYPE max_val    = std::max(source_val, dest_val);
  const FLOAT_TYPE out_val    = max_val * ::pow(( ::pow((source_val/max_val), p) +  ::pow((dest_val/max_val), p)), 1.0/p );

  result[dest_index] = out_val;
  result[source_index] = 0;
}

void add_prob_in_tensor(Tensor<FLOAT_TYPE> &result, const Vector<long> &result_first_support, signed_const_tup_t support, const FLOAT_TYPE val, const FLOAT_TYPE p){
  tup_t position = support_to_position_in_tensor(result, result_first_support, support);
  const unsigned long index = tensor_tuple_to_index(result, const_tup_t(position));
  const FLOAT_TYPE source_val = val;        
  const FLOAT_TYPE dest_val = result[index];
  const FLOAT_TYPE out_val = p_add_single_values(source_val, dest_val, p);
  result[index] = out_val;
}

void add_overflow_underflow(const PMF &lhs, const PMF &rhs, Tensor<FLOAT_TYPE> &result, const Vector<long> &minimum_possible_first_support, const Vector<long> &maximum_possible_last_support, const Vector<long> &current_result_first_support, const Vector<long> &current_result_last_support, const FLOAT_TYPE p){
  enumerate_for_each_tensors([&lhs, &rhs, &result, &p, &minimum_possible_first_support, &maximum_possible_last_support, &current_result_first_support, &current_result_last_support](const_tup_t counter, const unsigned char dim, double value){
      Vector<long> source_support(current_result_first_support.size());//, counter);
      Vector<long> destination_support(current_result_first_support.size());//, counter);
      for (int i=0; i < dim; ++i){
	source_support[i] = counter[i] + current_result_first_support[i];
	destination_support[i] = std::min(source_support[i], maximum_possible_last_support[i]);
	destination_support[i] = std::max(destination_support[i], minimum_possible_first_support[i]);
      }
      bool move_support = false;
      for (int i=0; i < dim; ++i){
	if (source_support[i] != destination_support[i]){
	  move_support = true;
	  break;
	}
      }
      if (move_support)
	move_support_in_tensor(result, current_result_first_support, source_support, destination_support, p);
    }, 
    result.data_shape(),
    result);
}


inline void first_supports_to_cause_overflow_in_forward_pass(Vector<long> &first_result_support_for_overflow, Vector<bool> &this_dimension_has_overflow, const PMF &parent, const PMF &sibling, const Vector<long> &result_first_support, const Vector<long> &result_last_support){
  for (unsigned dim = 0; dim < first_result_support_for_overflow.size(); ++dim){
    for (long result_support = result_last_support[dim]; result_support >= result_first_support[dim]; --result_support){
      if (sibling.last_support()[dim] + result_support > parent.last_support()[dim]){
	this_dimension_has_overflow[dim] = true;
        first_result_support_for_overflow[dim] = result_support;
      }
    }
  }
}

inline bool support_in_bounds_for_pmf(const PMF &pmf, const Vector<long> &support){
  const Vector<long> pmf_last_legal_support = pmf.last_support();
  for (unsigned char i=0; i < support.size(); ++i)
    if (support[i] > pmf_last_legal_support[i] || support[i] < pmf.first_support()[i])
      return false;
  return true;
}

void find_nearest_in_bound_support(const PMF &pmf, Vector<long> &support){
  apply_vectors([](long &support_in_dimension, const long &first_legal_support, const long &last_legal_support){
      support_in_dimension = std::max(support_in_dimension, first_legal_support);
      support_in_dimension = std::min(support_in_dimension, last_legal_support);
    }, support.size(), support, pmf.first_support(), pmf.last_support());
}

inline void first_supports_to_cause_underflow_in_forward_pass(Vector<long> &first_result_support_for_underflow, Vector<bool> &this_dimension_has_underflow, const PMF &parent, const PMF &sibling, const Vector<long> &result_first_support, const Vector<long> &result_last_support){
  for (unsigned dim = 0; dim < first_result_support_for_underflow.size(); ++dim){
    for (long result_support = result_first_support[dim]; result_support <= result_last_support[dim]; ++result_support){
      if (sibling.first_support()[dim] + result_support < parent.first_support()[dim]){
	this_dimension_has_underflow[dim] = true;
        first_result_support_for_underflow[dim] = result_support;
      }
    }
  }
}

void narrow_support(Tensor<FLOAT_TYPE> &result,   const Vector<long> & old_first_support, const Vector<long> & old_last_support, const Vector<long> & new_first_support, const Vector<long> & new_last_support) {
  Vector<unsigned long> new_shape(new_last_support.size());
  for (unsigned char i=0; i<new_last_support.size(); ++i){
    new_shape[i] = new_last_support[i] - new_first_support[i] + 1ul;
  }
  Vector<unsigned long> tensor_start = new_first_support - old_first_support;
  result.shrink(tensor_start, new_shape);
}

Tensor<FLOAT_TYPE> pad_tensor(const Tensor<FLOAT_TYPE> &input_tensor, const Vector<long> & new_first_support, const Vector<long> & new_last_support, const Vector<long> & old_first_support, const Vector<long> & old_last_support){
  Vector<unsigned long> new_shape(new_first_support.size());
  for (unsigned char i=0; i<new_first_support.size(); ++i)
    new_shape[i] = new_last_support[i] - new_first_support[i] + 1ul;
  
  Tensor<FLOAT_TYPE> output_tensor(new_shape);
  Vector<long> support_shift( new_first_support.size() );
  for( unsigned long i = 0; i < support_shift.size(); ++i)
    support_shift[i] = old_first_support[i] - new_first_support[i];

  enumerate_for_each_tensors([&input_tensor, &new_shape, &output_tensor, &new_first_support, &old_first_support, &support_shift](const_tup_t counter, const unsigned long dim) {
      unsigned long old_index = tuple_to_index(counter, input_tensor.data_shape(), dim);
      // Find new, shifted index
      unsigned long new_index = 0;
      unsigned char k;
      for (k=1; k<output_tensor.dimension(); ++k) {
	new_index += counter[k-1] + support_shift[k-1];
        new_index *= output_tensor.data_shape()[k];
      }
      new_index += counter[k-1] + support_shift[k-1];
      output_tensor.flat()[new_index] = input_tensor.flat()[old_index];
      
    },
    input_tensor.data_shape()
    );

  return output_tensor;
}

inline PMF p_noisy_or_add(const PMF & lhs,  const PMF & rhs, const Vector<long> &minimum_possible_first_support, const Vector<long> &maximum_possible_last_support, FLOAT_TYPE p) {
  #ifdef SHAPE_CHECK
  assert(lhs.table().dimension() == rhs.table().dimension());
  #endif

  Tensor<FLOAT_TYPE> result_tensor = numeric_p_convolve(lhs.table(), rhs.table(), p);
  Vector<long> current_result_first_support = lhs.first_support() + rhs.first_support();
  Vector<long> current_result_last_support  = lhs.last_support()  + rhs.last_support();

  for (unsigned long dim=1; dim<=lhs.table().dimension(); ++dim){
    // If the result tensor is shifted completely outside the legal
    // supports then shift to nearest support and the overflow/underflow
    // trimming will work without having to pad the tensor to include 0s
    // at the legal supports.

    if (current_result_first_support[dim-1] > maximum_possible_last_support[dim-1]){
      long shift = current_result_first_support[dim-1] - maximum_possible_last_support[dim-1];
      current_result_first_support[dim-1] = maximum_possible_last_support[dim-1];
      current_result_last_support[dim-1] -= shift;
    }

    else if (current_result_last_support[dim-1] < minimum_possible_first_support[dim-1]){
      long shift = minimum_possible_first_support[dim-1] - current_result_last_support[dim-1];
      current_result_last_support[dim-1] = minimum_possible_first_support[dim-1];
      current_result_first_support[dim-1] += shift;
    }
  }

  add_overflow_underflow(lhs, rhs, result_tensor, minimum_possible_first_support, maximum_possible_last_support, current_result_first_support, current_result_last_support, p);
  return PMF(current_result_first_support, result_tensor);
}

inline Tensor<FLOAT_TYPE> flip_tensor(const Tensor<FLOAT_TYPE> &input_tensor, const unsigned char flipped_dimension){
  Tensor<FLOAT_TYPE> flipped_tensor(input_tensor.data_shape());
  Vector<unsigned long> counter_flipped(flipped_dimension);
  enumerate_for_each_tensors([&flipped_tensor, &counter_flipped](const_tup_t counter, const unsigned char dim, FLOAT_TYPE val){
      for (unsigned char i=0; i<dim; ++i)
	counter_flipped[i] = flipped_tensor.data_shape()[i] - counter[i] - 1ul;

      flipped_tensor[ tuple_to_index(counter_flipped, flipped_tensor.data_shape(), dim) ] = val;
    },
    flipped_tensor.data_shape(),
    input_tensor);
  return flipped_tensor;
}

inline PMF p_noisy_or_sub(const PMF & lhs,  const PMF & rhs, const Vector<long> &minimum_possible_first_support, const Vector<long> &maximum_possible_last_support, FLOAT_TYPE p) {
  #ifdef SHAPE_CHECK
  assert(lhs.table().dimension() == rhs.table().dimension());
  #endif

  // Flip the rhs table along every axis so that addition of the
  // flipped table corresponds to subtraction with the original table:
  Tensor<FLOAT_TYPE> rhs_table_flipped = flip_tensor(rhs.table(), lhs.dimension());
  const Vector<long> parent_untrimmed_first_support = minimum_possible_first_support + rhs.first_support();
  const Vector<long> parent_untrimmed_last_support = maximum_possible_last_support + rhs.last_support();
  const Vector<long> parent_untrimmed_shape(parent_untrimmed_last_support - parent_untrimmed_first_support + 1l);
  Tensor<FLOAT_TYPE> padded_parent(parent_untrimmed_shape);

  enumerate_for_each_tensors([&padded_parent, &lhs, &parent_untrimmed_first_support](const_tup_t counter, const unsigned long dim, FLOAT_TYPE value){
      Vector<long> current_support_in_padded_parent(parent_untrimmed_first_support.size());
      for (unsigned long i=0; i < parent_untrimmed_first_support.size(); ++i)
	current_support_in_padded_parent[i] = counter[i] + parent_untrimmed_first_support[i];

      // current_support_in_padded_parent is currently support for the PADDED parent
      const unsigned long dest_index_in_padded_parent = tuple_to_index(counter, padded_parent.data_shape(), padded_parent.dimension());
      find_nearest_in_bound_support(lhs, current_support_in_padded_parent);
      tup_t source_position_in_parent = support_to_position_in_tensor(lhs.table(), lhs.first_support(), current_support_in_padded_parent);
      const unsigned long source_index_in_parent = tuple_to_index(source_position_in_parent, lhs.table().data_shape(), lhs.first_support().size());
      padded_parent[dest_index_in_padded_parent] = lhs.table()[source_index_in_parent];
    },
    padded_parent.data_shape(), padded_parent);

  Tensor<FLOAT_TYPE> result_tensor = numeric_p_convolve(padded_parent, rhs_table_flipped, p);
  narrow_support(result_tensor, (parent_untrimmed_first_support - rhs.last_support()), (parent_untrimmed_last_support - rhs.first_support()), minimum_possible_first_support, maximum_possible_last_support);
  return PMF(minimum_possible_first_support, result_tensor);
}

#endif
