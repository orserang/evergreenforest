#ifndef _SCALED_PMF_HPP
#define _SCALED_PMF_HPP

#include "squared.hpp"
#include "../Utility/FLOAT_TYPE.hpp"

// Note: could be sped up to not make local Vector objects and to not
// use tuple indexing (using integer offset):
inline void add_scaled_outcome(Tensor<FLOAT_TYPE> & ten, const Vector<long> & new_first_support, const Vector<FLOAT_TYPE> & scaled_tup, FLOAT_TYPE mass) {
  // For performance, don't bother if the mass is 0.
  if (mass > 0.0L) {
    Vector<unsigned long> start_index(ten.dimension());
    for (unsigned char i=0; i<ten.dimension(); ++i)
      start_index[i] = floorl(scaled_tup[i]) - new_first_support[i];

    Vector<unsigned long> scaled_bounding_box(ten.dimension());
    for (unsigned char i=0; i<ten.dimension(); ++i)
      scaled_bounding_box[i] = (ceill(scaled_tup[i]) - new_first_support[i] + 1) - start_index[i];

    // Split the mass over the partitioned boxes:
    for (unsigned char i=0; i<ten.dimension(); ++i)
      mass /= scaled_bounding_box[i];

    enumerate_apply_tensors([mass](const_tup_t tup, const unsigned char dim, FLOAT_TYPE & val){
	val += mass;
      },
      scaled_bounding_box,
      ten.start_at(start_index));
  }
}

inline PMF scaled_pmf(const PMF & pmf, const Vector<FLOAT_TYPE> & factor) {
  Vector<FLOAT_TYPE> extreme_a = pmf.first_support();
  extreme_a *= factor;
  Vector<FLOAT_TYPE> extreme_b = pmf.last_support();
  extreme_b *= factor;

  Vector<long> new_first_support(pmf.dimension());
  Vector<long> new_last_support(pmf.dimension());

  for (unsigned char i=0; i<pmf.dimension(); ++i) {

    new_first_support[i] = floorl( std::min(extreme_a[i], extreme_b[i]) );
    new_last_support[i] = ceill( std::max(extreme_a[i], extreme_b[i]) );
  }

  Tensor<FLOAT_TYPE> result_table(new_last_support - new_first_support + 1L);
  Vector<FLOAT_TYPE> scaled_tup(pmf.dimension());

  enumerate_for_each_tensors([&pmf, &result_table, &new_first_support, &scaled_tup, &factor](const_tup_t tup, const unsigned char dim, FLOAT_TYPE mass){
      for (unsigned char i=0; i<dim; ++i)
	scaled_tup[i] = (pmf.first_support()[i] + long(tup[i])) * factor[i];
      add_scaled_outcome(result_table, new_first_support, scaled_tup, mass);
    },
    pmf.table().data_shape(),
    pmf.table());

  return PMF(new_first_support, std::move(result_table));
}

#endif
