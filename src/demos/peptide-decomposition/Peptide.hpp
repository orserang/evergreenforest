#ifndef _Peptide_HPP
#define _Peptide_HPP

#include <string>
#include <set>
#include <iostream>
#include <vector>
#include <map>
#include <assert.h>

#include "../../Utility/FLOAT_TYPE.hpp"

class Peptide {
protected:
  std::string _amino_acids;

  FLOAT_TYPE _mass;
  FLOAT_TYPE _hydrophobicity;

  void verify_valid_characters() {
    std::set<char> amino_set(_amino_acids.begin(), _amino_acids.end());
    for (char c : _amino_acids) {
      if (amino_set.find(c) == amino_set.end()) {
	std::cerr << "Invalid character: " << c << std::endl;
	assert(false);
      }
    }
  }

  // Calculate the mass of the peptide.
  void init_mass() {
    std::map<char, FLOAT_TYPE> amino_acid_to_mass;
    for (unsigned long i=0; i<amino_acids.size(); ++i)
      amino_acid_to_mass[amino_acids[i]] = masses[i];
    
    _mass = 0.0;
    for (char aa : _amino_acids) {
      assert(amino_acid_to_mass.find(aa) != amino_acid_to_mass.end() && "Error: Amino acid not found.");
      _mass += amino_acid_to_mass[aa];
    }
  }
  
  // Calculate the hydrophobicity of the peptide. 
  void init_hydrophobicity() {
    std::map<char, FLOAT_TYPE> amino_acid_to_hydrophobicity;
    for (unsigned long i=0; i<amino_acids.size(); ++i)
      amino_acid_to_hydrophobicity[amino_acids[i]] = hydrophobicities[i];
    
    _hydrophobicity = 0.0;
    for (char aa : _amino_acids) {
      assert(amino_acid_to_hydrophobicity.find(aa) != amino_acid_to_hydrophobicity.end() && "Error: Amino acid not found.");
      _hydrophobicity += amino_acid_to_hydrophobicity[aa];
    }
  }
  
public:
  static const std::vector<char> amino_acids;
  static const std::vector<FLOAT_TYPE> masses;
  static const std::vector<FLOAT_TYPE> hydrophobicities;

  Peptide(const std::string & seq):
  _amino_acids(seq)
  {
    verify_valid_characters();
    init_mass();
    init_hydrophobicity();
  }

  unsigned long size() const {
    return _amino_acids.size();
  }

  char operator [] (unsigned long i) const {
    return _amino_acids[i];
  }
  
  const FLOAT_TYPE & mass() const{
    return _mass;
  }
  
  const FLOAT_TYPE & hydrophobicity() const{
    return _hydrophobicity;
  }

};

// {A:Ala, R:Arg, N:Asn, D:Asp, C:Cys, E:Glu, Q:Gln, G:Gly, H:His, I:Ile, L:Leu, K:Lys,
//  M:Met, F:Phe, P:Pro, S:Ser, T:Thr ,W:Trp , Y:Tyr, V:Val}
const std::vector<char> Peptide::amino_acids = {'A','R','N','D','C','E','Q','G','H','I','L','K','M','F','P','S','T','W','Y','V'};


// http://www.matrixscience.com/help/aa_help.html (average mass)
const std::vector<FLOAT_TYPE> Peptide::masses = {71.0779L, 156.1857L, 114.1026L,  115.0874L, 103.1429L, 129.114L, 128.1292L, 57.0513L, 137.1393L, 113.1576L, 113.1576L, 128.1723L, 131.1961L, 147.1739L, 97.1152L, 87.0773L, 101.1039L, 186.2099L, 163.1733L, 99.1311L};

// wwHydrophobicity from
// https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/midas/hydrophob.html
const std::vector<FLOAT_TYPE> Peptide::hydrophobicities = {-0.17L, -0.81L, -0.42L, -1.23L, 0.24L, -2.02L, -0.58L, -0.01L, -0.96L, 0.31L, 0.56L, -0.99L, 0.23L, 1.13L, -0.45L, -0.13L, -0.14L, 1.85L, 0.94L, -0.07L};

std::ostream & operator<<(std::ostream & os, const Peptide & rhs) {
  for (unsigned long i=0; i<rhs.size(); ++i)
    os << rhs[i];
  os << ": mass=" << rhs.mass() << " hydrophobicity=" << rhs.hydrophobicity();
  return os;
}

#endif
