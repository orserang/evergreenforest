import sys
import os
from RList import *
sys.path.append(os.path.abspath(os.getenv("EVERGREEN_SRC_PATH") + '/Language/python-interface'))
from Evergreen import *

class FidoProteinInference:
  def __init__(self, ppg, p, alpha, beta, gamma, use_epifany):
    self._ppg = ppg
    self._p = p

    self._alpha = alpha
    self._beta = beta
    self._gamma = gamma

    self._use_epifany = use_epifany
    self._evg_interface = Evergreen(evg_accuracy='high', p_norm=p)

    self._insert_protein_priors()
    self._insert_peptide_conditionals()

  def _make_table_for_peptide_count(self, max_n, pep_score):
    assert(0.0 <= pep_score <= 1.0)
    prob_table = np.zeros( (2,max_n+1), dtype=np.float64)
    prob_table[0] = (1-self._beta)*np.power(1-self._alpha, np.arange(0, max_n + 1))
    prob_table[1] = 1 - prob_table[0]

    prob_table[0] *= 1-pep_score
    prob_table[1] *= pep_score
    return sum(prob_table)

  def _insert_protein_priors(self):
    for prot in self._ppg.proteins():
      self._evg_interface.add_pmf(TableDependency((prot,), (0,), np.array([1-self._gamma, self._gamma], dtype=np.float64)))

  def _insert_peptide_conditionals(self):
    for pep in self._ppg.peptides():
      prots_for_peptide = self._ppg.proteins_matching_peptide(pep)
      num_prots_for_pep = len(prots_for_peptide)
      n_variable = 'N_' + pep
      self._evg_interface.add_boolean_expr(BooleanExpression("'" + n_variable + "'" + ' = ' + ' + '.join(["'" + prot + "'" for prot in prots_for_peptide])))
      self._evg_interface.add_pmf(TableDependency( (n_variable,), (0,), self._make_table_for_peptide_count(num_prots_for_pep, self._ppg.peptide_score(pep)) ))

      if self._use_epifany is True:
        self._evg_interface.add_pmf(TableDependency( (n_variable,), (0,), np.array([1.0/max(1,n) for n in range(num_prots_for_pep + 1)])))

  def print_rlist(self):
    pmf_dict, log_likelihood, kl_divergence = self._evg_interface.get_results(tuple(self._ppg.proteins()))
    # sometimes parameters for inference are too unrealistic for evg to solve, and no posteriors are given, so we want an empty rlist instead of an rlist with log-probabaility of None
    if len(pmf_dict) > 0:
      print RList.from_pmf_dict(pmf_dict, log_likelihood)
