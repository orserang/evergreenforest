import sys
import numpy as np
import matplotlib.pyplot as plt
import os

from TargetDecoy import *
from RList import *

def draw_all_series(all_series, all_labels, x_max, xlabel, ylabel, fig_num):
  assert( len(all_series) > 0 )
  assert( len(all_series) == len(all_labels) )

  plt.figure(fig_num)

  font = {'size' : 14}
  plt.rc('font', **font)

  col = ['red', 'blue', 'green', 'black', 'pink', 'orange', 'purple', 'yellow', 'cyan', 'magenta', 'gray']
  sty = ['-', '--', '-.', ':']

  colors = col*len(sty)
  styles = sty*len(col)

  plt.xlim(-x_max * 0.025, x_max)
  y_interp = -np.inf
  for series in all_series:
    series_x, series_y = zip(*series)
    y_interp = max(y_interp, np.interp(x_max, series_x, series_y))

  plt.ylim(0, 1.15 * y_interp)

  for series, label, c, s in zip(all_series, all_labels, colors, styles):
    plt.plot(*zip(*series), color=c, ls=s, label=label, linewidth=2, alpha=0.9)

  plt.xlabel(xlabel, size='xx-large')
  plt.ylabel(ylabel, size='xx-large')

  if any([ lbl != '' for lbl in all_labels ]):
    plt.legend(loc='lower right')

  plt.draw()

def draw_qvalue_rocs(all_rocs, all_labels, x_max, fig_num):
  draw_all_series(all_rocs, all_labels, x_max, 'q-value', 'Targets', fig_num)

def draw_calibrations(all_calibrations, all_labels, x_max, fig_num):
  draw_all_series(all_calibrations, all_labels, x_max, 'Empirical FDR', 'Computed FDR', fig_num)
  plt.plot([0, 1], [0, 1], ls='--')
  plt.draw()

def read_series_names(fname):

  with open(fname) as f:
    # Strip newline and add series name to names
    series_names = [line.replace('\n', '') for line in f if line]

    # Strip out empty lines
    series_names = [name for name in series_names if name]

  return series_names

def main(argv):
  if len(argv) < 3:
    # If user does not supply enough args, print usage.
    # Note args in are printed in usage such that they adhere to the following standard:
    # <required args> [optional args]
    print 'usage: <target_decoy_file> <decoy_max> <rlist1> [rlist2...] [--draw] [--series-names-file=series_filename]'
    sys.exit(1)


  # Determines whether or not to plot roc and calibration curves
  draw_plot = False
  # Let's the user use their own labels for the plots
  series_names = []

  # slicing ensures the list is copied
  argv_copy = argv[:]
  for arg in argv_copy:
    if arg.startswith("--"):
        argv.remove(arg)
        if arg == "--draw":
          draw_plot = True
        elif arg.startswith("--series-names-file="):
          series_names_file = arg.replace("--series-names-file=", "")
          if not os.path.isfile(series_names_file):
            raise Exception("series names file '" + series_names_file + "' not found")
          series_names = read_series_names(series_names_file)
        else:
          print "Warning: optional arg '" + arg +  "' not recognized" 

  if draw_plot == False and len(series_names) != 0:
    raise Exception("Turn on drawing to use series-names-file (pass --draw)")

  td = TargetDecoy.from_text(argv[0])
  x_max = float(argv[1])

  # Parse series and rlist names
  if len(series_names) == 0:
    series_names = argv[2:]
  rlist_fnames = argv[2:]

  # TODO: reimplement save feature

  roc_curves = []
  calibration_curves = []

  for fname in rlist_fnames:
    rlist = RList(fname)

    roc = td.qvalue_roc(rlist.sorted_probs_to_labels())
    roc_curves.append(roc)

    cal_curve = td.calibration_curve(rlist.sorted_probs_to_labels())
    calibration_curves.append(cal_curve)

    print fname, td.roc_area(rlist.sorted_probs_to_labels(), x_max), \
          td.square_calibration_error(rlist.sorted_probs_to_labels(), x_max), rlist.log_likelihood()

  if draw_plot:
    plt.ion()

    draw_qvalue_rocs(roc_curves, series_names, x_max, 1)
    draw_calibrations(calibration_curves, series_names, x_max, 2)

    # to draw interactively and wait
    plt.draw()
    print '[press a key]'
    raw_input()

if __name__ == '__main__':
    main(sys.argv[1:])
