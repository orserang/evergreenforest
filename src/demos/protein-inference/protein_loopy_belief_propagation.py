import sys
import os

# append to PYTHONPATH
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from ProteinPeptideGraph import *
from FidoProteinInference import *

def main(argv):
 
  if len(argv) < 5:
    print 'usage: <data> <p> <alpha> <beta> <gamma> [--use-epifany]'
    sys.exit(1)

  use_epifany = False
  # slicing guarantees argv is copied into argv_copy
  argv_copy = argv[:]
  # iterate through args to find --use-epifany flag
  for arg in argv_copy:
    if arg == "--use-epifany":
      use_epifany = True
      argv.remove(arg)

  # Parse parameters
  fname = argv[0]
  p = float(argv[1])
  alpha = float(argv[2])
  beta = float(argv[3])
  gamma = float(argv[4])

  assert(0.0 <= alpha <= 1.0)
  assert(0.0 <= beta <= 1.0)
  assert(0.0 <= gamma <= 1.0)

  ppg = ProteinPeptideGraph(fname)

  # Create .evg script that represents protein model
  pi = FidoProteinInference(ppg, p, alpha, beta, gamma, use_epifany)
  pi.print_rlist()

if __name__ == '__main__':
  main(sys.argv[1:])
