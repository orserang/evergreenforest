from collections import defaultdict

# Ranked list class
class RList:
  def __init__(self, rlist_fname=None):
    self._log_likelihood = 0.0

    probs_to_labels = defaultdict(set)

    if rlist_fname is not None:
      with open(rlist_fname) as f:
        for line in f:
          line = line.replace("\n", "")
          if len(line) == 0:
            continue
          prob, label = line.split()
          prob = float(prob)

          if label.startswith("Log-probability"):
            self._log_likelihood = prob

          else:
            probs_to_labels[prob].add(label)

      self._sorted_probs_to_labels = sorted( probs_to_labels.iteritems(), reverse=True )

  def sorted_probs_to_labels(self):
    return self._sorted_probs_to_labels

  def log_likelihood(self):
    return self._log_likelihood

  @classmethod
  def from_pmf_dict(cls, pmf_dict, log_likelihood):
    result = cls()
    probs_to_labels = defaultdict(set)
    result._log_likelihood = log_likelihood

    for variables, pmf in pmf_dict.iteritems():

      prob = pmf.get_mass(1)
      label = str(variables).translate(None, "()',")

      probs_to_labels[prob].add(label)

    result._sorted_probs_to_labels = sorted(probs_to_labels.iteritems(), reverse=True)
    return result

  def __str__(self):
    result = ""
    for prob, labels in self._sorted_probs_to_labels:
      for label in labels:
        result += str(prob) + ' ' + str(label) + '\n'
    result += str(self._log_likelihood) + " Log-probability-of-model\n"
    return result
