# For constructing a bipartite of proteins to peptides
class ProteinPeptideGraph:
  def __init__(self, fname=None):
    self._peptides = set()
    self._peptide_to_score = {}
    self._peptide_to_proteins = {}
    self._protein_to_peptides = {}
    self._proteins = set()

    if fname is not None:
      with open(fname) as f:
        for line in f:
          pep_score_prots = line.split()
          # todo: map isoleucine to leucine?
          pep = pep_score_prots[0]
          pep = pep.replace('I', 'L')
          score = pep_score_prots[1]
          prots_containing_pep = pep_score_prots[2:]

          self._peptides.add(pep)
          # todo: currently assumes each peptide occurs once; what should
          # be done when a peptide has multiple scores?
          self._peptide_to_score[pep] = float(score)

          self._peptide_to_proteins[pep] = set()
          for prot in prots_containing_pep:
            self._proteins.add(prot)
            self._peptide_to_proteins[pep].add(prot)

            if prot not in self._protein_to_peptides:
              self._protein_to_peptides[prot] = set()

            self._protein_to_peptides[prot].add(pep)

  def read_fido_no_charge(self, fname):
    self._peptides = set()
    self._peptide_to_score = {}
    self._peptide_to_proteins = {}
    self._proteins = set()

    with open(fname) as f:
      for line in f:
        action, arg = line.split()
        if action == 'e':
          # replace Isoleucine with Leucine (they are indistinguishable):
          pep_string = arg.replace('I','L')

          self._peptides.add(pep_string)
          if pep_string not in self._peptide_to_proteins:
            self._peptide_to_proteins[pep_string] = set([])

          # add a 0.0 score just in case there is no score:
          self._peptide_to_score[pep_string] = 0.0 # Default dict?
        elif action == 'r':
          prot_string = arg
          self._proteins.add(prot_string)
          self._peptide_to_proteins[pep_string].add(prot_string)
        elif action == 'p':
          pep_score = float(arg)
          self._peptide_to_score[pep_string] = pep_score
        else:
          raise Exception('Format error!')

  def print_as_pivdo(self):
    for pep in self.peptides():
      print 'e', pep
      for prot in self.proteins_matching_peptide(pep):
        print 'r', prot
      print 'p', self.peptide_score(pep)

  def write(self):
    for pep in self.peptides():
      print pep, self.peptide_score(pep),
      for prot in self.proteins_matching_peptide(pep):
        print prot,
      print
  
  def peptides(self):
    return self._peptides

  def proteins(self):
    return self._proteins

  def proteins_matching_peptide(self, pep):
    return self._peptide_to_proteins[pep]

  def peptides_matching_protein(self, prot):
    return self._protein_to_peptides[prot]

  def peptide_score(self, pep):
    return self._peptide_to_score[pep]
