### tools for dealing with target-decoy FDR estimation, roc_area, etc.
from scipy.interpolate import InterpolatedUnivariateSpline

class TargetDecoy(object):
  def __init__(self):
    self.targets = set()
    self.decoys = set()
    self.others = set()

  def roc(self, prot_rlist):
    prot_rlist = [ prots for score, prots in prot_rlist]

    tgt_count = 0
    dcy_count = 0
    decoy_and_target_counts = [(0, 0)]
    for p_set in prot_rlist:
      tgt_count += len(p_set & self.targets)
      dcy_count += len(p_set & self.decoys)
      decoy_and_target_counts.append( (dcy_count, tgt_count) )
    decoy_and_target_counts.append( (len(self.decoys), len(self.targets)) )
    return decoy_and_target_counts

  def roc_area(self, prot_rlist, max_decoy):

    roc = sorted(self.qvalue_roc(prot_rlist))
    for i in xrange(len(roc)):
        dcy,tgt = roc[i]
        dcy /= float(max_decoy)
        tgt /= float(len(self.targets))
        roc[i] = dcy,tgt
        
    decoy_to_max_targets = {}
    for decoy,target in roc:
        if decoy not in decoy_to_max_targets:
            decoy_to_max_targets[decoy] = -1 # replace with default dict?
        decoy_to_max_targets[decoy] = max(decoy_to_max_targets[decoy], target)

    roc = sorted(list(decoy_to_max_targets.iteritems()))

    x_points, y_points = zip(*roc)
    # Integrate over roc points using a linear interpolator
    return InterpolatedUnivariateSpline(x_points, y_points, k=1).integral(roc[0][0], max_decoy) / max_decoy

  def fdr(self, dcy_count, tgt_count):
    if tgt_count == 0:
      if dcy_count > 0:
        return 1.0
      else:
        return 0.0
    return min(float(dcy_count) / tgt_count, 1.0)

  def fdr_roc(self, prot_rlist):
    result = []
    decoy_and_target_counts = self.roc(prot_rlist)
    for dcy_count, tgt_count in decoy_and_target_counts:
      if tgt_count > 0:
        result.append( (self.fdr(dcy_count, tgt_count), tgt_count) )
    decoy_and_target_counts.append( ( self.fdr(len(self.decoys), len(self.targets)), len(self.targets) ) )
    return result

  def qvalue_roc(self, prot_rlist):
    fdr_roc = self.fdr_roc(prot_rlist)
    fdr_to_tp = {}
    for fdr, tp in fdr_roc:
      if fdr not in fdr_to_tp:
        fdr_to_tp[fdr] = []
      fdr_to_tp[fdr].append(tp)

    qvalue_to_tp = {}
    max_tp_so_far = 0
    for fdr, tps in sorted(fdr_to_tp.iteritems()):
      tp = max(tps)
      max_tp_so_far = max(max_tp_so_far, tp)
      qvalue_to_tp[fdr] = max_tp_so_far

    return [(0,0)] + sorted(qvalue_to_tp.iteritems())

  def calibration_curve(self, prot_rlist):
    result = [ (0.0,0.0) ]
    targets_at_threshold = set()
    decoys_at_threshold = set()
    num_prots_at_threshold = 0
    est_fps = 0.0
    for prob, prots in prot_rlist:
      num_prots_at_threshold += len(prots)
      est_fps += (1-prob)*len(prots)

      est_fdr = est_fps / num_prots_at_threshold

      for prot in prots:
        if prot in self.targets:
          targets_at_threshold.add(prot)
        elif prot in self.decoys:
          decoys_at_threshold.add(prot)

        emp_fdr = self.fdr(len(decoys_at_threshold), len(targets_at_threshold))
        result.append( (emp_fdr, est_fdr) )

    return result

  @staticmethod
  def integrate_squared_trapezoid(x1, y1, x2, y2, x_max):
    if x1 >= x2 or x1 >= x_max:
      return 0.0

    numer = -(x1-x2)**3 * (x1-y1)**3 + (x2*y1 - x1*y2 + x_max*(x1-x2-y1+y2))**3
    denom = 3.0 * (x1-x2)**2 * (x1-x2-y1+y2)
    return numer / denom

  def square_calibration_error(self, rlist, max_qval):
    cal_curve = self.calibration_curve(rlist)

    res = 0.0
    for i in xrange(len(cal_curve)-1):
      x,y = cal_curve[i]
      x_next,y_next = cal_curve[i+1]
      if x >= max_qval:
        break

      x_upper = min(x_next, max_qval)
      if x_upper != x_next:
        slope = (y_next - y) / (x_next - x)
        y_next = y + slope*(x_upper-x)
      integral = TargetDecoy.integrate_squared_trapezoid(x, y, x_next, y_next, x_upper)
      res += integral
    return res

  @classmethod
  def from_text(cls, fname):
    result = cls()
    all_prots = set()
    with open(fname) as f:
      for line in f:
        prot, lbl = line.split()
        if lbl not in ('target', 'decoy', 'other'):
          raise Exception('label must be "target", "decoy", or "other", but is "' + lbl + '"')

        if prot in all_prots:
          raise Exception('protein loaded multiple times')
        all_prots.add(prot)

        if lbl == 'target':
          result.targets.add(prot)
        elif lbl == 'decoy':
          result.decoys.add(prot)
        else: # lbl == 'other'
          result.others.add(prot)
    return result
