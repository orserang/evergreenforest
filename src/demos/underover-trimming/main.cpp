#include <fstream>

#include "../../Evergreen/evergreen.hpp"

void l1_project(tup_t dest_tuple, const_tup_t source_tuple, const Vector<unsigned long> & lower_support, const Vector<unsigned long> & upper_support) {
  for (unsigned char i=0; i<lower_support.size(); ++i)
    if (source_tuple[i] < lower_support[i])
      dest_tuple[i] = lower_support[i];
    else if (source_tuple[i] > upper_support[i])
      dest_tuple[i] = upper_support[i];
    else
      dest_tuple[i] = source_tuple[i];
}

int main() {
  PMF A_below({2L,1L},Tensor<FLOAT_TYPE>({3,3},{1,10,9,3,7,2,1,2,6}));
  PMF B_below({1L,0L},Tensor<FLOAT_TYPE>({3,3},{1,2,3,4,5,6,7,8,9}));
  PMF C_above({2L,0L},Tensor<FLOAT_TYPE>({3,3},{1,4,5,8,9,7,2,3,6}));
  A_below.reset_norm_constant();
  B_below.reset_norm_constant();
  C_above.reset_norm_constant();
  std::cout << A_below << std::endl;
  std::cout << B_below << std::endl;
  std::cout << C_above << std::endl;
  std::cout << "-----------" << std::endl;

  PMF C_below = p_add(A_below, B_below, 1.0);
  std::cout << C_below << std::endl;
  std::cout << numeric_p_convolve(A_below.table(), B_below.table(), 1.0) << std::endl << std::endl;

  PMF C_trimmed_standard = C_below;
  C_trimmed_standard.narrow_support(C_above.first_support(), C_above.last_support());
  std::cout << C_trimmed_standard << std::endl;

  Tensor<double> C_trimmed_standard_unnormed = C_trimmed_standard.table();
  const double norm_const = exp(C_trimmed_standard.log_normalization_constant());
  for (unsigned long i=0; i<C_trimmed_standard_unnormed.flat_size(); ++i)
    C_trimmed_standard_unnormed[i] *= norm_const;

  std::cout << C_trimmed_standard_unnormed << std::endl << std::endl;

  // Naive underover trimming for forward pass:
  const Tensor<double> & lhs = A_below.table();
  const Tensor<double> & rhs = B_below.table();
  Tensor<double> result(C_trimmed_standard.table().data_shape());
  Vector<unsigned long> counter_result(result.dimension());
  Vector<unsigned long> trimmed_counter_result(result.dimension());
  enumerate_for_each_tensors([&trimmed_counter_result, &counter_result, &result, &rhs, &A_below, &B_below, &C_trimmed_standard](const_tup_t counter_lhs, const unsigned char dim_lhs, double lhs_val) {
       enumerate_for_each_tensors([&trimmed_counter_result, &counter_result, &result, &rhs, &counter_lhs, &lhs_val, &A_below, &B_below, &C_trimmed_standard](const_tup_t counter_rhs, const unsigned char dim_rhs, double rhs_val) {
          // Compute the outcome tuple (in value, not index):
	  for (unsigned char i=0; i<dim_rhs; ++i)
	    counter_result[i] = counter_lhs[i] + counter_rhs[i] + A_below.first_support()[i] + B_below.first_support()[i];

	  // TODO: last_support() will allocate on the fly. Use variable capture instead.
	  l1_project(trimmed_counter_result, counter_result, C_trimmed_standard.first_support(), C_trimmed_standard.last_support());

          // Convert the index tuple to an outcome tuple (relative to the support of C_trimmed_standard):
	  for (unsigned char i=0; i<dim_rhs; ++i)
	    trimmed_counter_result[i] -= C_trimmed_standard.first_support()[i];

	  unsigned long result_flat = tuple_to_index(trimmed_counter_result, result.data_shape(), dim_rhs);
	  result[result_flat] += lhs_val * rhs_val;
	},
	rhs.data_shape(),
	rhs);
    },
    lhs.data_shape(),
    lhs);
  std::cout << result << std::endl;

  // Non-naive underover trimming for forward pass:
  Tensor<double> result_fast(C_trimmed_standard.table().data_shape());
  enumerate_for_each_tensors([&result_fast, &A_below, &B_below, &C_trimmed_standard, &C_below, &trimmed_counter_result](const_tup_t c_untrimmed_index, const unsigned char dim, double untrimmed_c_value) {
     // Map index to value:
     for (unsigned char i=0; i<dim; ++i)
       trimmed_counter_result[i] = c_untrimmed_index[i] + C_trimmed_standard.first_support()[i];

     // fixme: both are passed as restrict, but we are using one to modify the other:
     l1_project(trimmed_counter_result, trimmed_counter_result, C_trimmed_standard.first_support(), C_trimmed_standard.last_support());

     // Map value back to index:
     for (unsigned char i=0; i<dim; ++i)
       trimmed_counter_result[i] = trimmed_counter_result[i] - C_trimmed_standard.first_support()[i];

     unsigned long result_flat = tuple_to_index(trimmed_counter_result, result_fast.data_shape(), dim);
     result_fast[result_flat] += untrimmed_c_value;
  },
    C_below.table().data_shape(),
    C_below.table());

  std::cout << result_fast << std::endl;

  return 0;
}
