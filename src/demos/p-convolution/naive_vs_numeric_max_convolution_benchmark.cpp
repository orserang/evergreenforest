#include <iostream>
#include "../../Convolution/p_convolve.hpp"
#include "../../Utility/Clock.hpp"

Clock c;

void init_data(Tensor<FLOAT_TYPE> & x, Tensor<FLOAT_TYPE> & y) {
  unsigned long k;
  for (k=0; k<x.flat_size(); ++k)

    x[k] = expl( - (k - 128.0L)*(k - 128.0L) / (100.0L*100.0L) );
  for (k=0; k<y.flat_size(); ++k)

    y[k] = x[k] + expl( - (k - 700.0L)*(k - 700.0L) / (10.0L*10.0L) );
}

int main(int argc, char**argv) {
  if (argc != 2) {
    std::cerr << "Usage: convolution_benchmark <LOG_N>" << std::endl;
    return 1;
  }

  const unsigned int log_n = atoi(argv[1]);
  const unsigned long n = 1ul<<log_n;
    
  Tensor<FLOAT_TYPE> x({n});
  Tensor<FLOAT_TYPE> y({n});

  init_data(x,y);

  x.flat() /= sum( x.flat() );
  y.flat() /= sum( y.flat() );

  Clock c;

  c.tick();
  auto z = naive_max_convolve(x,y);
  std::cout << n << " " << c.tock() << " ";

  c.tick();
  auto z2 = numeric_p_convolve(x,y,std::numeric_limits<FLOAT_TYPE>::infinity());
  std::cout << c.tock() << std::endl;
}
