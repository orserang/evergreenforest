#ifndef _CPX_HPP
#define _CPX_HPP

#include "../Utility/force_inline.hpp"
#include "../Utility/FLOAT_TYPE.hpp"

#include <ostream>
// in MSVC M_PI constant is non-standard
#define _USE_MATH_DEFINES 
#include <cmath>

struct cpx {
  static constexpr FLOAT_TYPE PrintEpsilon = 1e-12L;
  FLOAT_TYPE r;
  FLOAT_TYPE i;

  constexpr cpx():
    r(0.0L),
    i(0.0L)
  { }

  constexpr cpx(FLOAT_TYPE rVal):
    r(rVal),
    i(0.0L)
  { }

  constexpr cpx(FLOAT_TYPE rVal, FLOAT_TYPE iVal):
    r(rVal),
    i(iVal)
  { }
  
  FORCE_INLINE cpx operator+=(cpx rhs){
    r += rhs.r;
    i += rhs.i;
    return *this;
  }
  FORCE_INLINE cpx operator-=(cpx rhs){
    r -= rhs.r;
    i -= rhs.i;
    return *this;
  }
  // Slightly faster than * operator (needs only one temporary FLOAT_TYPE):
  FORCE_INLINE const cpx & operator *=(cpx rhs) {
    FLOAT_TYPE temp = r;
    r *= rhs.r;
    r -= i*rhs.i;
    i = temp*rhs.i+i*rhs.r;
    return *this;
  }
  FORCE_INLINE const cpx & operator *=(FLOAT_TYPE scale) {
    r *= scale;
    i *= scale;
    return *this;
  }
  FORCE_INLINE const cpx & operator /=(FLOAT_TYPE denom) {
    denom = 1.0/denom;
    r *= denom;
    i *= denom;
    return *this;
  }
  FORCE_INLINE cpx conj() const {
    return cpx{r, -i};
  }
};

FORCE_INLINE cpx operator *(cpx lhs, cpx rhs) {
  // Gauss' method for multiplying complex numbers turns out not to be
  // faster after compiler optimizations; here is the naive method:
  return cpx{lhs.r*rhs.r-lhs.i*rhs.i, lhs.r*rhs.i+lhs.i*rhs.r};
}

FORCE_INLINE cpx operator *(FLOAT_TYPE lhs, cpx rhs) {
  rhs.r *= lhs;
  rhs.i *= lhs;
  return rhs;
}

FORCE_INLINE cpx operator -(cpx lhs, cpx rhs){
  return cpx{lhs.r-rhs.r, lhs.i-rhs.i};
}

FORCE_INLINE cpx operator +(cpx lhs, cpx rhs){
  return cpx{lhs.r+rhs.r, lhs.i+rhs.i};
}

FORCE_INLINE cpx operator /(cpx lhs, FLOAT_TYPE rhs) {
  lhs.r /= rhs;
  lhs.i /= rhs;
  return lhs;
}

FORCE_INLINE bool operator ==(cpx lhs, cpx rhs){
  return (lhs.r == rhs.r) && (lhs.i == rhs.i);
}

inline std::ostream & operator << (std::ostream & os, cpx cmplx) {
  if ( fabsl(cmplx.r) >= cpx::PrintEpsilon && fabsl(cmplx.i) >= cpx::PrintEpsilon ) {
    os << cmplx.r;
    if (cmplx.i > 0)
      os << '+';
    os << cmplx.i << 'j' ;
    return os;
  }

  if ( fabsl(cmplx.r) >= cpx::PrintEpsilon )
      return ( os << cmplx.r );

  if ( fabsl(cmplx.i) >= cpx::PrintEpsilon )
    return (os << cmplx.i << 'j' );
  return (os << 0.0L);
}

#endif
