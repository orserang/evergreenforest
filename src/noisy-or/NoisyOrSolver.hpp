#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <fstream>
#include <assert.h>
#include <math.h>
#include <unordered_map>

#include "Algorithms/PMF/PMF.hpp"
#include "Utility/Clock.hpp"

class NoisyOrSolver{
private:
  std::vector<std::pair<PMF, PMF> > _pmfs;
  std::unordered_map<unsigned long, unsigned long> _child_to_parent;
  std::unordered_map<unsigned long, std::pair<unsigned long, unsigned long> > _parent_to_children;
  std::unordered_map<unsigned long, unsigned long> _child_to_sibling;
  std::unordered_map<unsigned long, unsigned long> _node_to_label;
  unsigned long _number_leaves;
  unsigned long _number_of_nodes_in_tree;
  unsigned long _number_layers_in_tree;
  unsigned long _root_index;

  PMF get_binary_pmf(double probability_of_0, double probability_of_1){
    std::vector<unsigned int> v = {0,1};
    Tensor<FLOAT_TYPE> probability_table( {*v.rbegin() + 1ul} );
    for (unsigned int key : v) {
      if (key == 0)
        probability_table[key] = (probability_of_0);
      if (key == 1)      
        probability_table[key] = (probability_of_1);
    }
    return PMF({0L}, probability_table);
  }

  void create_pmfs_from_vectors(std::vector<FLOAT_TYPE> &input_probs, std::vector<FLOAT_TYPE> & output_probs){
    _pmfs.resize( _number_of_nodes_in_tree );

    // Leave's message in is prior
    for (unsigned long i = 0; i < _number_leaves; ++i){
      FLOAT_TYPE prob_equal_0 = input_probs[i];
      FLOAT_TYPE prob_equal_1 = 1.0 - input_probs[i];
      FLOAT_TYPE norm_const = prob_equal_0 + prob_equal_1;
      _pmfs[i].first = get_binary_pmf( prob_equal_0/norm_const, prob_equal_1/norm_const);
    }

    // Root's message in is the likelihood
    FLOAT_TYPE norm_const = output_probs[0] + output_probs[1];
    _pmfs[_root_index].second = get_binary_pmf( output_probs[0]/norm_const, output_probs[1]/norm_const);
  }

  void label_node_rec(unsigned long node, unsigned long label){
    _node_to_label.insert({node, label});
    if (node >= _number_leaves){ // node has children
      label_node_rec(_parent_to_children[node].first, 2*label+1);
      label_node_rec(_parent_to_children[node].second, 2*label+2);
    }
  }

  void build_tree(){
    unsigned long size_of_child_layer = _number_leaves;
    unsigned long start_of_child_layer = 0;

    unsigned long size_of_parent_layer = _number_leaves/2;
    unsigned long start_of_parent_layer = _number_leaves;

    while (size_of_parent_layer > 0){
      for (unsigned long i=0; i < size_of_parent_layer; ++i){
	unsigned long parent       = start_of_parent_layer + i;
	unsigned long first_child  = start_of_child_layer + 2*i;
	unsigned long second_child = start_of_child_layer + (2*i+1);
        _parent_to_children.insert({parent, std::make_pair(first_child, second_child)});
        _child_to_parent.insert({first_child, parent});
        _child_to_parent.insert({second_child, parent});
	_child_to_sibling.insert({first_child, second_child});
	_child_to_sibling.insert({second_child, first_child});
      }
      if (size_of_child_layer %2 == 0){
        size_of_child_layer   = size_of_parent_layer;
        size_of_parent_layer  = size_of_parent_layer/2;
        start_of_child_layer  = start_of_parent_layer;
        start_of_parent_layer = start_of_child_layer + size_of_child_layer;
      }else{
        size_of_child_layer   = size_of_parent_layer + 1;
        start_of_child_layer  = start_of_parent_layer-1;
        size_of_parent_layer  = size_of_child_layer/2;
        start_of_parent_layer = start_of_child_layer + size_of_child_layer;
      }
    }
  }

  void forward_pass(){
    std::vector<bool> prior_has_been_updated(_number_of_nodes_in_tree, false);
    std::set<unsigned long> nodes_to_be_updated;

    for (unsigned long leaf_node=0; leaf_node < _number_leaves; ++leaf_node)
      prior_has_been_updated[leaf_node] = true;

    for (unsigned long leaf_node=0; leaf_node < _number_leaves; ++leaf_node){
      unsigned long parent = _child_to_parent[leaf_node];
      if (prior_has_been_updated[_parent_to_children[parent].first] && prior_has_been_updated[_parent_to_children[parent].second])
	nodes_to_be_updated.insert(_child_to_parent[leaf_node]);
    }

    while (nodes_to_be_updated.size() > 0){
      auto parent = *nodes_to_be_updated.begin();
      unsigned long first_child  = _parent_to_children[parent].first;
      unsigned long second_child = _parent_to_children[parent].second;
      
      double parent_eq_0 = _pmfs[first_child].first.table()[0ul] * _pmfs[second_child].first.table()[0ul];
      double parent_eq_1 = _pmfs[first_child].first.table()[1ul] * _pmfs[second_child].first.table()[0ul]
    	+ _pmfs[first_child].first.table()[1ul] * _pmfs[second_child].first.table()[1ul]
    	+ _pmfs[first_child].first.table()[0ul] * _pmfs[second_child].first.table()[1ul];
      
      _pmfs[parent].first = get_binary_pmf(parent_eq_0, parent_eq_1);

      prior_has_been_updated[parent] = true;
      if (parent != _root_index){
	unsigned long grandparent = _child_to_parent[parent];
	// only add to nodes_to_be_updated if both children are up-to-date
	if (prior_has_been_updated[_parent_to_children[grandparent].first] && prior_has_been_updated[_parent_to_children[grandparent].second]){
	  nodes_to_be_updated.insert(grandparent);
	}
      }
      nodes_to_be_updated.erase(*nodes_to_be_updated.begin());
    }
  }

  void backward_pass(){
    std::vector<bool> prior_has_been_updated(_number_of_nodes_in_tree, false);
    std::set<unsigned long> nodes_to_be_updated;
    prior_has_been_updated[_root_index] = true;
    nodes_to_be_updated.insert( _parent_to_children[_root_index].first );
    nodes_to_be_updated.insert( _parent_to_children[_root_index].second );

    while (nodes_to_be_updated.size() > 0){
      unsigned long child = *nodes_to_be_updated.begin();
      unsigned long sibling = _child_to_sibling[child];
      unsigned long parent = _child_to_parent[child];
      double child_eq_0 = _pmfs[sibling].first.table()[0ul] * _pmfs[parent].second.table()[0ul]
	+ _pmfs[sibling].first.table()[1ul] * _pmfs[parent].second.table()[1ul];
      double child_eq_1 =
	_pmfs[sibling].first.table()[0ul] * _pmfs[parent].second.table()[1ul]
	+ _pmfs[sibling].first.table()[1ul] * _pmfs[parent].second.table()[1ul];
      
      _pmfs[child].second = get_binary_pmf(child_eq_0, child_eq_1);
      nodes_to_be_updated.erase(*nodes_to_be_updated.begin());
      if (child >= _number_leaves){
	nodes_to_be_updated.insert(_parent_to_children[child].first);
	nodes_to_be_updated.insert(_parent_to_children[child].second);
      }
    }
  }

public:
  NoisyOrSolver(std::vector<FLOAT_TYPE> &input_probs, std::vector<FLOAT_TYPE> & output_probs):
    _number_leaves(input_probs.size()),
    _number_of_nodes_in_tree(2*input_probs.size() -1),
    _number_layers_in_tree(1+ceil(log2(input_probs.size()))),
    _root_index((2*input_probs.size() -1)-1)    
  {
    build_tree();
    label_node_rec(_root_index, 0ul);
    create_pmfs_from_vectors(input_probs, output_probs);
  }

  void calc_and_print_all_marginals(){
    Clock c;
    forward_pass();
    backward_pass();
    c.ptock();
    for (unsigned long i=0; i < _number_leaves; ++i){
      double posterior_eq_0 = _pmfs[i].first.table()[0ul]*_pmfs[i].second.table()[0ul];
      double posterior_eq_1 = _pmfs[i].first.table()[1ul]*_pmfs[i].second.table()[1ul];
      double nrm_const = posterior_eq_0+posterior_eq_1;
      std::cout << "X" << i << " [0] " << posterior_eq_0/nrm_const << ", " << posterior_eq_1/nrm_const << std::endl;
    }

    if (_pmfs[_root_index].first.table().data_shape()[0] == 1)
      std::cout << "Y" << " [0] " << 0 << ", " << 1 << std::endl;
    else{
      double posterior_eq_0 = _pmfs[_root_index].first.table()[0ul] * _pmfs[_root_index].second.table()[0ul];
      double posterior_eq_1 = _pmfs[_root_index].first.table()[1ul] * _pmfs[_root_index].second.table()[1ul];
      double nrm_const = posterior_eq_0+posterior_eq_1;
      std::cout << "Y" << " [0] " << posterior_eq_0/nrm_const << ", " << posterior_eq_1/nrm_const << std::endl;
    }
  }
};
