#ifndef _PNORMMIXIN_HPP
#define _PNORMMIXIN_HPP

#include "../Utility/FLOAT_TYPE.hpp"


class PNormMixin {
public:
  const FLOAT_TYPE p;

public:
  PNormMixin(FLOAT_TYPE p_param):
    p(p_param)
  { }
};

#endif
