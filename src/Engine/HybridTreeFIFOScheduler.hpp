#ifndef _HYBRIDTREEFIFOSCHEDULER_HPP
#define _HYBRIDTREEFIFOSCHEDULER_HPP

#include "FIFOScheduler.hpp"
#include "SingleTreeScheduler.hpp"
#include "../Utility/FLOAT_TYPE.hpp"


// Combines SingleTreeScheduler and FIFOScheduler: first run SingleTreeScheduler
// (this is useful for solving trees exactly, while still also solving
// graphs which have loops in them).
template <typename VARIABLE_KEY>
class HybridTreeFIFOScheduler : public Scheduler<VARIABLE_KEY> {
protected:

  FIFOScheduler<VARIABLE_KEY>* fs;
  SingleTreeScheduler<VARIABLE_KEY>* sts;
  
public:
  HybridTreeFIFOScheduler(FLOAT_TYPE dampening_lambda, FLOAT_TYPE convergence_threshold, unsigned long maximum_iterations, InferenceGraph<VARIABLE_KEY> & graph):
    Scheduler<VARIABLE_KEY>(dampening_lambda, convergence_threshold, maximum_iterations, graph),
    fs(NULL),
    sts(NULL)
  { }

  ~HybridTreeFIFOScheduler() {
    if (fs != NULL)
      delete fs;
    if (sts != NULL)
      delete sts;
  }

  void add_ab_initio_edges() {
    sts = new SingleTreeScheduler<VARIABLE_KEY> (this->_dampening_lambda, this->_convergence_threshold, this->_maximum_iterations, *this->_graph_ptr);
    sts->add_ab_initio_edges();
  }

  unsigned long process_next_edges() {    
    if (!sts->has_converged())
      return sts->process_next_edges();
    else
      return fs->process_next_edges();
  }

  unsigned long run_until_convergence() {

    // First, send messages across every reachable edge via the SingleTreeScheduler:
    unsigned long iterations_used = sts->run_until_convergence();

    // get any messages that can pass
    ListQueue<VARIABLE_KEY> edges_to_queue = sts->get_edges_that_can_pass(); 
 
    // iterations_used should be <= this->_maximum_iterations, so subtracting is safe:
    fs = new FIFOScheduler<VARIABLE_KEY>(this->_dampening_lambda, this->_convergence_threshold, this->_maximum_iterations - iterations_used, *this->_graph_ptr);

    fs->set_queue(edges_to_queue);
    return iterations_used + fs->run_until_convergence();
  }

  bool has_converged() const {
    return fs->has_converged();
  }
};

#endif
