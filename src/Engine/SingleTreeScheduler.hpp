#ifndef _SINGLETREESCHEDULER_HPP
#define _SINGLETREESCHEDULER_HPP

#include "Scheduler.hpp"
#include "random_tree_subgraph.hpp"
#include "../Utility/FLOAT_TYPE.hpp"


template <typename VARIABLE_KEY>
class SingleTreeScheduler : public Scheduler<VARIABLE_KEY> {
private:
 
  std::vector<MessagePasser<VARIABLE_KEY>* > _mp_ordering;

  // FIXME: better way to do this?
  bool _have_edges_been_processed = false;
  
  void clear_edge_colors() {
    for (MessagePasser<VARIABLE_KEY>* mp : this->_graph_ptr->message_passers)
      for (unsigned long edge_ind=0; edge_ind<mp->number_edges(); ++edge_ind)
    	mp->get_edge_out(edge_ind)->color = 0;
  }

  bool extremal_hyperedge(const MessagePasser<VARIABLE_KEY>* mp) {
    return mp->number_edges() == 1 && ! mp->ready_to_send_message_ab_initio(0);
  }

  // Passes all valid messages for mp.
  void pass_all_messages_possible(MessagePasser<VARIABLE_KEY>* mp) {
    
    // Note: Could save some time by ignoring cases where it's clear
    // no message can be passed:
    // if (mp->can_potentially_pass_any_messages())
    // However, this would be a little tricky since it does not
    // include the ab initio case.
    for (unsigned long i=0; i<mp->number_edges(); ++i) {
      Edge<VARIABLE_KEY>*edge = mp->get_edge_out(i);
      MessagePasser<VARIABLE_KEY>* dest_mp = edge->dest; 
            
      // Note: we do not want to pass to a hyperedge until we have propagated
      // all other information so we delay this as long as possible
      if ( ! extremal_hyperedge(dest_mp) && ( mp->ready_to_send_message_ab_initio(i) || mp->ready_to_send_message(i) ) ) {

	Edge<VARIABLE_KEY>* backward_edge = edge->get_opposite_edge_ptr();
	LabeledPMF<VARIABLE_KEY> new_msg = mp->update_and_get_message_out(i);
	
	// Don't pass a message back along the edge we just came from.
	if ( backward_edge->color == 0 && ( ! edge->has_message() || (edge->has_message() && mse_divergence(edge->get_possibly_outdated_message(), new_msg) > this->_convergence_threshold) ) ) {

	  // Mark edge as being used
	  edge->color = 1;
	  
	  // FIXME: remove brackets on if statement
	  if (edge->has_message())
	    // Dampen:
	    new_msg = dampen(edge->get_possibly_outdated_message(), new_msg, this->_dampening_lambda).transposed(*edge->variables_ptr);

	  edge->set_message( std::move(new_msg) );

	  // Recieve message
	  dest_mp->receive_message_in_and_update(edge->dest_edge_index);	  
	} 
      }
    }
  }

public:
  SingleTreeScheduler(FLOAT_TYPE dampening_lambda_param, FLOAT_TYPE convergence_threshold_param, unsigned long maximum_iterations_param, InferenceGraph<VARIABLE_KEY> & graph):
    Scheduler<VARIABLE_KEY>(dampening_lambda_param, convergence_threshold_param, maximum_iterations_param, graph)
  { }

  void add_ab_initio_edges() {
    _mp_ordering = random_tree_subgraph(*this->_graph_ptr);    
  }

  unsigned long process_next_edges() {
    
    _have_edges_been_processed = true;    
    unsigned long iteration = 0;

    clear_edge_colors();    
        
    // Gather messages in:
    for (auto iter = _mp_ordering.rbegin(); iter != _mp_ordering.rend() && iteration < this->_maximum_iterations; ++iter, ++iteration) {
       pass_all_messages_possible(*iter);
    }

    clear_edge_colors();

    // Scatter messages out:
    for (auto iter = _mp_ordering.begin(); iter != _mp_ordering.end() && iteration < this->_maximum_iterations; ++iter, ++iteration) {
      pass_all_messages_possible(*iter);
    }
    return iteration;
  }

  // Function to get edges which should be queued by FIFO scheduler
  ListQueue<VARIABLE_KEY> get_edges_that_can_pass() {
    ListQueue<VARIABLE_KEY> queue;
    for (MessagePasser<VARIABLE_KEY>* mp : _mp_ordering) {
      for (unsigned long edge_ind = 0; edge_ind < mp->number_edges(); ++edge_ind) {
    	if (mp->ready_to_send_message(edge_ind) && mp->get_edge_out(edge_ind)->color == 0) {
    	  queue.push_if_not_in_queue(mp->get_edge_out(edge_ind));	  
    	}
      }
    }
    return queue;
  }

  bool has_converged() const {
    return _have_edges_been_processed;
  }
};

#endif
