#ifndef _FORCE_INLINE_HPP
#define _FORCE_INLINE_HPP

// Create a macro for ALWAYS_INLINE (different modifiers on different compilers)
#if defined(_MSC_VER)
#define FORCE_INLINE __forceinline
#else
#define FORCE_INLINE inline __attribute__((always_inline))
#endif

#endif
