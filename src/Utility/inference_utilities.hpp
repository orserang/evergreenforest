#ifndef _INFERENCE_UTILITIES_HPP
#define _INFERENCE_UTILITIES_HPP

#include "Clock.hpp"
#include <iostream>
#include <algorithm>
#include "../Utility/FLOAT_TYPE.hpp"

template<template <typename, typename...> class CONTAINER, typename T, typename ...OTHER_ARGS>
std::vector<std::vector<T> > make_singletons(const CONTAINER<T, OTHER_ARGS...> & var_container) {
  std::vector<std::vector<T> > result;
  for (const T & t : var_container)
    result.push_back({t});
  return result;
}

template <typename T>
std::vector<T> flatten(const std::vector<std::vector<T> > & container) {
  std::vector<T> result;
  for (const std::vector<T> & row : container)
    for (const T & var : row)
      result.push_back(var);
  return result;
}

template <typename T>
std::vector<T> from_singletons(const std::vector<std::vector<T> > & singletons) {
  for (const std::vector<T> & t : singletons)
    assert( t.size() == 1 );

  return flatten(singletons);
}

template <typename T>
void estimate_and_print_posteriors(BruteForceInferenceEngine<T> & bf, const std::vector<std::vector<T> > & singletons){
  Clock c;
  auto result = bf.estimate_posteriors(singletons);
  c.ptock();
  for (auto res : result)
    std::cout << res << std::endl;
}

template <typename T>
void estimate_and_print_posteriors(BeliefPropagationInferenceEngine<T> & bpie, const std::vector<std::vector<T> > & singletons){
  Clock c;
  auto result = bpie.estimate_posteriors(singletons);
  c.ptock();
  for (auto res : result)
    std::cout << res << std::endl;
}

template <typename T>
LabeledPMF<T> make_nonneg_uniform(const T & var_name, unsigned long max_val) {
  Tensor<FLOAT_TYPE> ten({max_val+1});
  ten.flat().fill(1.0);
  PMF pmf( {0L}, ten );
  return LabeledPMF<T>({var_name}, pmf);
}

inline FLOAT_TYPE gaussian_density(FLOAT_TYPE x, const FLOAT_TYPE mu, const FLOAT_TYPE sigma){
  FLOAT_TYPE var = sigma*sigma;
  FLOAT_TYPE dev = (x - mu);
  return ( expl(-(dev*dev) / (2*var)) / sigma ) / (sigma * sqrtl(2*M_PI));
}

inline FLOAT_TYPE inverse_standard_norm_cdf(const FLOAT_TYPE p) {
  assert(p >= 0.5L);

  return 5.5556L*(1.0L-powl((1.0L-p)/p, 0.1186L));
}

template <typename T>
TableDependency<T> table_dependency_by_gaussian(const T & label, const Vector<long> & support, FLOAT_TYPE goal, const FLOAT_TYPE p, const FLOAT_TYPE sd){
  Tensor<FLOAT_TYPE> pmf({support.size()});
  for(unsigned int i=0; i<support.size(); ++i)
    pmf[i] = gaussian_density(support[i], goal, sd);

  TableDependency<T> td(LabeledPMF<T>({label}, PMF({(long)floorl(support[0])}, pmf)), p);
  return td;
}


template <typename T>
LabeledPMF<T> make_gaussian(const T & label, FLOAT_TYPE mu, const FLOAT_TYPE sigma, const FLOAT_TYPE epsilon){
  const FLOAT_TYPE max_z = inverse_standard_norm_cdf(1-epsilon);
  const FLOAT_TYPE min_z = -max_z;

  // z-score = (x - mu) / sigma
  // --> x = z*sigma + mu

  // Find minimum and maximum integer values beyond which tails of
  // Gaussian are < epsilon.
  FLOAT_TYPE min_double_support = mu + min_z*sigma;
  FLOAT_TYPE max_double_support = mu + max_z*sigma;

  long min_support = (long) floorl(min_double_support);
  long max_support = (long) ceill(max_double_support);

  Tensor<FLOAT_TYPE> table({ (unsigned long)(max_support - min_support + 1) });
  for(unsigned long i=0; i<table.flat_size(); ++i)
    table[i] = gaussian_density(min_support + long(i), mu, sigma);

  return LabeledPMF<T>( {label}, PMF({min_support}, table) );
}

template <typename T>
LabeledPMF<T> make_nonneg_gaussian(const T & label, FLOAT_TYPE mu, const FLOAT_TYPE sigma, const FLOAT_TYPE epsilon){
  const FLOAT_TYPE max_z = inverse_standard_norm_cdf(1-epsilon);
  const FLOAT_TYPE min_z = -max_z;

  // z-score = (x - mu) / sigma
  // --> x = z*sigma + mu

  // Find minimum and maximum integer values beyond which tails of
  // Gaussian are < epsilon.
  FLOAT_TYPE min_double_support = mu + min_z*sigma;
  FLOAT_TYPE max_double_support = mu + max_z*sigma;

  long min_support = (long) floorl(min_double_support);
  long max_support = (long) ceill(max_double_support);

  min_support = std::max(0L, min_support);
  assert(max_support >= min_support);

  Tensor<FLOAT_TYPE> table({ (unsigned long)(max_support - min_support + 1) });
  for(unsigned long i=0; i<table.flat_size(); ++i)
    table[i] = gaussian_density(min_support + long(i), mu, sigma);
  return LabeledPMF<T>({label}, PMF({min_support}, table));
}

// Like a Gaussian but with guaranteed minimum probability (to
// increase the density of tails):
template <typename T>
LabeledPMF<T> make_nonneg_pseudo_gaussian(const T & label, FLOAT_TYPE mu, const FLOAT_TYPE sigma, const FLOAT_TYPE epsilon, long max_support, FLOAT_TYPE pseudo_count){
  const FLOAT_TYPE max_z = inverse_standard_norm_cdf(1-epsilon);

  // z-score = (x - mu) / sigma
  // --> x = z*sigma + mu

  // Find minimum and maximum integer values beyond which tails of
  // Gaussian are < epsilon.
  FLOAT_TYPE max_double_support = mu + max_z*sigma;

  long min_support = 0;
  max_support = std::max( (long) ceill(max_double_support), max_support);

  assert(max_support >= min_support);

  Tensor<FLOAT_TYPE> table({ (unsigned long)(max_support - min_support + 1) });
  for(unsigned long i=0; i<table.flat_size(); ++i)
    table[i] = std::max( gaussian_density(min_support + long(i), mu, sigma), pseudo_count);
  return LabeledPMF<T>({label}, PMF({min_support}, table));
}

template <typename T>
LabeledPMF<T> make_bernoulli(const T & var_name, FLOAT_TYPE probability_x_equals_one) {
  assert(probability_x_equals_one >= 0.0L && probability_x_equals_one <= 1.0L && "make_bernoulli must receive a valid probability");
  return LabeledPMF<T>({var_name}, PMF({0L}, Tensor<FLOAT_TYPE>({2ul}, {1-probability_x_equals_one, probability_x_equals_one})));
}

#endif
