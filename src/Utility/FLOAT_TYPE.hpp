#ifndef _FLOAT_TYPE_HPP
#define _FLOAT_TYPE_HPP

  #ifdef FAST_FLOAT
    typedef float FLOAT_TYPE;
  #else
    #ifdef ACCURATE_FLOAT
      typedef long double FLOAT_TYPE;
    #else
      // standard
      typedef double FLOAT_TYPE;
    #endif
  #endif

#endif
