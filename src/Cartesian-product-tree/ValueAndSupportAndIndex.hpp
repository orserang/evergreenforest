#ifndef _VALUE_AND_SUPPORT_AND_INDEX_HPP
#define _VALUE_AND_SUPPORT_AND_INDEX_HPP

using namespace serang_lab;

template <typename T>
struct ValueAndSupportAndIndex {
  T val;
	long support;
  unsigned long i;
  unsigned long j;
  unsigned long layer_i;
  unsigned long layer_j;

  void operator =(const ValueAndSupportAndIndex & rhs) {
    val = rhs.val;
		support = rhs.support;
    i = rhs.i;
    j = rhs.j;
    layer_i = rhs.layer_i;
    layer_j = rhs.layer_j;    
  }

  void operator +=(const ValueAndSupportAndIndex & rhs) {
    val += rhs.val;
		support += rhs.support;
  }

  ValueAndSupportAndIndex():
    val(T(0)),
		support(0),
    i(0),
    j(0),
    layer_i(0),
    layer_j(0)    
  {}

  ValueAndSupportAndIndex(T v, long new_support, unsigned long ii, unsigned long jj, unsigned long ll_i, unsigned long ll_j):
    val(v),
		support(new_support),
    i(ii),
    j(jj),
    layer_i(ll_i),
    layer_j(ll_j)    
  {}

  ValueAndSupportAndIndex(ValueAndSupportAndIndex & rhs):
    val(rhs.val),
		support(rhs.support),
    i(rhs.i),
    j(rhs.j),
    layer_i(rhs.layer_i),
    layer_j(rhs.layer_j)
  { }

  ValueAndSupportAndIndex(ValueAndSupportAndIndex && rhs):
    val(0),
		support(0),
    i(0),
    j(0),
		layer_i(0),
		layer_j(0)		
  {
    std::swap(val, rhs.val);
		std::swap(support, rhs.support);
    std::swap(i, rhs.i);
    std::swap(j, rhs.j);
    std::swap(layer_i, rhs.layer_i);
    std::swap(layer_j, rhs.layer_j);        
  }

  friend std::ostream & operator<<(std::ostream & os, const ValueAndSupportAndIndex & rhs) {
    os << rhs.val << " (" << rhs.support << ") (" << rhs.i << ", " << rhs.j << ") " << rhs.layer_i << ", " << rhs.layer_j << std::endl;
    return os;
  }
};

template <typename T>
bool operator <(const ValueAndSupportAndIndex<T> & lhs, const ValueAndSupportAndIndex<T> & rhs) {
  return lhs.val < rhs.val;
}

template <typename T>
bool operator >(const ValueAndSupportAndIndex<T> & lhs, const ValueAndSupportAndIndex<T> & rhs)  {
  return lhs.val > rhs.val;
}

template <typename T>
bool operator ==(const ValueAndSupportAndIndex<T> & lhs, const ValueAndSupportAndIndex<T> & rhs) {
  return lhs.val == rhs.val;
}

template <typename T>
ValueAndSupportAndIndex<T> operator +(const ValueAndSupportAndIndex<T> & lhs, const ValueAndSupportAndIndex<T> & rhs)  {
  ValueAndSupportAndIndex<T> result(lhs.val + rhs.val, lhs.support + rhs.support, lhs.i, rhs.j, lhs.layer_i, rhs.layer_j);
  return result;
}

#endif
