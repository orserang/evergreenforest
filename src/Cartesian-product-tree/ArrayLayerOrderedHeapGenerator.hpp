#ifndef _ARRAYLAYERORDEREDHEAPGENERATOR_HPP
#define _ARRAYLAYERORDEREDHEAPGENERATOR_HPP

#if defined INDICES
#include "./LayerOrderedHeapGenerator.hpp"
#else
#include "LayerOrderedHeapGenerator.hpp"
#endif

#include "Datastructures/LOH/LayerOrderedHeap.hpp"


namespace serang_lab {
  
template <typename T>
class ArrayLayerOrderedHeapGenerator : public LayerOrderedHeapGenerator<T>{
private:
  LayerOrderedHeap<T> _loh;

public:
  ArrayLayerOrderedHeapGenerator(T*data, long n, LayerArithmetic*la, std::function<bool(const T&,const T&)> compare = [](const auto & lhs, const auto & rhs){return lhs>rhs;}):
    LayerOrderedHeapGenerator<T>(la),

    _loh(data, n, la, compare)
  {
    this->_number_of_elements_possible = n;
    this->_number_of_elements_generated = 0;

    #ifdef INDICES
    this->_number_leaves = 1;
    #endif
  }

  T*layer_begin(unsigned long layer_i) const override {
    return _loh.layer_begin(layer_i);
  }
  T*layer_end(unsigned long layer_i) const override {
    return _loh.layer_end(layer_i);
  }

  void compute_next_layer_if_available() override {
    if (this->are_more_layers_available()){
      this->_number_of_elements_generated += this->layer_size(this->_number_of_layers_generated);
      ++this->_number_of_layers_generated;
    }
  }

  bool is_leaf() override {
    return true;
  }

  #ifdef INDICES
  void get_m_tuple_index(T result, unsigned long m, unsigned long* index, unsigned long offset=0){
		index[0] = result.i;
  }
  #endif
};

}

#endif
