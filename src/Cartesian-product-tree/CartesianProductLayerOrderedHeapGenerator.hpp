#ifndef _CARTESIANPRODUCTLAYERORDEREDHEAPGENERATOR_HPP
#define _CARTESIANPRODUCTLAYERORDEREDHEAPGENERATOR_HPP

#include "./DynamicLayerOrderedHeapGenerator.hpp"

using namespace serang_lab;

template <typename T>
class CartesianProductLayerOrderedHeapGenerator : public DynamicLayerOrderedHeapGenerator<T>{
protected:
  LayerOrderedHeapGenerator<T>*_loh_a;
  LayerOrderedHeapGenerator<T>*_loh_b;
  
public:
  CartesianProductLayerOrderedHeapGenerator(LayerOrderedHeapGenerator<T> *loh_a, LayerOrderedHeapGenerator<T> *loh_b, LayerArithmetic*la):
    DynamicLayerOrderedHeapGenerator<T>(la),
    _loh_a(loh_a),
    _loh_b(loh_b)
  { 
    this->_number_of_elements_possible = _loh_a->n_elements_possible()*_loh_b->n_elements_possible();

    #ifdef INDICES
      this->_number_leaves = this->_loh_a->number_leaves() + this->_loh_b->number_leaves();
    #endif
  }

	virtual ~CartesianProductLayerOrderedHeapGenerator() {
		delete _loh_a;
		delete _loh_b;		
	}
};



#endif
