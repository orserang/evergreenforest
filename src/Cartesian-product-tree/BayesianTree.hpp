#ifndef __BAYESIAN_TREE_HPP
#define __BAYESIAN_TREE_HPP

#include <numeric>

#include "ValueAndSupport.hpp"

#include "./ArrayLayerOrderedHeapGenerator.hpp"
#include "./LayerOrderedHeapGenerator.hpp"

#ifdef INDICES
#include "ValueAndSupportAndIndex.hpp"
typedef ValueAndSupportAndIndex<double> DATATYPE;
#else
typedef ValueAndSupport<double> DATATYPE;
#endif


#ifdef WOBBLY
#include "./APlusBLayerOrderedHeapGeneratorWobbly.hpp"
#else
#include "./APlusBLayerOrderedHeapGeneratorStrict.hpp"
#endif
#include "Utility/io.hpp"


namespace serang_lab {

template <typename T>
class BayesianTree{
private:
  T** _prior_probabilities;
  unsigned long* _prior_probability_sizes;  
  LayerOrderedHeapGenerator<T>* _root;
  LayerArithmetic* _la;
  unsigned long _m;
	std::function<bool(const T&,const T&)> _compare;
	unsigned long _max_k;
	bool _tree_has_been_built;

	void _build_tree_single_leaf(long index_to_leave_out=-1) {
		if (index_to_leave_out > -1) {
			if (index_to_leave_out == 0) 
				_root = new ArrayLayerOrderedHeapGenerator<T>(_prior_probabilities[1], _prior_probability_sizes[1], _la, _compare);
			else 
				_root = new ArrayLayerOrderedHeapGenerator<T>(_prior_probabilities[0], _prior_probability_sizes[0], _la, _compare);
		}
		else {
			_root = new ArrayLayerOrderedHeapGenerator<T>(_prior_probabilities[0], _prior_probability_sizes[0], _la, _compare);
		}
	}
	
  void _build_tree(unsigned long number_leaves, long index_to_leave_out=-1) {
		if ((number_leaves == 2 && index_to_leave_out > -1) || number_leaves == 1)
			_build_tree_single_leaf(index_to_leave_out);

		_tree_has_been_built=true;		
    std::vector<LayerOrderedHeapGenerator<T>* > current_layer;
		_max_k = 1;

		if (index_to_leave_out > -1) {
			for (long i=0; i < index_to_leave_out; ++i) {
				ArrayLayerOrderedHeapGenerator<T>* leaf = new ArrayLayerOrderedHeapGenerator<T>(_prior_probabilities[i], _prior_probability_sizes[i], _la, _compare);
				current_layer.push_back(leaf);
				unsigned long index = (unsigned long) i;
				_max_k *= _prior_probability_sizes[index];
			}
			for (unsigned long i=(index_to_leave_out+1); i < number_leaves; ++i) {
				ArrayLayerOrderedHeapGenerator<T>* leaf = new ArrayLayerOrderedHeapGenerator<T>(_prior_probabilities[i], _prior_probability_sizes[i], _la, _compare);
				_max_k *= _prior_probability_sizes[i];				
				current_layer.push_back(leaf);

			}
		}
		else {
      for (unsigned long i=0; i < number_leaves; ++i) {
        ArrayLayerOrderedHeapGenerator<T>* leaf = new ArrayLayerOrderedHeapGenerator<T>(_prior_probabilities[i], _prior_probability_sizes[i], _la, _compare);
				_max_k *= _prior_probability_sizes[i];				
        current_layer.push_back(leaf);
      }
		}

    while (current_layer.size() > 1){
      std::vector<LayerOrderedHeapGenerator<T>* > next_layer;
      for (unsigned long i=0; i < (current_layer.size()>>1); ++i) 
				next_layer.emplace_back(new APlusBLayerOrderedHeapGenerator<T>(current_layer[2*i], current_layer[2*i + 1], _la, _compare));

      if (current_layer.size()%2 == 1)
    	next_layer.push_back(current_layer[current_layer.size() - 1]);
      current_layer = next_layer;
    }
     _root = current_layer[0];
  }

	std::pair<T*, unsigned long> select_on_built_tree(unsigned long k) {
		#ifdef SAFE
		assert(_tree_has_been_built);
		#endif
		if (k > _max_k && _max_k > 0)
			k = _max_k;

    while (_root->n_elements_generated() < k) 
      _root->compute_next_layer_if_available();

    T* result = new T[_root->n_elements_generated()];
    long result_index = 0;
    for (unsigned layer = 0; layer < _root->n_layers_generated(); ++layer){
      for (long i = 0; i < (_root->layer_end(layer) - _root->layer_begin(layer)); ++i){
				result[result_index] = *(_root->layer_begin(layer)+i);
				++result_index;
      }
    }
    std::nth_element(result, result+k-1, result + _root->n_elements_generated(), _compare);
    return {result, _root->n_elements_generated()};
	}

	void _negate_supports(T* v, unsigned long n) {
		for (unsigned long i=0; i<n; ++i)
			v[i].support *= -1;
	}

	double* _get_zeroed_vector(unsigned long size) {
		double* v = new double[size];
		for (unsigned long i=0; i<size; ++i)
		  v[i] = log(0);
		return v;		
	}

	void _swap_priors(unsigned long i, unsigned long j) {
		std::swap(_prior_probabilities[i], _prior_probabilities[j]);
		std::swap(_prior_probability_sizes[i], _prior_probability_sizes[j]);
	}

	void _multiply_in_prior(double* results, unsigned long i) {
		for (unsigned long j=0; j<_prior_probability_sizes[i]; ++j) 
			results[j] += _prior_probabilities[i][j].val;
	}	

public:
  BayesianTree(T** input_prior_probabilities, unsigned long* input_prior_probability_sizes, LayerArithmetic* la, unsigned long m, std::function<bool(const T&,const T&)> compare = [](const auto & lhs, const auto & rhs){return lhs>rhs;}):
    _prior_probabilities(input_prior_probabilities),
    _prior_probability_sizes(input_prior_probability_sizes),
    _la(la),
    _m(m),
		_compare(compare),
		_tree_has_been_built(false)
  {}

	~BayesianTree() {
		delete _root;
	}

  std::pair<double*, unsigned long> marginal_on_y(unsigned long k){
    _build_tree(_m);

		if (_max_k > 0)
			k = std::min(k, _max_k);
		
		#ifdef SAFE
		if (_max_k == 0) {
			printn("ERROR WITH -D SAFE; _max_k has overflowed to be 0");
			exit(1);
		}
		
		k = _max_k;
		#endif

		std::pair<T*, unsigned long> probabilities_and_support_with_size = select_on_built_tree(k);
		T* probabilities_and_support = probabilities_and_support_with_size.first;
		k = probabilities_and_support_with_size.second;

		double* marginal_on_y = _get_zeroed_vector(_prior_probability_sizes[_m]);

		// Fill in with max values from LLH on Y
		for (unsigned long i=0; i<k; ++i) 
		  marginal_on_y[probabilities_and_support[i].support] = std::max(marginal_on_y[probabilities_and_support[i].support], probabilities_and_support[i].val);

		// Multiply in prior on Y
		_multiply_in_prior(marginal_on_y, _m);

		delete[] probabilities_and_support;

		return {marginal_on_y, _prior_probability_sizes[_m]};
  }

	std::pair<double*, unsigned long> marginal_on_y_holding_out_x_i(long x_i, unsigned long k_from_additive_sum, unsigned long k_from_x_i) {
		if (_max_k > 0)
			k_from_additive_sum = std::min(k_from_additive_sum, _max_k);

		unsigned long k_prod = k_from_additive_sum * k_from_x_i;
		k_from_x_i = std::min(k_from_x_i,  _prior_probability_sizes[x_i]);
		k_from_additive_sum = k_prod / k_from_x_i;


		#ifdef SAFE
		if (_max_k == 0) {				
				printn("PROBLEM WITH -D SAFE");
				exit(1);
			}
		
		k_from_additive_sum = _max_k;
		k_from_x_i = _prior_probability_sizes[x_i];
		#endif

		_build_tree(_m, x_i);		
		std::pair<T*, unsigned long> marginals_on_y_without_x_i_and_size = select_on_built_tree(k_from_additive_sum);

		T* marginals_on_y_without_x_i= marginals_on_y_without_x_i_and_size.first;
		k_from_additive_sum = marginals_on_y_without_x_i_and_size.second;

		// Vector of probabilities, initialize to 0
		double* marginal_probabilities_on_y = _get_zeroed_vector(_prior_probability_sizes[_m]);

		// Cartesian product of top k_from_x_i values from X_i with the top k_from_additive_sum values from Y
		std::nth_element(_prior_probabilities[x_i], _prior_probabilities[x_i] + (k_from_x_i-1), _prior_probabilities[x_i] + _prior_probability_sizes[x_i], [](auto lhs, auto rhs){return lhs > rhs;});

		for (unsigned long i=0; i<k_from_x_i; ++i)
			for (unsigned long j=0; j<k_from_additive_sum; ++j) {
			  DATATYPE v = _prior_probabilities[x_i][i] + marginals_on_y_without_x_i[j];

				if (0 <= v.support && v.support < (long) _prior_probability_sizes[_m])
					marginal_probabilities_on_y[v.support] = std::max(v.val, marginal_probabilities_on_y[v.support]);
			}

		// Multiply in prior on Y
		_multiply_in_prior(marginal_probabilities_on_y, _m);

		delete[] marginals_on_y_without_x_i;

		return {marginal_probabilities_on_y, _prior_probability_sizes[_m]};
	}
	

	std::pair<double*, unsigned long> marginal_on_x_i(unsigned long x_i, unsigned long k) {
		// Reverse Y so that it is subtracted
		_negate_supports(_prior_probabilities[_m], _prior_probability_sizes[_m]);

		// Swap X_i, Y so the convolution is performed with -Y and NOT X_i
		_swap_priors(x_i, _m);

	  const unsigned long x_i_index_after_swap = _m;

		_build_tree(_m); 
		if (_max_k > 0)
			k = std::min(k, _max_k);
		k = std::min(k, _prior_probability_sizes[x_i_index_after_swap]);

		#ifdef SAFE
		if (_max_k == 0) {
			printn("ERROR WITH -D SAFE; _max_k has overflowed to be 0");
			exit(1);
		}

		k = _max_k;
		#endif

		// Top k values of X_1 + X_2 + \cdots + X_{i-1} + X_{i+1} + \cdots + X_m
		std::pair<T*, unsigned long> marginal_values_and_supports_from_additive_sum_and_size = select_on_built_tree(k);
		T* marginal_values_and_supports_from_additive_sum = marginal_values_and_supports_from_additive_sum_and_size.first;
		k = marginal_values_and_supports_from_additive_sum_and_size.second;

		double* marginals = _get_zeroed_vector(_prior_probability_sizes[x_i_index_after_swap]);

		for (unsigned long j=0; j < k; ++j) {
			double probability = marginal_values_and_supports_from_additive_sum[j].val;

			// Solves for -X_i, so negate the support
			long support_in_a = -1*marginal_values_and_supports_from_additive_sum[j].support;
			if (0 <= support_in_a && support_in_a < (long) _prior_probability_sizes[x_i_index_after_swap]) 
					marginals[support_in_a] = std::max(probability, marginals[support_in_a]);
		}

		_swap_priors(x_i, _m);
		_negate_supports(_prior_probabilities[_m], _prior_probability_sizes[_m]);
	  _multiply_in_prior(marginals, x_i);

		delete[] marginal_values_and_supports_from_additive_sum;
		
		return {marginals, _prior_probability_sizes[x_i]};
	}
	
	std::pair<double*, unsigned long> marginal_on_x_i_holdout_x_j(unsigned long x_i,unsigned long x_j, unsigned long k_from_additive_sum, unsigned long k_from_x_j) {
		#ifdef SAFE
		assert(_m > 2);
		#endif

		// Reverse Y so that it is subtracted
		_negate_supports(_prior_probabilities[_m], _prior_probability_sizes[_m]);
		_swap_priors(x_i, _m);

	  const unsigned long x_i_index_after_swap = _m;
		const unsigned long y_index_after_swap = x_i;

		_build_tree(_m, x_j); // leave out x_j

		if (_max_k > 0)
			k_from_additive_sum = std::min(k_from_additive_sum, _max_k);

		k_from_additive_sum = std::min(k_from_additive_sum, _prior_probability_sizes[x_i_index_after_swap]);
		k_from_x_j = std::min(k_from_x_j, _prior_probability_sizes[x_j]);

		#ifdef SAFE
		if (_max_k == 0) {
			printn("ERROR WITH -D SAFE; _max_k has overflowed to be 0");
			exit(1);
		}

		k_from_additive_sum = _max_k;
		k_from_x_j = _prior_probability_sizes[x_j];
		#endif

		// Top k values of X_1 + X_2 + \cdots + X_{i-1} + X_{i+1} + \cdots + X_m
		std::pair<T*, unsigned long> marginal_values_and_supports_from_additive_sum_and_size = select_on_built_tree(k_from_additive_sum);
		T* marginal_values_and_supports_from_additive_sum = marginal_values_and_supports_from_additive_sum_and_size.first;
		k_from_additive_sum = marginal_values_and_supports_from_additive_sum_and_size.second;
		
		double* marginals = _get_zeroed_vector(_prior_probability_sizes[x_i_index_after_swap]);

		std::nth_element(_prior_probabilities[x_j], _prior_probabilities[x_j] + (k_from_x_j-1), _prior_probabilities[x_j] + _prior_probability_sizes[x_j], [](auto lhs, auto rhs){return lhs > rhs;});
		
		for (unsigned long i=0; i < k_from_x_j; ++i) 
			for (unsigned long j=0; j < k_from_additive_sum; ++j) {
				double probability = marginal_values_and_supports_from_additive_sum[j].val+ _prior_probabilities[x_j][i].val;
				// Solves for -X_i, so negate the support
				long support_in_a = -1*(marginal_values_and_supports_from_additive_sum[j].support + _prior_probabilities[x_j][i].support);
				if (0 <= support_in_a && support_in_a < (long) _prior_probability_sizes[x_i_index_after_swap]) 
					marginals[support_in_a] = std::max(probability, marginals[support_in_a]);
			}

		_swap_priors(x_i_index_after_swap, y_index_after_swap);

		_negate_supports(_prior_probabilities[_m], _prior_probability_sizes[_m]);

	  _multiply_in_prior(marginals, x_i);

		delete[] marginal_values_and_supports_from_additive_sum;

		return {marginals, _prior_probability_sizes[x_i]};
	}

	PrimitiveVector<std::pair<double*, unsigned long> >  marginal_on_all_x_j(unsigned long k) {
		#ifndef INDICES
		printn("ERROR: Need to compile with -D INDICES if you want marginal on all x_j");
		exit(1);
		#else

		if (_max_k > 0)
			k = std::min(k, _max_k);
		
		#ifdef SAFE
		if (_max_k == 0) {
			printn("ERROR WITH -D SAFE; _max_k has overflowed to be 0");
			exit(1);
		}
		
		k = _max_k;
		#endif

		_build_tree(_m);
		std::pair<T*, unsigned long> probabilities_and_support_with_size = select_on_built_tree(k);
		T* probabilities_and_support = probabilities_and_support_with_size.first;
		k = probabilities_and_support_with_size.second;

		unsigned long** indices_for_each_y = get_indices(probabilities_and_support, k, _m);

		PrimitiveVector<std::pair<double*, unsigned long> > marginals_and_sizes_for_all_x_j(_m);

		for (unsigned long j=0; j<_m; ++j) {
			marginals_and_sizes_for_all_x_j[j].second = _prior_probability_sizes[j];
			marginals_and_sizes_for_all_x_j[j].first = _get_zeroed_vector(marginals_and_sizes_for_all_x_j[j].second);
		}

		// Go through all Y values found and update marginals for X_j
		for (unsigned long i=0; i<k; ++i) {
		  unsigned long* index_for_y_value = indices_for_each_y[i];
			
			long y_support = probabilities_and_support[i].support;
			double y_value = probabilities_and_support[i].val;
			//double y_value = normalized_marginals_on_y[y_support];

			double prior_on_y = _prior_probabilities[_m][y_support].val;
			double marginal_on_y = prior_on_y + y_value;

			// Go through all x_j
			for (unsigned long j=0; j<_m; ++j) {
			  unsigned long index_in_x_j = index_for_y_value[j];
			  long x_j_support = _prior_probabilities[j][index_in_x_j].support;

				marginals_and_sizes_for_all_x_j[j].first[x_j_support] = std::max(marginals_and_sizes_for_all_x_j[j].first[x_j_support], marginal_on_y);
			}
		}

		delete[] probabilities_and_support;
		return marginals_and_sizes_for_all_x_j;
		#endif
	}


	PrimitiveVector<std::pair<double*, unsigned long> >  marginal_on_all_x_j_holdout_x_i(unsigned long x_i, unsigned long k_from_additive_sum, unsigned long k_from_x_i) {
		#ifndef INDICES
		printn("ERROR: Need to compile with -D INDICES if you want marginal on all x_j");
		exit(1);
		#else

		unsigned long k_prod = k_from_additive_sum * k_from_x_i;
		k_from_x_i = std::min(k_from_x_i,  _prior_probability_sizes[x_i]);
		k_from_additive_sum = k_prod / k_from_x_i;

		// Build tree, leaving out X_i
		_build_tree(_m, x_i);

		if (_max_k > 0) {
			k_from_additive_sum = std::min(k_from_additive_sum, _max_k);
			k_from_x_i = std::min(k_from_x_i, _max_k);
		}
		
		#ifdef SAFE
		if (_max_k == 0) {
			printn("ERROR WITH -D SAFE; _max_k has overflowed to be 0");
			exit(1);
		}

		k_from_additive_sum = _max_k;
		k_from_x_i = _prior_probability_sizes[x_i];
		#endif

		// Top k values of Y= X_0 + X_1 + X_2 + \cdots + X_{i-1} + X_{i+1} + \cdots + X_m
		std::pair<T*, unsigned long> marginal_values_and_supports_from_additive_sum_and_size = select_on_built_tree(k_from_additive_sum);
		T* marginal_values_and_supports_from_additive_sum = marginal_values_and_supports_from_additive_sum_and_size.first;
		k_from_additive_sum = marginal_values_and_supports_from_additive_sum_and_size.second;

		// Partition the prior_probabilities so that the top k are on the lhs
		std::nth_element(_prior_probabilities[x_i], _prior_probabilities[x_i] + (k_from_x_i-1), _prior_probabilities[x_i] + _prior_probability_sizes[x_i], [](auto lhs, auto rhs){return lhs > rhs;});

		// Have to do _m-1 since we held out a variable
		unsigned long** indices_for_each_y = get_indices(marginal_values_and_supports_from_additive_sum, k_from_additive_sum, (_m-1)); 

		// Get zeroed out vectors for marginals on all X_j
		PrimitiveVector<std::pair<double*, unsigned long> > marginals_and_sizes_for_all_x_j(_m);
		for (unsigned long i=0; i<_m; ++i) {
			marginals_and_sizes_for_all_x_j[i].second = _prior_probability_sizes[i];
			marginals_and_sizes_for_all_x_j[i].first = _get_zeroed_vector(_prior_probability_sizes[i]);
		}

		// Cartesian product of values on Y with values from X_i
		for (unsigned long i=0; i < k_from_x_i; ++i) {
			const long support_in_x_j = _prior_probabilities[x_i][i].support;
			const double prior_on_x_j = _prior_probabilities[x_i][i].val;

			for (unsigned long h=0; h < k_from_additive_sum; ++h) {
				const long   support_in_Y  = marginal_values_and_supports_from_additive_sum[h].support + support_in_x_j;
				const double llh_on_y      = marginal_values_and_supports_from_additive_sum[h].val;
				const double prior_on_y    = _prior_probabilities[_m][support_in_Y].val;
				const double marginal_on_y =  llh_on_y + prior_on_x_j + prior_on_y;

				// Set marginal for X_i
				marginals_and_sizes_for_all_x_j[x_i].first[support_in_x_j] = std::max(marginals_and_sizes_for_all_x_j[x_i].first[support_in_x_j], marginal_on_y);
						
				unsigned long* index_for_y_value = indices_for_each_y[h];
				
				// Iterate over all X_{j!=i}
				for (unsigned long j=0; j< (_m-1); ++j) { // _m-1 since we are skipping X_{j=i}
					unsigned long index_in_x_j = index_for_y_value[j];
					
					// Adds one to jj (if jj >= x_i) to account for skipping over X_i when building the tree
					unsigned long modified_j_index = j + (j>=x_i);
					long x_j_support = _prior_probabilities[modified_j_index][index_in_x_j].support;

					// Set the probability for X_j[x_j_support]
					marginals_and_sizes_for_all_x_j[modified_j_index].first[x_j_support] = std::max(marginals_and_sizes_for_all_x_j[modified_j_index].first[x_j_support], marginal_on_y);
				}
			}
		}

		delete[] marginal_values_and_supports_from_additive_sum;

		return marginals_and_sizes_for_all_x_j;
		#endif
	}


	std::pair<double*, unsigned long>  marginal_on_just_x_0_holdout_x_0(unsigned long x_i, unsigned long k_from_additive_sum, unsigned long k_from_x_i) {
	  x_i = 0;

		unsigned long k_prod = k_from_additive_sum * k_from_x_i;
		k_from_x_i = std::min(k_from_x_i,  _prior_probability_sizes[x_i]);
		k_from_additive_sum = k_prod / k_from_x_i;

		// Build tree, leaving out X_i
		_build_tree(_m, x_i);

		if (_max_k > 0) {
			k_from_additive_sum = std::min(k_from_additive_sum, _max_k);
			k_from_x_i = std::min(k_from_x_i, _max_k);
		}
		
		#ifdef SAFE
		if (_max_k == 0) {
			printn("ERROR WITH -D SAFE; _max_k has overflowed to be 0");
			exit(1);
		}

		k_from_additive_sum = _max_k;
		k_from_x_i = _prior_probability_sizes[x_i];
		#endif

		// Top k values of Y= X_0 + X_1 + X_2 + \cdots + X_{i-1} + X_{i+1} + \cdots + X_m
		std::pair<T*, unsigned long> marginal_values_and_supports_from_additive_sum_and_size = select_on_built_tree(k_from_additive_sum);
		T* marginal_values_and_supports_from_additive_sum = marginal_values_and_supports_from_additive_sum_and_size.first;
		k_from_additive_sum = marginal_values_and_supports_from_additive_sum_and_size.second;

		// Partition the prior_probabilities so that the top k are on the lhs
		std::nth_element(_prior_probabilities[x_i], _prior_probabilities[x_i] + (k_from_x_i-1), _prior_probabilities[x_i] + _prior_probability_sizes[x_i], [](auto lhs, auto rhs){return lhs > rhs;});


		double* marginal_on_x_0 = _get_zeroed_vector(_prior_probability_sizes[0]);

		// Cartesian product of values on Y with values from X_i
		for (unsigned long i=0; i < k_from_x_i; ++i) {
			const long support_in_x_j = _prior_probabilities[x_i][i].support;
			const double prior_on_x_j = _prior_probabilities[x_i][i].val;

			for (unsigned long h=0; h < k_from_additive_sum; ++h) {
				const long   support_in_Y  = marginal_values_and_supports_from_additive_sum[h].support + support_in_x_j;
				const double llh_on_y      = marginal_values_and_supports_from_additive_sum[h].val;
				const double prior_on_y    = _prior_probabilities[_m][support_in_Y].val;
				const double marginal_on_y =  llh_on_y + prior_on_x_j + prior_on_y;

				// Set marginal for X_i

				marginal_on_x_0[support_in_x_j] = std::max(marginal_on_x_0[support_in_x_j], marginal_on_y);
			}
		}

		//delete[] k_from_additive_sum;

		return {marginal_on_x_0, _prior_probability_sizes[0]};
	}
	


  #ifdef INDICES
  unsigned long** get_indices(T* results, unsigned long k, unsigned long m) {
    unsigned long** indices = new unsigned long*[k];

    for (unsigned long i=0; i < k; ++i) {
      unsigned long* index = new unsigned long[m];
      _root->get_m_tuple_index(results[i], m, index, 0);
      indices[i] = index;
    }
    return indices;
  }
  #endif
  
};
}

#endif
