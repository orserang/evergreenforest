#ifndef _LAYERORDEREDHEAPGENERATOR_HPP
#define _LAYERORDEREDHEAPGENERATOR_HPP

#include "Datastructures/LOH/LayerArithmetic.hpp"

namespace serang_lab {
  
template <typename T>
class LayerOrderedHeapGenerator{
protected:
  LayerArithmetic*_la;
  
  long _number_of_layers_generated;
  long _number_of_elements_generated;
	// This is different than number of elements generated in that it
	// accounts for only those which have survived the trimming of
	// previously occupied supports
	long _number_of_elements_in_output;

  #ifdef INDICES
  unsigned long _number_leaves;
  #endif

  // Note: this uses long double so that it will have >64 bits for the
  // integer part and be safely compared to an unsigned long
  long double _number_of_elements_possible;

public:
  LayerOrderedHeapGenerator(LayerArithmetic*la):
    _la(la),
    _number_of_layers_generated(0),
    _number_of_elements_generated(0),
		_number_of_elements_in_output(0)
  {}

  virtual T*layer_begin(unsigned long layer_i) const = 0;
  virtual T*layer_end(unsigned long layer_i) const = 0;
  virtual void compute_next_layer_if_available() = 0;

  virtual ~LayerOrderedHeapGenerator() {}

  const T & min_in_layer(long layer_i) const {
    return *layer_begin(layer_i);
  }

  const T & max_in_layer(long layer_i) const {
    return *(layer_end(layer_i)-1);
  }

	unsigned long n_elements_output() const {
		return _number_of_elements_in_output;
	}

  unsigned long n_layers_generated() const {
    return _number_of_layers_generated;
  }

  unsigned long n_elements_generated() const {
    return _number_of_elements_generated;
  }

  long double n_elements_possible() const {
    return _number_of_elements_possible;
  }
  
  bool are_more_layers_available() const {
    return n_elements_generated() < n_elements_possible();
  }

  bool layer_has_been_generated(unsigned long layer_i) const {
    return layer_i < n_layers_generated();
  }

  unsigned long layer_size(unsigned long layer_i) const {
    if (layer_has_been_generated(layer_i))
      return layer_end(layer_i) - layer_begin(layer_i);

    // layer has not yet been made:
    _la->guarantee_i_layers_supported(layer_i);
    unsigned long start_index = _la->get_flat_layer_start_index(layer_i);
    unsigned long end_index = _la->get_flat_layer_end_index(layer_i);
    // Check that this layer is even in bounds:
    #ifdef SAFE
    assert(_number_of_elements_possible >= start_index);
    #endif
	
    // If this is the final layer and is partially full, use real size, not theoretical size:
    if (end_index < _number_of_elements_possible)
      return end_index - start_index;
    return _number_of_elements_possible - start_index;
  }

  unsigned long next_layer_size() const {
    unsigned long theoretical_layer_size = this->layer_size(this->_number_of_layers_generated);
    long double num_elements_remaining = this->n_elements_possible() - this->n_elements_generated();
    if (theoretical_layer_size < num_elements_remaining)
      return theoretical_layer_size;
    return (unsigned long)num_elements_remaining;
  }

	// Note: always returns false for all classes but
	// ArrayLayerOrderedHeapGenerator
  virtual bool is_leaf() {
    return false;
  }

  #ifdef INDICES
  virtual unsigned long number_leaves() {
    return _number_leaves;
  }

  virtual void get_m_tuple_index(T result, unsigned long m, unsigned long* index, unsigned long offset=0){
  }

  #endif


  
};

}

#endif
