#ifndef _APLUSBLAYERORDEREDHEAPGENERATOR_HPP
#define _APLUSBLAYERORDEREDHEAPGENERATOR_HPP

#include "./CartesianProductLayerOrderedHeapGenerator.hpp"

#include <iostream>
#include <unordered_set>

using namespace serang_lab;

template <typename T>
class APlusBLayerOrderedHeapGenerator : public CartesianProductLayerOrderedHeapGenerator<T> {
protected:
  struct LayerProductCornerIndex {
    unsigned long i,j;
    bool is_max;

    bool operator <(const LayerProductCornerIndex & rhs) const {
      if (i < rhs.i)
				return true;
      if (i > rhs.i)
				return false;
      if (j < rhs.j)
				return true;
      if (j > rhs.j)
				return false;
			
      if (is_max < rhs.is_max)
				return true;
      return false;
    }
		
    bool operator ==(const LayerProductCornerIndex & rhs) const {
      return i == rhs.i && j == rhs.j && is_max == rhs.is_max;
    }

    friend std::ostream & operator<<(std::ostream & os, const LayerProductCornerIndex & rhs) {
      return (os << "(" << rhs.i << "," << rhs.j << ")," << rhs.is_max);
    }
  };

  struct LayerProductCornerIndexHash {
    std::size_t operator() (const LayerProductCornerIndex & lpci) const {
      return ((lpci.i<<32) ^ lpci.j ^ lpci.is_max);
    }
  };

  struct LayerProductCorner {
    T val;
    LayerProductCornerIndex index;

    bool operator <(const LayerProductCorner & rhs) const {
      if (val < rhs.val)
	return true;
      if (val > rhs.val)
	return false;
      
      return index < rhs.index;
    }

    friend std::ostream & operator<<(std::ostream & os, const LayerProductCorner & rhs) {
      return (os << rhs.val << "," << rhs.index);
    }
  };

  PrimitiveVector<LayerProductCorner> _hull_heap;
  unsigned long _num_max_corner_elements_popped;
  unsigned long _all_cumulative_selections_to_date;
	std::unordered_set<unsigned long> _supports_used;


  PrimitiveVector<T> _values_considered;
	std::function<bool(const T&,const T&)> _negation_of_nth_element_compare_function;

  LayerProductCorner _min_corner(unsigned long i, unsigned long j) {
    return LayerProductCorner{this->_loh_a->min_in_layer(i)+this->_loh_b->min_in_layer(j), {i,j, false}};
  }
  LayerProductCorner _max_corner(unsigned long i, unsigned long j) {
    return LayerProductCorner{this->_loh_a->max_in_layer(i)+this->_loh_b->max_in_layer(j), {i,j, true}};
  }

  bool _in_bounds(unsigned long i, unsigned long j) {
    return i < this->_loh_a->n_layers_generated() && j < this->_loh_b->n_layers_generated();
  }

  void _extend_axes_as_necessary(unsigned long i, unsigned long j) {
    if (i >= this->_loh_a->n_layers_generated() && this->_loh_a->are_more_layers_available())
      this->_loh_a->compute_next_layer_if_available();
    if (j >= this->_loh_b->n_layers_generated() && this->_loh_b->are_more_layers_available())
      this->_loh_b->compute_next_layer_if_available();
  }

  void _insert_if_in_bounds_and_extend_axes_as_necessary(unsigned long i, unsigned long j, bool is_max) {
    _extend_axes_as_necessary(i,j);

    if (! _in_bounds(i,j))
      return;
		
    if (is_max)
      _hull_heap.push_back(_max_corner(i,j));
    else
      _hull_heap.push_back(_min_corner(i,j));
    // Invert < operator because heap defaults to max heap:

    std::push_heap(_hull_heap.begin(), _hull_heap.end(), [](const LayerProductCorner & lhs, const LayerProductCorner & rhs){return (lhs<rhs);});
  }

  virtual void _insert_neighbors_into_heap(unsigned long i, unsigned long j) {
    // Kaplan-like scheme: prevents adding twice and thus eliminates need for _hull_set:
    if (j==0)
      _insert_if_in_bounds_and_extend_axes_as_necessary(i+1, j, false);
    _insert_if_in_bounds_and_extend_axes_as_necessary(i, j+1, false);
    _insert_if_in_bounds_and_extend_axes_as_necessary(i, j, true);
  }

  virtual void _insert_values_considered(unsigned long i, unsigned long j) {
    unsigned long result_index = _values_considered.size();
    _values_considered.resize(_values_considered.size() + this->_loh_a->layer_size(i)*this->_loh_b->layer_size(j));
    T*a_layer = this->_loh_a->layer_begin(i);
    T*b_layer = this->_loh_b->layer_begin(j);
    const unsigned long a_layer_size = this->_loh_a->layer_size(i);
    const unsigned long b_layer_size = this->_loh_b->layer_size(j);
    for (unsigned long ii=0; ii<a_layer_size; ++ii) {
      T a_val = a_layer[ii];
      T*location_in_values_considered = &_values_considered[result_index];

      #ifndef INDICES
      std::copy(b_layer, b_layer+b_layer_size, location_in_values_considered);
      #endif

      for (unsigned long jj=0; jj<b_layer_size; ++jj) {
				#ifdef INDICES
				location_in_values_considered[jj].val = a_val.val + b_layer[jj].val;
				location_in_values_considered[jj].support = a_val.support + b_layer[jj].support;
				location_in_values_considered[jj].i = ii;
				location_in_values_considered[jj].j = jj;
				location_in_values_considered[jj].layer_i = i;
				location_in_values_considered[jj].layer_j = j;
        #else
				//printn("Combining", location_in_values_considered[jj], "and", a_val, "to get", location_in_values_considered[jj] + a_val);

				location_in_values_considered[jj] += a_val;
       	#endif

      }
      result_index += b_layer_size;
    }
  }

  virtual unsigned long _get_layer_product_size(unsigned long i, unsigned long j) const {
    return this->_loh_a->layer_size(i)*this->_loh_b->layer_size(j);
  }

  void _pop_next_layer_product() {
    #ifdef SAFE
    assert(_hull_heap.size() > 0);
    #endif
    // Invert < operator because heap defaults to max heap:
    
		std::pop_heap(_hull_heap.begin(), _hull_heap.end(), [](const LayerProductCorner & lhs, const LayerProductCorner & rhs){return (lhs<rhs);});

    //LayerProductCorner result = _hull_heap.back();
    long result_i = _hull_heap.back().index.i;
    long result_j = _hull_heap.back().index.j;
    long result_is_max = _hull_heap.back().index.is_max;    
    _hull_heap.pop_back();

    unsigned long layer_product_size = _get_layer_product_size(result_i, result_j);
    if (! result_is_max) {
      _insert_neighbors_into_heap(result_i, result_j);
      // In same layer product tile, min will get popped before max;
      // therefore, only add values from min corners to avoid adding twice.
      _values_considered.reserve_more(layer_product_size);
      _insert_values_considered(result_i, result_j);
    }
    else {
      // Only count elements when max corner is popped:
      _num_max_corner_elements_popped += layer_product_size;
    }
  }

  // Note: k here is the new selection amount (not the cumulative)
	std::pair<T*, unsigned long> _select(unsigned long k) {
    _all_cumulative_selections_to_date += k;

    #ifdef SAFE
    assert( _all_cumulative_selections_to_date <= this->n_elements_possible() );
    #endif

    while (_num_max_corner_elements_popped < _all_cumulative_selections_to_date && (_hull_heap.size() > 0)) 
      _pop_next_layer_product();

	  if (_values_considered.size() < k) {
			k = _values_considered.size();
			printn("k is now", k);
		}

    #ifdef SAFE
    assert(_values_considered.size() >= k);
    #endif    

    unsigned long num_unused_vals = _values_considered.size() - k;

    std::nth_element(_values_considered.begin(), _values_considered.begin()+num_unused_vals-1, _values_considered.end(), [this](T lhs, T rhs){return lhs < rhs;});

    T*result = new T[k];

    std::copy(_values_considered.begin()+num_unused_vals, _values_considered.end(), result);
    _values_considered.resize(num_unused_vals);

    return {result, k};
  }


public:
  APlusBLayerOrderedHeapGenerator(LayerOrderedHeapGenerator<T>*loh_a, LayerOrderedHeapGenerator<T>*loh_b, LayerArithmetic*la, std::function<bool(const T&,const T&)> compare = [](const auto & lhs, const auto & rhs){return lhs<rhs;}):
    CartesianProductLayerOrderedHeapGenerator<T>(loh_a, loh_b, la),
		_negation_of_nth_element_compare_function(compare)
  {
    _insert_if_in_bounds_and_extend_axes_as_necessary(0,0,false);
    _all_cumulative_selections_to_date = 0;
    _num_max_corner_elements_popped = 0;
  }


	unsigned long filter_layer_of_repeated_supports(T* new_layer, unsigned long new_layer_size) {
		T* end_of_supports_not_used = std::partition(new_layer, new_layer + new_layer_size, [this](auto &lhs){ return (_supports_used.count(lhs.support) == 0); });
		unsigned long number_with_novel_supports = end_of_supports_not_used - new_layer;
		new_layer_size = number_with_novel_supports;
			
		if (new_layer_size > 0) {
			for (unsigned long i=0; i<new_layer_size; ++i) {
				#ifdef SAFE
				assert(_supports_used.find(lhs.support) == false);
				#endif 
				
				_supports_used.insert(new_layer[i].support);
			}
		}
		return number_with_novel_supports;
	}


  void compute_next_layer_if_available() override {
    if (this->are_more_layers_available()) {
      unsigned long new_layer_size = this->next_layer_size();

			std::pair<T*, unsigned long> new_layer_and_size = this->_select(new_layer_size);
			T* new_layer = new_layer_and_size.first;

			new_layer_size = new_layer_and_size.second;
			this->_number_of_elements_generated += new_layer_size;
			
			// unique supports touched is bad name. Really it is the number
			// of elements generated after trimming
			this->_number_of_elements_in_output += new_layer_size;
			this->add_layer(new_layer, new_layer_size);
    }
  }

  #ifdef INDICES
  void get_m_tuple_index(T result, unsigned long m, unsigned long* index, unsigned long offset=0){
    unsigned long i=result.i;
    unsigned long j=result.j;
    unsigned long layer_i = result.layer_i;
    unsigned long layer_j = result.layer_j;

    T*a_layer = this->_loh_a->layer_begin(layer_i);
    T*b_layer = this->_loh_b->layer_begin(layer_j);

    T a_val = a_layer[i];
    T b_val = b_layer[j];

    if (!this->_loh_a->is_leaf()) 
      this->_loh_a->get_m_tuple_index(a_val, m, index, offset);
    else // base case, child is a leaf
      index[offset] = a_val.i; // The index in the root is always stored in i

    offset += this->_loh_a->number_leaves();

    if (!this->_loh_b->is_leaf()) 
      this->_loh_b->get_m_tuple_index(b_val, m, index, offset);
    else // base case, child is a leaf
      index[offset] = b_val.i; // The index in the root is always stored in i

  }
  #endif

};

#endif
