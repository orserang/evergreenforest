#include <vector>
#include <assert.h>
#include <tuple>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <cstring>

#include "BayesianTree.hpp"
#include "Utility/io.hpp"
#include "Utility/Clock.hpp"
#include "Datastructures/PrimitiveVector.hpp"

#include "ValueAndSupport.hpp"
#include "LayerOrderedHeap.hpp"

#ifdef INDICES
#include "ValueAndSupportAndIndex.hpp"
typedef ValueAndSupportAndIndex<double> DATATYPE;
#else
typedef ValueAndSupport<double> DATATYPE;
#endif

std::function<bool(const DATATYPE&,const DATATYPE&)> COMPARE = [](const auto & lhs, const auto & rhs){return lhs>rhs;};

void normalize(DATATYPE* probs, unsigned long n) {
	double sm = 0;
  for (unsigned long i=0; i<n; ++i) 
		sm += probs[i].val;

	for (unsigned long i=0; i<n; ++i)
		probs[i].val = probs[i].val/sm;
}
void _log(DATATYPE* probs, unsigned long n) {
	for (unsigned long i=0; i<n; ++i)
		probs[i].val = log( probs[i].val );
}

void normalize(double* probs, unsigned long n) {
	double sm = 0;
  for (unsigned long i=0; i<n; ++i) 
		sm += probs[i];

	for (unsigned long i=0; i<n; ++i)
		probs[i] = probs[i]/sm;
}

void normalize(std::pair<double*, unsigned long > prob_and_n) {
  normalize(prob_and_n.first, prob_and_n.second);
}

void normalize_and_exp(std::pair<double*, unsigned long> vals_and_len) {
	double sm = 0;
  for (unsigned long i=0; i<vals_and_len.second; ++i)  {
	  vals_and_len.first[i] = exp(vals_and_len.first[i]);
		sm += vals_and_len.first[i];
	}

	if (sm == 0) {
		for (unsigned long i=0; i<vals_and_len.second; ++i)
			vals_and_len.first[i] = 0;
	} else {
		for (unsigned long i=0; i<vals_and_len.second; ++i)
			vals_and_len.first[i] = vals_and_len.first[i]/sm;
	}
}


void write_evg_file(DATATYPE** data, unsigned long* data_sizes, unsigned long m, bool print_all_xi) {
	// Used to create a .evg file to run Evergreen on
	// NOT necessary to run CPP version of CPT
	std::ofstream evgfile;
	evgfile.open("test.evg");
	evgfile << "@p=inf\n";
	for (unsigned long i=0; i<m; ++i) {
		evgfile << "PMF (X" << i << ") (0) [";
		for (unsigned long j=0; j<data_sizes[i]-1; ++j) 
			evgfile << data[i][j].val << ", ";
		evgfile << data[i][data_sizes[i]-1].val << "]" << "\n";
	}
	evgfile << "PMF (Y) (0) [";

	for (unsigned long j=0; j<data_sizes[m]-1; ++j) 
		evgfile << (data[m][j].val) << ", ";
	evgfile << data[m][data_sizes[m]-1].val << "]" << "\n";

	evgfile << "Y=";
	for (unsigned long i=0; i<m-1; ++i)	
		evgfile << "X" << i << "+";
	evgfile << "X" << m-1 << "\n";

	evgfile << "@tick\n";
	evgfile << "Pr(Y;X0";

	// note: skip X0 since hardcoded
	if (print_all_xi) {
		evgfile << ";";
	  for (unsigned long i=1; i<m-1; ++i) 
	  	evgfile << "X" << i << ";";
	  evgfile << "X" << m-1;
	}

	evgfile << ")\n";
	evgfile << "@tock\n";	
	evgfile.close();		
}

void copy_data(DATATYPE** original_data, DATATYPE** new_data, unsigned long* original_data_sizes, unsigned long* new_data_sizes, unsigned long m) {
	// Copy data to use on different methods. Methods may re-arrange
	// data so just run with own copy
  for (unsigned long i=0; i < m+1; ++i) {
    new_data[i] = new DATATYPE[original_data_sizes[i]];
    new_data_sizes[i] = original_data_sizes[i];
    
		#ifdef INDICES
    for (unsigned long j=0; j < original_data_sizes[i]; ++j) 
      new_data[i][j] = {original_data[i][j].val, original_data[i][j].support, original_data[i][j].i, original_data[i][j].j, original_data[i][j].layer_i, original_data[i][j].layer_j};
		#else
    for (unsigned long j=0; j < original_data_sizes[i]; ++j) 
      new_data[i][j] = {original_data[i][j].val, original_data[i][j].support};
		#endif
  }
}

void generate_data(DATATYPE** data, unsigned long* data_sizes, unsigned long n, unsigned long m, unsigned long k, bool get_all_xj_marginals, LayerArithmetic* la) {
	// Generate random 
  for (unsigned long i=0; i < m; ++i) {
    data[i] = new DATATYPE[n];
    data_sizes[i] = n;
    
    for (unsigned long j=0; j < n; ++j) {
      double v = (double) rand()/(RAND_MAX);
			long support = (long) j;

      #ifdef INDICES
      DATATYPE val = {v, support, j, 0, 0, 0};
      #else
      DATATYPE val = {v, support};
      #endif
      data[i][j] = val;
    }

    // lohify data
    LayerOrderedHeap<DATATYPE> loh(data[i], data_sizes[i], la, COMPARE);
    for (unsigned long j=0; j < data_sizes[i]; ++j) {
      data[i][j].support = j;
      #ifdef INDICES
      data[i][j].i = j;
      data[i][j].j = j;
      #endif
    }
    
    normalize(data[i], data_sizes[i]);    
  }
  
  // Supports on Y
  unsigned long num_y_support = m * (n-1) + 1;
  data[m] = new DATATYPE[num_y_support];
  data_sizes[m] = num_y_support;
  for (unsigned long j=0; j < num_y_support; ++j) {
		double v = (double) rand()/(RAND_MAX);
		long support = (long) j;

    #ifdef INDICES
    DATATYPE val = {v,support, j, 0, 0, 0};
    #else

    DATATYPE val = {v, support};
    #endif
    data[m][j] = val;
	}

	// lohify data
	LayerOrderedHeap<DATATYPE> loh(data[m], data_sizes[m], la, COMPARE);
	for (unsigned long j=0; j < data_sizes[m]; ++j)
		data[m][j].support = j;

	normalize(data[m], data_sizes[m]);

	//write_evg_file(data, data_sizes, m, get_all_xj_marginals);
	
	for (unsigned long i=0; i < m+1; ++i) 
	  _log(data[i], data_sizes[i]);
}

std::pair<double*, unsigned long> get_marginal_on_y(DATATYPE** data, unsigned long* data_sizes, LayerArithmetic* la, unsigned long m, unsigned long k) {
	// Does a standard Cartesian product on X_1 + X_2 + ... + X_m
	BayesianTree<DATATYPE> tree(data, data_sizes, la, m, COMPARE);
	Clock c;
	std::pair<double*, unsigned long> res =  tree.marginal_on_y(k);
	normalize_and_exp(res);
	double t = c.tock();
	printn("Seconds for marginal on y only TOOK ", t);
	return res;
}

std::pair<double*, unsigned long> get_marginal_on_y_holding_out_x_i(DATATYPE** data, unsigned long* data_sizes, LayerArithmetic* la, unsigned long m, unsigned long i, unsigned long k_from_additive_sum, unsigned long k_from_interested_variable) {
	/*
	 1. Does a (k_from_additive_sum)-select Cartesian product on X_1 +
	 X_2 + ... + X_{i-1} + X_{i+1} + ... + X_m

	 2. Then a (k_from_interes-select)-select on X_i.

	 3. The full Cartesian product on the (k_from_additive_sum)-selection
	 of X_1 + X_2 + ... + X_{i-1} + X_{i+1} + ... + X_m and the
	 (k_from_interes-select)-select of X_i is performed
	*/
	
	BayesianTree<DATATYPE> tree(data, data_sizes, la, m, COMPARE);
	Clock c;
	std::pair<double*, unsigned long> res = tree.marginal_on_y_holding_out_x_i(i,k_from_additive_sum, k_from_interested_variable);
	normalize_and_exp(res);
	double t = c.tock();
	printn("Seconds for marginal on y holding out x_i TOOK ", t);
	return res;
}

std::pair<double*, unsigned long> get_marginal_on_x_i_holdout_x_j(DATATYPE** data, unsigned long* data_sizes, LayerArithmetic* la, unsigned long m, unsigned long i, unsigned long j, unsigned long k_from_additive_sum, unsigned long k_from_interested_variable) {
	/*
	 1. Does a (k_from_additive_sum)-select Cartesian product on X_1 +
	 X_2 + ... + X_{i-1} + X_{i+1} + ... + X_m

	 2. Then a (k_from_interes-select)-select on X_i.

	 3. The full Cartesian product on the
	 (k_from_additive_sum)-selection of X_1 + X_2 + ... + X_{i-1} +
	 X_{i+1} + ... + X_m and the (k_from_interes-select)-select of X_i
	 is performed and then the marginal on X_i is found by looking
	 through all output values and replacing the marginal at X_i[k] if k
	 is the index in X_i that contributed to the Y.
	*/

	BayesianTree<DATATYPE> tree(data, data_sizes, la, m, COMPARE);
	Clock c;
	std::pair<double*, unsigned long> res = tree.marginal_on_x_i_holdout_x_j(i, j, k_from_additive_sum, k_from_interested_variable);
	normalize_and_exp(res);
	double t = c.tock();
	printn("Seconds for marginal on x_i holding out x_j TOOK ", t);
	return res;
}

PrimitiveVector<std::pair<double*, unsigned long> > get_marginal_on_all_x_j(DATATYPE** data, unsigned long* data_sizes, LayerArithmetic* la, unsigned long m, unsigned long k) {
	/*
		For all X_j:
		Standard get marginal on X_j. Solve for top k on X_1 + X_2 + ... +
	  X_m.  For each outcome in Y, get the support in X_i that
	  contributed to the support in Y, if it is the maximum seen so far,
	  assign the value on Y to the value at X_.  (Multiply in prior on
	  Y after the selection, before looking at X_j values).
	*/

	#ifndef INDICES
	printn("ERROR: need to compile with -D INDICES if you want marginal on all x_j");
	exit(1);
	#endif
	BayesianTree<DATATYPE> tree(data, data_sizes, la, m, COMPARE);
	Clock c;
  PrimitiveVector<std::pair<double*, unsigned long> > res = tree.marginal_on_all_x_j(k);
	for (unsigned long i=0; i<res.size(); ++i)
		normalize_and_exp(res[i]);

	double t = c.tock();
	printn("Seconds for marginals on all x_j TOOK ", t);
	return res;
}

PrimitiveVector<std::pair<double*, unsigned long> > get_marginal_on_all_x_j_holdout_x_i(DATATYPE** data, unsigned long* data_sizes, LayerArithmetic* la, unsigned long m, unsigned long i, unsigned long k_from_additive_sum, unsigned long k_from_interested_variable) {
	/* 
		 Solve for same selection as get_marginal_on_y_holding_out_x_i
		 then after the Cartesian product between the selection and the
		 top k_from_interested_variable and the selection on X_1 + X_2 +
		 ... get marginals on all x_j in the same way as
		 get_marginal_on_x_i_holdout_x_j
	 */

	BayesianTree<DATATYPE> tree(data, data_sizes, la, m, COMPARE);
	Clock c;
	PrimitiveVector<std::pair<double*, unsigned long> > res = tree.marginal_on_all_x_j_holdout_x_i(i, k_from_additive_sum, k_from_interested_variable);
	for (unsigned long i=0; i<res.size(); ++i)
		normalize_and_exp(res[i]);

	double t = c.tock();
	printn("Seconds for marginals on all x_j holdout x_i TOOK ", t);
	return res;
}

std::pair<double*, unsigned long> get_marginal_on_just_xi_holdout_xi(DATATYPE** data, unsigned long* data_sizes, LayerArithmetic* la, unsigned long m, unsigned long i, unsigned long k_from_additive_sum, unsigned long k_from_interested_variable) {
	/*
		Perform (k_from_additive_sum)-selection of X_1 + X_2 + ... +
	 X_{i-1} + X_{i+1} + ... + X_m and the
	 (k_from_interes-select)-select of X_i (which is defaulted to
	 X_0). Then get marginal on X_i as in other functions; however, this
	 method does not need to keep track of indices in Y since we already
	 know the index in X_i when the Cartesian product between X_i and
	 X_1 + X_2 + .... is performed. Thus it is lightning fast
	 */
	BayesianTree<DATATYPE> tree(data, data_sizes, la, m, COMPARE);
	Clock c;
	std::pair<double*, unsigned long> res = tree.marginal_on_just_x_0_holdout_x_0(0, k_from_additive_sum, k_from_interested_variable);
	normalize_and_exp(res);
	double t = c.tock();
	printn("Seconds for get_marginal_on_just_x0_holdout_x0 TOOK ", t);
	return res;
}


int main(int argc, char**argv){
  if (argc < 6 ) {
    std::cerr << "usage:  <n> <m> <k> <alpha> <seed>" << std::endl;
	}
	
  else {

    // Gather parameters
    unsigned long n = read<unsigned long>(argv[1]);
    unsigned long m = read<unsigned long>(argv[2]);
    unsigned long k = read<unsigned long>(argv[3]);
		printn("n=", n);
		printn("k=", k);
		printn("m=", m);

    double alpha = read<double>(argv[4]);
    long seed    = read<long>(argv[5]);
    srand(seed);

		#ifdef INDICES
    bool run_marginal_on_y_no_holdouts       = true;
    bool run_marginal_on_y_holdout_x0        = true;
    bool run_marginal_on_x_i_holdout_x_j     = true;
    bool run_marginal_on_just_x0_holdout_x0  = true;
    bool run_marginal_on_all_x_j             = true;
    bool run_marginal_on_all_x_j_holdout_x_i = true;
		#else
    bool run_marginal_on_y_no_holdouts       = true;
    bool run_marginal_on_y_holdout_x0        = true;
    bool run_marginal_on_x_i_holdout_x_j     = true;
    bool run_marginal_on_just_x0_holdout_x0  = true;
    bool run_marginal_on_all_x_j             = false;
    bool run_marginal_on_all_x_j_holdout_x_i = false;
		#endif

		std::pair<double*, unsigned long> marginal_on_y_no_holdouts;
		std::pair<double*, unsigned long> marginal_on_y_holdout_x0;
		std::pair<double*, unsigned long> marginal_on_x_i_holdout_x_j;
		std::pair<double*, unsigned long> marginal_on_x0_holdout_x0;
		PrimitiveVector<std::pair<double*, unsigned long> > marginals_on_all_x_j;
		PrimitiveVector<std::pair<double*, unsigned long> > marginals_on_all_x_j_holdout_x_i;		

    DATATYPE** data  = new DATATYPE*[m+1];
    unsigned long* data_sizes = new unsigned long[m+1];

    LayerArithmetic* la = new LayerArithmetic(alpha, n);
    generate_data(data, data_sizes, n, m, k, (run_marginal_on_all_x_j || run_marginal_on_all_x_j_holdout_x_i), la);

		unsigned long k_from_additive_sum = sqrt(k);
		unsigned long k_from_interested_variable = sqrt(k);
		unsigned long interested_variable = 0;

		if (run_marginal_on_y_no_holdouts) {
			DATATYPE** local_data  = new DATATYPE*[m+1];
			unsigned long* local_data_sizes = new unsigned long[m+1];
		  copy_data(data, local_data, data_sizes, local_data_sizes, m);
			
			marginal_on_y_no_holdouts = get_marginal_on_y(local_data, local_data_sizes, la, m, k);

			delete[] local_data_sizes;
			for (unsigned long i=0; i< m+1; ++i)
				delete[] local_data[i];
		}

		if (run_marginal_on_y_holdout_x0) {
			DATATYPE** local_data  = new DATATYPE*[m+1];
			unsigned long* local_data_sizes = new unsigned long[m+1];
		  copy_data(data, local_data, data_sizes, local_data_sizes, m);		

			marginal_on_y_holdout_x0 = get_marginal_on_y_holding_out_x_i(local_data, local_data_sizes, la, m, interested_variable, k_from_additive_sum, k_from_interested_variable);

			delete[] local_data_sizes;
			for (unsigned long i=0; i< m+1; ++i)
				delete[] local_data[i];
			delete[] local_data;
		}

		if (run_marginal_on_x_i_holdout_x_j) {
			DATATYPE** local_data  = new DATATYPE*[m+1];
			unsigned long* local_data_sizes = new unsigned long[m+1];
		  copy_data(data, local_data, data_sizes, local_data_sizes, m);

			unsigned long x_holdout = 1;
			unsigned long x_var = 0;
			marginal_on_x_i_holdout_x_j = get_marginal_on_x_i_holdout_x_j(local_data, local_data_sizes, la, m, x_var, x_holdout, k_from_additive_sum, k_from_interested_variable);

			delete[] local_data_sizes;
			for (unsigned long i=0; i< m+1; ++i) 
				delete[] local_data[i];
			delete[] local_data;
		}

		if (run_marginal_on_all_x_j) {
			DATATYPE** local_data  = new DATATYPE*[m+1];
			unsigned long* local_data_sizes = new unsigned long[m+1];
		  copy_data(data, local_data, data_sizes, local_data_sizes, m);
			
			marginals_on_all_x_j = get_marginal_on_all_x_j(local_data, local_data_sizes, la, m, k);

			delete[] local_data_sizes;
			for (unsigned long i=0; i< m+1; ++i)
				delete[] local_data[i];
			delete[] local_data;
		}

		if (run_marginal_on_all_x_j_holdout_x_i) {
			DATATYPE** local_data  = new DATATYPE*[m+1];
			unsigned long* local_data_sizes = new unsigned long[m+1];
		  copy_data(data, local_data, data_sizes, local_data_sizes, m);
			
			marginals_on_all_x_j_holdout_x_i = get_marginal_on_all_x_j_holdout_x_i(local_data, local_data_sizes, la, m, interested_variable, k_from_additive_sum, k_from_interested_variable);

			delete[] local_data_sizes;
			for (unsigned long i=0; i< m+1; ++i)
				delete[] local_data[i];
			delete[] local_data;
		}


		if (run_marginal_on_just_x0_holdout_x0) {
			DATATYPE** local_data  = new DATATYPE*[m+1];
			unsigned long* local_data_sizes = new unsigned long[m+1];
			copy_data(data, local_data, data_sizes, local_data_sizes, m);
			
		        marginal_on_x0_holdout_x0 = get_marginal_on_just_xi_holdout_xi(local_data, local_data_sizes, la, m, interested_variable, k_from_additive_sum, k_from_interested_variable);

			delete[] local_data_sizes;
			for (unsigned long i=0; i< m+1; ++i)
			  delete[] local_data[i];
			delete[] local_data;
		}


		///////////////// PRINT RESULTS


		printn();

		if (run_marginal_on_y_no_holdouts) {
			printn("#Marginal on Y (no holdouts)");
			std::cout << "Y_no_holdouts=[";
			for (unsigned long i=0; i<marginal_on_y_no_holdouts.second; ++i) 
				std::cout << "(" <<  marginal_on_y_no_holdouts.first[i] << ", " << i << "), ";
			printn("]\n");
		}

		if (run_marginal_on_y_holdout_x0) {
			printn("#Marginal on Y (holdout X_i)");
			std::cout << "Y_holdout_x0=[";
			for (unsigned long i=0; i<marginal_on_y_holdout_x0.second; ++i)
				std::cout << "(" <<  marginal_on_y_holdout_x0.first[i] << ", " << i << "), ";
			printn("]\n");
		}

		if (run_marginal_on_x_i_holdout_x_j) {
			printn("#Marginal on X_0 (holdout X1)");
			std::cout << "X0_holdout_X1=[";
			for (unsigned long i=0; i<marginal_on_x_i_holdout_x_j.second; ++i)
				std::cout << "(" <<  marginal_on_x_i_holdout_x_j.first[i] << ", " << i << "), ";
			printn("]\n");
		}

		if (run_marginal_on_just_x0_holdout_x0) {
		  printn("#Marginal on X_0 ho x0");
		  std::cout << "X0_just_ho_x0=[";
		  for (unsigned long i=0; i<marginal_on_x0_holdout_x0.second; ++i)
		    std::cout << "(" <<  marginal_on_x0_holdout_x0.first[i] << ", " << i << "), ";
		  printn("]\n");
		}

		if (run_marginal_on_all_x_j) {
			for (unsigned long j=0; j<marginals_on_all_x_j.size(); ++j) {
				std::cout << "#Marginal on X_" <<  j << std::endl;
				std::cout << "X" << j << "_no_holdout=[";
				for (unsigned long i=0; i<marginals_on_all_x_j[j].second; ++i)
					std::cout << "(" <<  marginals_on_all_x_j[j].first[i] << ", " << i << "), ";
				printn("]\n");
			}
		}

		if (run_marginal_on_all_x_j_holdout_x_i) {
			for (unsigned long j=0; j<marginals_on_all_x_j_holdout_x_i.size(); ++j) {
				std::cout << "#Marginal on X_" <<  j << " holdout X_" << interested_variable <<  std::endl;
				std::cout << "X" << j << "_holdout_x_i=[";
				for (unsigned long i=0; i<marginals_on_all_x_j_holdout_x_i[j].second; ++i)
					std::cout << "(" <<  marginals_on_all_x_j_holdout_x_i[j].first[i] << ", " << i << "), ";
				printn("]\n");
			}
		}

		// Free all memory
		for (unsigned long i=0; i< m+1; ++i)
			delete[] data[i];
		delete[] data;

		delete[] marginal_on_y_no_holdouts.first;
		delete[] marginal_on_y_holdout_x0.first;
		delete[] marginal_on_x_i_holdout_x_j.first;
		delete la;
  }
}
