#ifndef _VALUE_AND_SUPPORT_HPP
#define _VALUE_AND_SUPPORT_HPP

#include <math.h>
#include "Utility/io.hpp"

using namespace serang_lab;

template <typename T>
struct ValueAndSupport {
  T val;
  long support;

  void operator =(const ValueAndSupport & rhs) {
    val = rhs.val;
    support = rhs.support;
  }

  void operator +=(const ValueAndSupport & rhs) {
    val += rhs.val;
    support += rhs.support;
  }

  ValueAndSupport():
    val(T(0)),
    support(0)
  {}

  ValueAndSupport(T v, long support):
    val(v),
    support(support)
  {}

  ValueAndSupport(ValueAndSupport & rhs):
    val(rhs.val),
    support(rhs.support)
  {}

  ValueAndSupport(ValueAndSupport && rhs):
    val(0),
    support(0)
  {
    std::swap(val, rhs.val);
    std::swap(support, rhs.support);
  }

  friend std::ostream & operator<<(std::ostream & os, const ValueAndSupport & rhs) {
    os << exp(rhs.val) << " support ( " << rhs.support << " )";
    return os;
  }
};

template <typename T>
bool operator <(const ValueAndSupport<T> & lhs, const ValueAndSupport<T> & rhs) {
  return lhs.val < rhs.val;
}

template <typename T>
bool operator >(const ValueAndSupport<T> & lhs, const ValueAndSupport<T> & rhs)  {
  return lhs.val > rhs.val;
}

template <typename T>
bool operator ==(const ValueAndSupport<T> & lhs, const ValueAndSupport<T> & rhs) {
  return lhs.val == rhs.val;
}

template <typename T>
ValueAndSupport<T> operator +(const ValueAndSupport<T> & lhs, const ValueAndSupport<T> & rhs)  {
  ValueAndSupport<T> result(lhs.val + rhs.val, lhs.support + rhs.support);
  return result;
}

#endif
